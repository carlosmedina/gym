﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Sistema
{
    public class Listas
    {
        public static List<SelectListItem> ObtenerListaGenero()
        {
            return new List<SelectListItem>
            {
                new SelectListItem{ Text = "Seleccione...", Value = ""},
                new SelectListItem{ Text = "Femenino", Value = "1"},
                new SelectListItem{ Text = "Masculino", Value = "2"},
            };
        }

        public static List<SelectListItem> ObtenerListaCantidadPagina()
        {
            return new List<SelectListItem>
            {
                new SelectListItem{ Text = "10", Value = "10"},
                new SelectListItem{ Text = "20", Value = "20"},
                new SelectListItem{ Text = "50", Value = "50"},
                new SelectListItem{ Text = "100", Value = "100"},
            };
        }

        public static List<SelectListItem> ObtenerListaCadido()
        {
            return new List<SelectListItem>
            {
                new SelectListItem{ Text = "Si / No", Value = ""},
                new SelectListItem{ Text = "Si", Value = "1"},
                new SelectListItem{ Text = "No", Value = "2"},
            };
        }

        public static List<SelectListItem> ObtenerListaDisposicionDocumental()
        {
            return new List<SelectListItem>
                {
                    new SelectListItem {Text = "Seleccione...", Value = ""},
                    new SelectListItem {Text = "Conservacion Permanente", Value = "1"},
                    new SelectListItem {Text = "Muestreo", Value = "2"},
                    new SelectListItem {Text = "Baja Documental", Value = "3"}
                };
        }
        public static List<SelectListItem> ObtenerListaValorDocumental()
        {
            return new List<SelectListItem>
            {
                new SelectListItem {Text = "Seleccione...", Value = ""},
                new SelectListItem {Text = "Administrativo", Value = "1"},
                new SelectListItem {Text = "Contable", Value = "2"},
                new SelectListItem {Text = "Legal", Value = "3"}
            };
        }

        public static List<SelectListItem> ObtenerListaClasificacionInformacion()
        {
            return new List<SelectListItem>
            {
                new SelectListItem {Text = "Seleccione...", Value = ""},
                new SelectListItem {Text = "Público", Value = "1"},
                new SelectListItem {Text = "Confidencial", Value = "2"},
                new SelectListItem {Text = "Reservado", Value = "3"}
            };
        }
    }
}
