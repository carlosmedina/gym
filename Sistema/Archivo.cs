﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Web;
using Sistema.Extensiones;

namespace Sistema
{
    public class Archivo
    {
        public enum Ruta
        {
            [Description(@"~\Archivos")] Archivos,
            [Description(@"~\Archivos\FotosEmpleados\")] FotosEmpleados,
            [Description(@"~\Archivos\FotosUsuarios\")] FotosUsuario,
            [Description(@"~\Archivos\FotosUsuarios\no_user.png")] FotoDefault,
            [Description(@"~\Archivos/Expedientes\")] Expedientes
        }

        public static string ObtenerRutaServidor(HttpPostedFileBase archivo, Ruta ruta, string nombreArchivo = null)
        {
            if (archivo == null) return string.Empty;

            if (nombreArchivo == null) nombreArchivo = archivo.FileName;

            return Path.Combine(HttpContext.Current.Server.MapPath(ruta.Descripcion()), nombreArchivo);
        }

        public static string ObtenerRutaServidor(HttpPostedFileBase archivo, Ruta rutaRelativaRepositorio, string rutaExpediente, string nombreArchivo = null)
        {
            if (archivo == null) return string.Empty;

            if (nombreArchivo == null) nombreArchivo = archivo.FileName;

            var rutaAbsolutaRepositorio = HttpContext.Current.Server.MapPath(rutaRelativaRepositorio.Descripcion());

            var rutaAbsoluta = Path.Combine(rutaAbsolutaRepositorio, rutaExpediente, nombreArchivo);

            return rutaAbsoluta;
        }

        public static string ObtenerRutaServidor(HttpPostedFileBase archivo, string rutaAbsolutaRepositorio, string rutaExpediente, string nombreArchivo = null)
        {
            if (archivo == null) return string.Empty;

            if (nombreArchivo == null) nombreArchivo = archivo.FileName;
            
            var rutaAbsoluta = Path.Combine(rutaAbsolutaRepositorio, rutaExpediente, nombreArchivo);

            return rutaAbsoluta;
        }

        public static string ObtenerRutaServidor(string rutaRepositorio, string ubicacionArchivo)
        {
            var repositrioArchivo = Path.Combine(rutaRepositorio, ubicacionArchivo);

            var directorioAbsoluto = HttpContext.Current.Server.MapPath(repositrioArchivo);

            return directorioAbsoluto;
        }

        public static string ObtenerRutaFotoBaseDatos(HttpPostedFileBase archivo, Ruta ruta, string nombreFoto = null)
        {
            return archivo == null
                ? Ruta.FotoDefault.Descripcion()
                : ruta.Descripcion() + nombreFoto + Path.GetExtension(archivo.FileName);
        }

        public static void GuardarArchivo(HttpPostedFileBase archivo, Ruta ruta)
        {
            archivo?.SaveAs(ObtenerRutaServidor(archivo, ruta));
        }

        public static bool GuardarImagen(HttpPostedFileBase archivo, Ruta ruta, string nombreFoto = null)
        {
            if (archivo == null) return false;
            var extension = Path.GetExtension(archivo.FileName);

            if (extension != ".jpeg" && extension != ".jpg" && extension != ".png") return false;
            if (nombreFoto == null) nombreFoto = archivo.FileName;

            var rutaGuardar = ObtenerRutaServidor(archivo, ruta, nombreFoto + extension);

            if (File.Exists(rutaGuardar)) File.Delete(rutaGuardar);
            archivo.SaveAs(rutaGuardar);

            return true;
        }

        public static string CrearCarpetaExpediente(PartesUbicacionExpediente partes)
        {
            var relativaDirectorio = Path.Combine(partes.ObtenerArreglo());

            var ubicacionRepositorio = ConfigurationManager.AppSettings["UbicacionRepositorio"];

            var absolutaDirectorio = string.IsNullOrEmpty(ubicacionRepositorio)
                ? ObtenerRutaServidor(Ruta.Expedientes.Descripcion(), relativaDirectorio)
                : ObtenerRutaServidor(ubicacionRepositorio, relativaDirectorio);

            if (Directory.Exists(absolutaDirectorio) == false)
                Directory.CreateDirectory(absolutaDirectorio);
            
            return relativaDirectorio + @"\";
        }

        public static void GuardarExpediente(HttpPostedFileBase adjunto, string ubicacionActual)
        {
            var ubicacionRepositorio = ConfigurationManager.AppSettings["UbicacionRepositorio"];

            var absolutaArchivo = string.IsNullOrEmpty(ubicacionRepositorio)
                ? ObtenerRutaServidor(Ruta.Expedientes.Descripcion(), ubicacionActual)
                : ObtenerRutaServidor(ubicacionRepositorio, ubicacionActual);

            var absolutaDirectorio = Path.GetDirectoryName(absolutaArchivo);

            if (Directory.Exists(absolutaDirectorio) == false)
                Directory.CreateDirectory(absolutaDirectorio);

            if (File.Exists(absolutaArchivo))
                File.Delete(absolutaArchivo);

            adjunto.SaveAs(absolutaArchivo);
        }

        public static string ObtenerRutaExpediente(string ubicacionActual)
        {
            var ubicacionRepositorio = ConfigurationManager.AppSettings["UbicacionRepositorio"];

            var absolutaArchivo = string.IsNullOrEmpty(ubicacionRepositorio)
                ? ObtenerRutaServidor(Ruta.Expedientes.Descripcion(), ubicacionActual)
                : ObtenerRutaServidor(ubicacionRepositorio, ubicacionActual);

            return absolutaArchivo;
        }

        public static byte[] ObtenerExpediente(string ubicacionExpediente)
        {
            var bytes = File.ReadAllBytes(ubicacionExpediente);

            return bytes;
        }
    }
}
