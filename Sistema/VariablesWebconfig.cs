﻿
using System.Configuration;

namespace Sistema
{
    public static class VariablesWebconfig
    {
        public static string ObtenerUrlVisualizadorDocumentosOffice()
        {
            return ConfigurationManager.AppSettings["UrlVisualizadorDocumentosOffice"];
        }

        public static string ObtenerUrlVisualizadorDocumentosPdf()
        {
            return ConfigurationManager.AppSettings["UrlVisualizadorDocumentosPdf"];
        }
    }
}
