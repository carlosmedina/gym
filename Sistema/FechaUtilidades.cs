﻿using System;
using System.Globalization;

namespace Sistema
{
    public class FechaUtilidades
    {
        public static string ObtenerNombreMes(int mes)
        {
            var nombreMes = new DateTime(DateTime.Now.Year, mes, 1).ToString("MMMM", new CultureInfo("es-ES"));
            return nombreMes[0].ToString().ToUpper() + nombreMes.Substring(1);
        }
    }
}
