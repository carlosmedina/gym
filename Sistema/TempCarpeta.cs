﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Sistema
{
    public static class TempCarpeta
    {
        public static string RutaTemp { get; set; }

        public static void EliminarArchivosAsync()
        {
            new Task(_EliminarArchivosViejos).Start();
        }

        public static void Guardar(byte[] archivo, string nombre)
        {
            var ruta = Path.Combine(RutaTemp, nombre);

            if (File.Exists(ruta)) _Eliminar(ruta);

            File.WriteAllBytes(ruta, archivo);

            _EliminarArchivosViejos();
        }

        private static void _Eliminar(string ruta)
        {
            try
            {
                File.Delete(ruta);
            }
            catch
            {
            }
        }

        private static void _EliminarArchivosViejos()
        {
            var directorio = new DirectoryInfo(RutaTemp);

            foreach (var archivo in directorio.GetFiles())
            {
                if (archivo.LastAccessTime < DateTime.Now.AddDays(-1))
                {
                    try
                    {
                        archivo.Delete();
                    }
                    catch
                    {
                    }
                }
            }
        }
    }
}
