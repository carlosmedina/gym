﻿using System;
using System.Collections.Generic;

namespace Sistema
{
    public class PartesUbicacionExpediente
    {
        public string ClaveFondo { get; set; }
        public string EntidadClave { get; set; }
        public string ClaveSeccion { get; set; }
        public string ClaveSerie { get; set; }
        public string ClaveSubserie { get; set; }
        public string NumeroExpediente { get; set; }
        
        public string[] ObtenerArreglo()
        {
            var lista = new List<string>
            {
                ClaveFondo,
                EntidadClave,
                ClaveSeccion,
                ClaveSerie
            };

            if (string.IsNullOrEmpty(ClaveSubserie) == false)
                lista.Add(ClaveSubserie);

            lista.Add(DateTime.Now.Year.ToString());
            lista.Add(DateTime.Now.Month.ToString());
            lista.Add(DateTime.Now.Day.ToString());

            lista.Add(NumeroExpediente);

            return lista.ToArray();
        }
    }
}
