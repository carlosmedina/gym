﻿using System;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Sistema
{
    public class Correo
    {
        public static async Task<bool> RestablecerContrasena(string destinatario, string asunto, string codigo)
        {
            var msg = new MailMessage();
            var enlace = DireccionWebSitio() + "login/RestablecerContrasena?c=" + codigo;
            msg.To.Add(destinatario);
            msg.From = new MailAddress(CorreoElectronico(), NombreAplicacion(), Encoding.UTF8);
            msg.Subject = asunto;
            msg.SubjectEncoding = Encoding.UTF8;
            msg.Body = "<h2>Restablecer contraseña</h2><br/><a href='" + enlace + "'>" + enlace + "</a>";
            msg.BodyEncoding = Encoding.UTF8;
            msg.IsBodyHtml = true;

            //Si vas a enviar un correo con contenido html entonces cambia el valor a true
            //Aquí es donde se hace lo especial
            var clienteSmtp = PropiedadesServidorCorreo();
            try
            {
                await clienteSmtp.SendMailAsync(msg);
                return true;
            }
            catch (SmtpFailedRecipientsException)
            {
                return false;
            }
            catch (SmtpException)
            {
                return false;
            }

        }

        #region Correo
        /// <summary>
        /// Envia una notificacion por correo electronico
        /// </summary>
        /// <param name="para">Direccion de correo electronico del destinatario</param>
        /// <param name="mensaje">Mensaje de la notificacion</param>
        private void EnviarPorCorreoElectronico(string para, string mensaje)
        {
            var correo = NuevoCorreo(para, mensaje);
            EnviarCorreo(correo);
        }

        /// <summary>
        /// Crea un nuevo correo electronico listo para su envio
        /// </summary>
        /// <param name="para">Correo electronico del distinatario</param>
        /// <param name="mensaje">Mensaje del correo electronico</param>
        /// <returns>Nueva instancia de tipo MailMessage lista para enviarse</returns>
        private MailMessage NuevoCorreo(string para, string mensaje)
        {
            var correo = CrearCorreo();

            correo.To.Add(CrearDireccionDeCorreo(para));

            correo.Body = mensaje;

            return correo;
        }

        /// <summary>
        /// Crea un nuevo correo electronico
        /// </summary>
        /// <returns>Nueva instancia de tipo MailMessage con los valos de configuracion por default</returns>
        private MailMessage CrearCorreo()
        {
            return new MailMessage
            {
                From = CrearDireccionDeCorreo(CorreoElectronico()),
                Subject = AsuntoCorreoElectronico(),
                BodyEncoding = Encoding.UTF8,
                IsBodyHtml = true
            };
        }

        /// <summary>
        /// Envia un correo electronico de manera asincrona
        /// </summary>
        /// <param name="correo">Correo electronico a enviar</param>
        private void EnviarCorreo(MailMessage correo)
        {
            var clienteSmtp = CrearClienteSmtp();

            clienteSmtp.SendAsync(correo, null);

        }

        /// <summary>
        /// Crea un cliente de SMTP para el envio de correos electronicos
        /// </summary>
        /// <returns>Nueva instancia de tipo SmtpClient para el envio de correos</returns>
        private SmtpClient CrearClienteSmtp()
        {
            var clienteSmtp = new SmtpClient
            {
                Host = ServidorSmtp(),
                UseDefaultCredentials = true,
                Credentials = CrearCredencial(),
                Port = PuertoSmtp(),
                EnableSsl = UsarSslSmtp()
            };

            return clienteSmtp;
        }

        /// <summary>
        /// Crea una nueva credencial para el envio del correo electronico
        /// </summary>
        /// <returns>Nueva instancia de tipo NetworkCredential con la cuenta el envio de correos electronicos</returns>
        private System.Net.NetworkCredential CrearCredencial()
        {
            return new System.Net.NetworkCredential(UsuarioCorreoElectronico(), PasswordCorreoElectronico());
        }

        /// <summary>
        /// Crea una nueva direccicon de correo electronico
        /// </summary>
        /// <param name="correo">Correo electronico a convertir</param>
        /// <returns>Nueva instancia MailAdress con la direccion de correo electronico</returns>
        private MailAddress CrearDireccionDeCorreo(string correo)
        {
            return new MailAddress(correo);
        }
        #endregion

        #region Website
        public static string DireccionWebSitio()
        {
            return System.Configuration.ConfigurationManager.AppSettings["DireccionWebSitio"];
        }

        /// <summary>
        /// Obtiene el nombre de la aplicacion
        /// </summary>
        /// <returns>Nombre de la aplicacion</returns>
        public static string NombreAplicacion()
        {
            return System.Configuration.ConfigurationManager.AppSettings["NombreAplicacion"];
        }
        #endregion

        #region CorreoElectronico

        /// <summary>
        /// Contiene la direccion de correo electronico del sistema
        /// </summary>
        /// <returns>Direccion de correo electronico del sistema</returns>
        public static string CorreoElectronico()
        {
            return System.Configuration.ConfigurationManager.AppSettings["CorreoElectronico"];
        }

        /// <summary>
        /// Contiene el nombre de usuario de la cuenta de correo electronico del sistema
        /// </summary>
        /// <returns>Nombre de usuario de la cuenta de correo electronico del sistema</returns>
        public static string UsuarioCorreoElectronico()
        {
            return System.Configuration.ConfigurationManager.AppSettings["UsuarioCorreo"];
        }

        /// <summary>
        /// Contiene el password de la cuenta de correo electronico del sistema
        /// </summary>
        /// <returns>Password de la cuenta de correo electronico del sistema</returns>
        public static string PasswordCorreoElectronico()
        {
            return System.Configuration.ConfigurationManager.AppSettings["PasswordCorreo"];
        }

        /// <summary>
        /// Contiene la direccion del servidor de SMTP
        /// </summary>
        /// <returns>Direccion del servidor de SMTP</returns>
        public static string ServidorSmtp()
        {
            return System.Configuration.ConfigurationManager.AppSettings["ServidorSmtp"];
        }

        /// <summary>
        /// Contiene la direccion del puerto para servidor de SMTP
        /// </summary>
        /// <returns>Direccion del puerto para el servidor de SMTP</returns>
        public static int PuertoSmtp()
        {
            return int.Parse(System.Configuration.ConfigurationManager.AppSettings["PuertoSmtp"]);
        }

        /// <summary>
        /// Contiene si se usuara una conexion SSL para el envio del correo
        /// </summary>
        /// <returns>Indica si se usuara una conexion SSL para el envio del correo</returns>
        public static bool UsarSslSmtp()
        {
            return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["UsarSslSmtp"]);
        }

        /// <summary>
        /// Contiene el asunto que tendra el correo electronico
        /// </summary>
        /// <returns>Asunto que tendra el correo electronico</returns>
        public static string AsuntoCorreoElectronico()
        {
            return System.Configuration.ConfigurationManager.AppSettings["AsuntoCorreoElectronico"];
        }

        #endregion

        #region Servidor SMTP
        public static SmtpClient PropiedadesServidorCorreo()
        {
            return new SmtpClient
            {
                Credentials = new System.Net.NetworkCredential(UsuarioCorreoElectronico(), PasswordCorreoElectronico()),
                Port = PuertoSmtp(),
                Host = ServidorSmtp(),
                EnableSsl = UsarSslSmtp()
            };
        }
        #endregion

    }
}
