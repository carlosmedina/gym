﻿using Datos.Recursos;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Datos.Repositorios.Configuracion
{
    public class RolRepositorio : Repositorio<Rol>
    {
        public RolRepositorio(Contexto contexto) 
            : base(contexto)
        {
        }

        public void CambiarHabilitado(int idRol, bool habilitado)
        {
            var rol = new Rol { IDRol = idRol };
            Contexto.Rol.Attach(rol);
            rol.Habilitado = habilitado;
            Contexto.Entry(rol).Property(o => o.Habilitado).IsModified = true;
        }

        public void ModificarRol(Rol rol)
        {
            var rolModificar = new Rol { IDRol = rol.IDRol };
            Contexto.Rol.Attach(rolModificar);
            rolModificar.Nombre = rol.Nombre;
            rolModificar.Descripcion = rol.Descripcion;
            rolModificar.PaginaInicio = rol.PaginaInicio;
        }

        public string ObtenerNombrePorId(int idRol)
        {
            return (from r in Contexto.Rol
                    where r.IDRol == idRol
                    select r.Nombre).SingleOrDefault();
        }

        public static List<SelectListItem> BuscarRoles()
        {
            List<SelectListItem> lista;
            using (var bd = new Contexto())
            {
                lista = (from m in bd.Rol
                         where m.Habilitado
                         orderby m.Nombre
                         select new SelectListItem
                         {
                             Value = m.IDRol.ToString(),
                             Text = m.Nombre
                         }).ToList();

                if (lista.Count > 0)
                {
                    lista.Insert(0, new SelectListItem { Text = General.Seleccione, Value = string.Empty });
                }
            }
            return lista;
        }
    }
}
