﻿using System.Collections.Generic;
using System.Linq;

namespace Datos.Repositorios.Configuracion
{
    public class MenuRepositorio : Repositorio<Menu>
    {
        public MenuRepositorio(Contexto contexto) 
            : base(contexto)
        {
        }
        
        public new int Guardar(Menu menuInsertar)
        {
            Contexto.Menu.Add(menuInsertar);
            return Contexto.SaveChanges();
        }
        
        public new int Modificar(Menu menuCambios)
        {
            var menuModificar = new Menu { IDMenu = menuCambios.IDMenu };
            Contexto.Menu.Attach(menuModificar);
            menuModificar.Opcion = menuCambios.Opcion;
            menuModificar.Destino = menuCambios.Destino;
            menuModificar.Visible = menuCambios.Visible;
            menuModificar.Camino = menuCambios.Camino;
            menuModificar.IconoCss = menuCambios.IconoCss;
            menuModificar.Controlador = menuCambios.Controlador;
            menuModificar.Accion = menuCambios.Accion;
            menuModificar.Area = menuCambios.Area;
            Contexto.Entry(menuModificar).Property(o => o.Visible).IsModified = true;
            return Contexto.SaveChanges();
        }
        
        public bool CambiarHabilitado(int idMenu, bool habilitado)
        {
            var opcion = new Menu {IDMenu = idMenu};

            Contexto.Entry(opcion).State = System.Data.Entity.EntityState.Unchanged;

            opcion.Habilitado = habilitado;

            Contexto.Entry(opcion).Property(p => p.Habilitado).IsModified = true;

            return Contexto.SaveChanges() == 1;
        }
        
        public Menu BuscarPorId(int idMenu)
        {
            return (from om in Contexto.Menu
                    where om.IDMenu == idMenu
                    select om).FirstOrDefault();
        }
        
        public List<Menu> Buscar(bool habilitados)
        {
            return (from om in Contexto.Menu
                    where om.Habilitado == habilitados
                    orderby om.Indice
                    select om).ToList();
        }
        
        public List<Menu> Buscar(int idPadreMenu, bool habilitados)
        {
            return (from om in Contexto.Menu
                    where om.Padre == idPadreMenu &&
                          om.Habilitado == habilitados
                    orderby om.Indice
                    select om).ToList();
        }
        
        public List<Menu> Buscar(int idPadreMenu, string nombreOpcion, bool habilitados)
        {
            return (from om in Contexto.Menu
                    where om.Padre == idPadreMenu &&
                          om.Opcion.Contains(nombreOpcion) &&
                          om.Habilitado == habilitados
                    orderby om.Indice
                    select om).ToList();
        }
        
        public int ObtenerId(string controlador, string accion)
        {
            return
                (from m in Contexto.Menu
                 where m.Controlador == controlador && m.Accion == accion
                 select m.IDMenu
                ).FirstOrDefault();
        }
        
        public List<Menu> BuscarOpcionMenuYOpcionesSuperiores(int idOpcionMenu)
        {
            return (from om in Contexto.BuscarOpcionMenuAndOpcionesMenuSuperiores(idOpcionMenu)
                    select new Menu
                    {
                        IDMenu = om.IDMenu ?? 0,
                        Opcion = om.Opcion ?? string.Empty,
                        Controlador = om.Controlador,
                        Accion = om.Accion,
                        Area = om.Area,
                        Destino = om.Destino
                    }).ToList();
        }
        
        public List<Menu> BuscarOpcionMenuYOpcionesInferiores(int idOpcionMenu)
        {
            return (from om in Contexto.BuscarOpcionMenuAndOpcionesMenuInferiores(idOpcionMenu)
                    select new Menu
                    {
                        IDMenu = om.IDMenu ?? 0,
                        Opcion = om.Opcion ?? string.Empty
                    }).ToList();
        }
        
        public int? ObtenerMaximoIndice(int idPadre) {
            var opcion = (from menu in Contexto.Menu
                    where menu.Padre == idPadre
                    orderby menu.Indice descending
                    select menu).FirstOrDefault();
            if (opcion != null)
                return opcion.Indice == null ? 1 : opcion.Indice + 1;
            return 1;
        }
        
        public bool BajarNivel(int idMenu, int idPadre)
        {
            var opcion1 = Contexto.Menu.Find(idMenu);
            var opcion2 = Contexto.Menu.First(m => m.Padre == idPadre && m.Indice == opcion1.Indice + 1);

            Contexto.Entry(opcion1).State = System.Data.Entity.EntityState.Detached;
            Contexto.Entry(opcion2).State = System.Data.Entity.EntityState.Detached;

            opcion1.Indice = opcion1.Indice + 1;
            opcion2.Indice = opcion2.Indice - 1;

            Contexto.Entry(opcion1).State = System.Data.Entity.EntityState.Modified;
            Contexto.Entry(opcion2).State = System.Data.Entity.EntityState.Modified;

            return Contexto.SaveChanges() == 1;
        }

        public bool SubirNivel(int idMenu, int idPadre)
        {
            var opcion1 = Contexto.Menu.Find(idMenu);
            var opcion2 = Contexto.Menu.First(m => m.Padre == idPadre && m.Indice == opcion1.Indice - 1);

            Contexto.Entry(opcion1).State = System.Data.Entity.EntityState.Detached;
            Contexto.Entry(opcion2).State = System.Data.Entity.EntityState.Detached;

            opcion1.Indice = opcion1.Indice - 1;
            opcion2.Indice = opcion2.Indice + 1;

            Contexto.Entry(opcion1).State = System.Data.Entity.EntityState.Modified;
            Contexto.Entry(opcion2).State = System.Data.Entity.EntityState.Modified;

            return Contexto.SaveChanges() == 1;
        }
    }
}
