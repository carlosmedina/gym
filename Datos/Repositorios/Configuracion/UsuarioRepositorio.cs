﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Web.Mvc;
using Sistema.Extensiones;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Recursos;

namespace Datos.Repositorios.Configuracion
{
    public class UsuarioRepositorio : Repositorio<Usuario>
    {
        public UsuarioRepositorio(Contexto contexto) : 
            base(contexto)
        {
        }
        
        public Usuario BuscarUsuarioPorCorreoYContrasena(string correo, string contrasena)
        {
            return (from u in Contexto.Usuario
                    .Include(x => x.Rol)
                    .Include(x=> x.Rol.Permisos)
                    .Include(x => x.Rol.MenuInicio)
                    where (u.Email == correo || u.NombreUsuario == correo) &&
                          u.Contrasena == contrasena &&
                          u.Rol.Habilitado &&
                          u.Habilitado
                    select u).FirstOrDefault();
        }
        
        public int ObtenerIdRolPorIdUsuario(int idUsuario)
        {
            return
                (from u in Contexto.Usuario
                 where u.IDUsuario == idUsuario
                 select u.Rol.IDRol).SingleOrDefault();
        }

        public string ObtenerNombrePorId(int idUsuario)
        {
            return (from u in Contexto.Usuario
                    where u.IDUsuario == idUsuario
                    select u.NombreUsuario).SingleOrDefault();
        }

        public string ObtenerContrasenaPorId(int idUsuario)
        {
            return (from u in Contexto.Usuario
                where u.IDUsuario == idUsuario
                select u.Contrasena).SingleOrDefault();
        }

        public Usuario BuscarPorCorreo(string email)
        {
            return (from u in Contexto.Usuario
                    where u.Email == email
                    select u).SingleOrDefault();
        }

        public void ActualizarContrasena(int idUsuario, string contrasena)
        {
            var usuario = new Usuario {IDUsuario = idUsuario};
            Contexto.Usuario.Attach(usuario);
            usuario.Contrasena = contrasena.GetHashSha512();
            Contexto.SaveChanges();
        }
        
        public List<Usuario> Buscar(UsuarioViewModel criterios)
        {
            return ObtenerQuery(criterios, true).ToList();
        }
        
        public int ObtenerTotalRegistros(UsuarioViewModel criterios)
        {
            return ObtenerQuery(criterios, false).Count();
        }

        private IQueryable<Usuario> ObtenerQuery(UsuarioViewModel criterios, bool paginar)
        {
            IQueryable<Usuario> query = Contexto.Set<Usuario>();

            query = query.Where(b => b.Habilitado == criterios.Usuario.Habilitado);

            if (!string.IsNullOrEmpty(criterios.Usuario.NombreUsuario))
            {
                query = query.Where(b => b.NombreUsuario.Contains(criterios.Usuario.NombreUsuario));
            }
            if (criterios.Usuario.Rol != null && criterios.Usuario.Rol.IDRol > 0)
            {
                query = query.Where(b => b.Rol.IDRol == criterios.Usuario.Rol.IDRol);
            }
            if (criterios.Usuario.IDEntidad > 0)
            {
                query = query.Where(q => q.IDEntidad == criterios.Usuario.IDEntidad);
            }

            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.Include(q => q.Entidad);
                query = query.Include(q => q.Rol);

                query = query.OrderBy(q => q.NombreUsuario);

                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }
            return query;
        }
        
        public Usuario BuscarPorIdEmpleado(int idEmpleado)
        {
            return (from u in Contexto.Usuario
                    where u.IDUsuario == idEmpleado
                    select u).SingleOrDefault();
        }
        
        public Usuario BuscarPorId(int idUsuario)
        {
            return (from r in Contexto.Usuario
                        .Include(x => x.Rol)
                    where r.IDUsuario == idUsuario
                    select r).SingleOrDefault();
        }
        
        public int ObtenerIdUsuarioPorIdEmpleado(int idEmpleado)
        {
            return (from u in Contexto.Usuario
                    where u.IDUsuario == idEmpleado
                    select u.IDUsuario).SingleOrDefault();
        }
        
        public void GuardarUsuario(Usuario usuario)
        {
            Contexto.Usuario.Attach(usuario);
            usuario.Habilitado = true;
            usuario.NombreUsuario = usuario.NombreUsuario;
            usuario.Contrasena = usuario.Contrasena.GetHashSha512();
            Guardar(usuario);
        }
        
        public void ActualizarUsuario(Usuario usuario)
        {
            var usuarioModificar = new Usuario { IDUsuario = usuario.IDUsuario };
            
            Contexto.Usuario.Attach(usuarioModificar);

            usuarioModificar.IDRol = usuario.IDRol;
            usuarioModificar.NombreUsuario = usuario.NombreUsuario;
            usuarioModificar.Nombre = usuario.Nombre;
            usuarioModificar.ApellidoP = usuario.ApellidoP;
            usuarioModificar.ApellidoM = usuario.ApellidoM;
            usuarioModificar.IDEntidad = usuario.IDEntidad;
            usuarioModificar.UrlFoto = usuario.UrlFoto;
            usuarioModificar.Sexo = usuario.Sexo;

            if (usuario.IDEntidad > 0)
            {
                var entidad = new Entidad {IDEntidad = usuario.IDEntidad.Value};

                Contexto.Entidad.Attach(entidad);

                usuarioModificar.Entidad = entidad;
            }
            else
            {
                usuarioModificar.IDEntidad = null;
                Contexto.Entry(usuarioModificar).Property(p => p.IDEntidad).IsModified = true;
            }
        }
        
        public void CambiarHabilitado(int idUsuario, bool habilitado)
        {
            var eventoCambiarHabilitado = new Usuario { IDUsuario = idUsuario };
            Contexto.Usuario.Attach(eventoCambiarHabilitado);
            eventoCambiarHabilitado.Habilitado = habilitado;
            Contexto.Entry(eventoCambiarHabilitado).Property(r => r.Habilitado).IsModified = true;
        }

        public static List<SelectListItem> Buscar(int idEntidad)
        {
            using (var contexto = new Contexto())
            {
                var usuarios = (from e in contexto.Usuario
                    where e.Habilitado && (idEntidad == 0 || e.IDEntidad == idEntidad)
                    orderby e.ApellidoP + " " + e.ApellidoM + " " + e.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?)e.IDUsuario).Trim(),
                        Text = e.ApellidoP + @" " + e.ApellidoM + @" " + e.Nombre
                    }).ToList();

                if (usuarios.Count > 0)
                {
                    usuarios.Insert(0, new SelectListItem { Text = General.Seleccione, Value = string.Empty });
                }

                return usuarios;
            }
        }
    }
}