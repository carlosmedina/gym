﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Web.Mvc;
using Datos.DTO.Infraestructura;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Recursos;

namespace Datos.Repositorios.Configuracion
{
    public class CuadroArchivisticoRepositorio : Repositorio<CuadroArchivistico>
    {
        public CuadroArchivisticoRepositorio(Contexto contexto) :
            base(contexto)
        {
        }

        public override void Modificar(CuadroArchivistico cuadroarchivistico)
        {
            Contexto.Entry(cuadroarchivistico).State = EntityState.Modified;
            Contexto.Entry(cuadroarchivistico).Property(b => b.FechaCreacion).IsModified = false;
            Contexto.Entry(cuadroarchivistico).Property(b => b.Habilitado).IsModified = false;
        }

        public void CambiarHabilitado(int idCuadroarchivistico, bool habilitado)
        {
            var eventoCambiarHabilitado = new CuadroArchivistico{ IDCuadroArchivistico = idCuadroarchivistico };
            Contexto.CuadroArchivistico.Attach(eventoCambiarHabilitado);
            eventoCambiarHabilitado.Habilitado = habilitado;
            Contexto.Entry(eventoCambiarHabilitado).Property(r => r.Habilitado).IsModified = true;
        }
        
        public List<CuadroArchivisticoBuscar> Buscar(CuadroArchivisticoViewModel criterios)
        {
            var query = ObtenerQuery(criterios, true);

            return (from q in query
                select new CuadroArchivisticoBuscar
                {
                    IDCuadroArchivistico = q.IDCuadroArchivistico,
                    IDPadreCuadroArchivistico = q.IDPadreCuadroArchivistico,
                    Clave = q.Clave,
                    Nombre = q.Nombre,
                    NivelUno = q.PadreCuadroArchivistico,
                    NivelDos = q.PadreCuadroArchivistico.PadreCuadroArchivistico,
                    NivelTres = q.PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico,
                    NivelCuatro = q.PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                   .PadreCuadroArchivistico,
                    NivelCinco = q.PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                  .PadreCuadroArchivistico.PadreCuadroArchivistico,
                    FechaCreacion = q.FechaCreacion,
                    FechaModificacion = q.FechaModificado,
                    Habilitado = q.Habilitado,
                    Agregado = criterios.IdEntidad > 0 && (from e in q.CuadroArchivisticoEntidad
                                   where e.IDEntidad == criterios.IdEntidad
                                   select e).Any()
                }).ToList();
        }

        public CuadroArchivistico BuscarPorIdCuadroArchivistico(int idCuadroArchivistico)
        {
            return (from u in Contexto.CuadroArchivistico.Include(u => u.PadreCuadroArchivistico)
                    .Include(u => u.PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                        .PadreCuadroArchivistico.PadreCuadroArchivistico)
                where u.IDCuadroArchivistico == idCuadroArchivistico
                select u).SingleOrDefault();
        }

        public int ObtenerTotalRegistros(CuadroArchivisticoViewModel criterios)
        {
            return ObtenerQuery(criterios,false).Count();
        }
     
        private IQueryable<CuadroArchivistico> ObtenerQuery(CuadroArchivisticoViewModel criterios, bool paginar)
        {
            IQueryable<CuadroArchivistico> query = Contexto.Set<CuadroArchivistico>();

            query = query.Where(b => b.Habilitado == criterios.CuadroArchivistico.Habilitado);

            if (criterios.CuadroArchivistico.IDPadreCuadroArchivistico > 0)
            {
                query = query.Where(b => b.IDPadreCuadroArchivistico == criterios.CuadroArchivistico.IDPadreCuadroArchivistico);
            }
            else
            {
                query = FiltrarPorAntecesores(query, criterios);
            }
            if (criterios.CuadroArchivistico.IDTipoCuadroArchivistico > 0)
            {
                query = query.Where(b => b.IDTipoCuadroArchivistico == criterios.CuadroArchivistico.IDTipoCuadroArchivistico);
            }
            if (!string.IsNullOrEmpty(criterios.CuadroArchivistico.Clave))
            {
                query = query.Where(b => b.Clave.Contains(criterios.CuadroArchivistico.Clave));
            }
            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.OrderBy(q => q.Clave);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }

            return query;
        }

        #region Filtro por antecesores
        private IQueryable<CuadroArchivistico> FiltrarPorAntecesores(IQueryable<CuadroArchivistico> query, CuadroArchivisticoViewModel criterios)
        {
            var tipo = (TiposCuadroArchivistico) criterios.CuadroArchivistico.IDTipoCuadroArchivistico;

            if (tipo == 0) return query;

            switch (tipo)
            {
                case TiposCuadroArchivistico.Subfondo:
                    query = FiltrarParaSubfondo(query, criterios);
                    break;
                case TiposCuadroArchivistico.Seccion:
                    query = FiltrarParaSeccion(query, criterios);
                    break;
                case TiposCuadroArchivistico.Subseccion:
                    query = FiltrarParaSubseccion(query, criterios);
                    break;
                case TiposCuadroArchivistico.Serie:
                    query = FiltrarParaSerie(query, criterios);
                    break;
                case TiposCuadroArchivistico.Subserie:
                    query = FiltrarParaSubserie(query, criterios);
                    break;
            }

            return query;
        }

        private IQueryable<CuadroArchivistico> FiltrarParaSubserie(IQueryable<CuadroArchivistico> query, CuadroArchivisticoViewModel criterios)
        {
            if (criterios.IdSerie > 0)
            {
                query = query.Where(b => b.IDPadreCuadroArchivistico == criterios.IdSubseccion);
            }
            if (criterios.IdSubseccion > 0)
            {
                query = query.Where(b => b.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSubseccion);
            }
            else if (criterios.IdSeccion > 0)
            {
                query = query.Where(b => b.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSeccion ||
                                         b.PadreCuadroArchivistico.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSeccion);
            }
            else if (criterios.IdSubfondo > 0)
            {
                query = query.Where(b => b.PadreCuadroArchivistico.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSubfondo ||
                                         b.PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSubfondo);
            }
            else if (criterios.IdFondo > 0)
            {
                query = query.Where(b => b.PadreCuadroArchivistico.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdFondo ||
                                         b.PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdFondo ||
                                         b.PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdFondo);
            }

            return query;
        }

        private IQueryable<CuadroArchivistico> FiltrarParaSerie(IQueryable<CuadroArchivistico> query, CuadroArchivisticoViewModel criterios)
        {
            if (criterios.IdSubseccion > 0)
            {
                query = query.Where(b => b.IDPadreCuadroArchivistico == criterios.IdSubseccion);
            }
            else if (criterios.IdSeccion > 0)
            {
                query = query.Where(b => b.IDPadreCuadroArchivistico == criterios.IdSeccion ||
                                         b.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSeccion);
            }
            else if (criterios.IdSubfondo > 0)
            {
                query = query.Where(b => b.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSubfondo ||
                                         b.PadreCuadroArchivistico.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSubfondo);
            }
            else if (criterios.IdFondo > 0)
            {
                query = query.Where(b => b.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdFondo ||
                                         b.PadreCuadroArchivistico.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdFondo ||
                                         b.PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdFondo);
            }

            return query;
        }

        private IQueryable<CuadroArchivistico> FiltrarParaSubseccion(IQueryable<CuadroArchivistico> query, CuadroArchivisticoViewModel criterios)
        {
            if (criterios.IdSeccion > 0)
            {
                query = query.Where(b => b.IDPadreCuadroArchivistico == criterios.IdSeccion);
            }
            else if (criterios.IdSubfondo > 0)
            {
                query = query.Where(b => b.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSubfondo);
            }
            else if (criterios.IdFondo > 0)
            {
                query = query.Where(b => b.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdFondo ||
                                         b.PadreCuadroArchivistico.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdFondo);
            }

            return query;
        }

        private IQueryable<CuadroArchivistico> FiltrarParaSeccion(IQueryable<CuadroArchivistico> query, CuadroArchivisticoViewModel criterios)
        {
            if (criterios.IdSubfondo > 0)
            {
                query = query.Where(b => b.IDPadreCuadroArchivistico == criterios.IdSubfondo);
            }
            else if (criterios.IdFondo > 0)
            {
                query = query.Where(b => b.IDPadreCuadroArchivistico == criterios.IdFondo ||
                                         b.PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdFondo);
            }

            return query;
        }

        private IQueryable<CuadroArchivistico> FiltrarParaSubfondo(IQueryable<CuadroArchivistico> query, CuadroArchivisticoViewModel criterios)
        {
            if (criterios.IdFondo > 0)
            {
                query = query.Where(b => b.IDPadreCuadroArchivistico == criterios.IdFondo);
            }

            return query;
        }
        #endregion  

        public static List<SelectListItem> BuscarTipo(TiposCuadroArchivistico tipo)
        {
            List<SelectListItem> lista;
            using (var bd = new Contexto())
            {
                lista = (from m in bd.CuadroArchivistico
                    where m.IDTipoCuadroArchivistico == (int)tipo && m.Habilitado
                    orderby m.Clave
                    select new SelectListItem
                    {
                        Value = m.IDCuadroArchivistico.ToString(),
                        Text = m.Clave + @" - " + m.Nombre
                    }).ToList();

                if (lista.Count > 0)
                {
                    lista.Insert(0, new SelectListItem { Text = General.Seleccione, Value = string.Empty });
                }
            }
            return lista;
        }

        public static List<SelectListItem> BuscarTipo(TiposCuadroArchivistico tipo, int idEntidad)
        {
            if (idEntidad == 0) return BuscarTipo(tipo);

            List<SelectListItem> lista;
            using (var bd = new Contexto())
            {
                lista = (from m in bd.CuadroArchivistico
                    where m.IDTipoCuadroArchivistico == (int) tipo &&
                          m.Habilitado &&
                          bd.fuTieneEntidadCuadroArchivisticoDescendientes(m.IDCuadroArchivistico, idEntidad).Any()
                         orderby m.Clave
                    select new SelectListItem
                    {
                        Value = m.IDCuadroArchivistico.ToString(),
                        Text = m.Clave + @" - " + m.Nombre
                    }).ToList();

                if (lista.Count > 0)
                {
                    lista.Insert(0, new SelectListItem { Text = General.Seleccione, Value = string.Empty });
                }
            }
            return lista;
        }

        public static List<SelectListItem> Buscar(int idPadreCuadroArchvistico, TiposCuadroArchivistico tipo)
        {
            using (var contexto = new Contexto())
            {
                IQueryable<CuadroArchivistico> query = contexto.Set<CuadroArchivistico>();
                if (tipo > 0)
                {
                    query = query.Where(b => b.IDTipoCuadroArchivistico == (int) tipo);
                }

                if (idPadreCuadroArchvistico > 0)
                {
                    query = query.Where(b => b.IDPadreCuadroArchivistico == (int)idPadreCuadroArchvistico);
                }

                return (from c in query
                    where c.Habilitado
                    orderby c.Clave
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?) c.IDCuadroArchivistico).Trim(),
                        Text = c.Clave + @" - " + c.Nombre
                    }).ToList();
            }
        }

        public static List<SelectListItem> Buscar(int idPadreCuadroArchvistico, TiposCuadroArchivistico tipo, int idEntidad)
        {
            if (idEntidad == 0) return Buscar(idPadreCuadroArchvistico, tipo);

            using (var contexto = new Contexto())
            {
                IQueryable<CuadroArchivistico> query = contexto.Set<CuadroArchivistico>();
                if (tipo > 0)
                {
                    query = query.Where(b => b.IDTipoCuadroArchivistico == (int)tipo);
                }

                if (idPadreCuadroArchvistico > 0)
                {
                    query = query.Where(b => b.IDPadreCuadroArchivistico == (int)idPadreCuadroArchvistico);
                }

                return (from c in query
                    where c.Habilitado &&
                          contexto.fuTieneEntidadCuadroArchivisticoDescendientes(c.IDCuadroArchivistico, idEntidad).Any()
                        orderby c.Clave
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?) c.IDCuadroArchivistico).Trim(),
                        Text = c.Clave + @" - " + c.Nombre
                    }).ToList();
            }
        }

        public void AgregarCuadroArchivisticoEntidad(int idEntidad, int idCuadroArchivistico)
        {
            var entidad = new Entidad { IDEntidad = idEntidad };
            var cuadroEntidad = new CuadroArchivisticoEntidad
            {
                IDEntidad = idEntidad,
                IDCuadroArchivistico = idCuadroArchivistico,
                FechaCreacion = DateTime.Now
            };

            Contexto.Entidad.Attach(entidad);

            entidad.CuadroArchivisticoEntidad.Add(cuadroEntidad);
        }

        public void QuitarCuadroArchivisticoEntidad(int idEntidad, int idCuadroArchivistico)
        {
            var entidad = new Entidad { IDEntidad = idEntidad };
            var cuadroEntidad = new CuadroArchivisticoEntidad { IDEntidad = idEntidad, IDCuadroArchivistico = idCuadroArchivistico };

            entidad.CuadroArchivisticoEntidad.Add(cuadroEntidad);

            Contexto.Entidad.Attach(entidad);

            entidad.CuadroArchivisticoEntidad.Remove(cuadroEntidad);
        }
    }
}
