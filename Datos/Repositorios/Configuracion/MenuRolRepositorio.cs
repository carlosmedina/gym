﻿using System.Collections.Generic;
using System.Linq;

namespace Datos.Repositorios.Configuracion
{
    public class MenuRolRepositorio
    {
        public int Insertar(int idRol, List<int> idOpcionesMenuInsertar)
        {
            var rolAgregarOpciones = new Rol { IDRol = idRol };
            using (var bd = new Contexto())
            {
                bd.Rol.Attach(rolAgregarOpciones);
                var menusSinAcceso = idOpcionesMenuInsertar.Where(idMenu => !PoseeOpcionMenu(idMenu, idRol))
                    .Select(m => new Menu { IDMenu = m }).ToList();
                foreach (var menu in menusSinAcceso)
                {
                    bd.Menu.Attach(menu);
                    rolAgregarOpciones.MenuInicio.Add(menu);
                }
                return bd.SaveChanges();
            }
        }
        
        public int Eliminar(int idRol, List<int> idOpcionesMenuEliminar)
        {
            var rolQuitarOpciones = new Rol { IDRol = idRol };
            using (var bd = new Contexto())
            {
                var menusConAcceso = idOpcionesMenuEliminar.Where(idMenu => PoseeOpcionMenu(idMenu, idRol))
                    .Select(m => new Menu { IDMenu = m }).ToList();
                menusConAcceso.ForEach(m => rolQuitarOpciones.MenuInicio.Add(new Menu { IDMenu = m.IDMenu }));
                bd.Rol.Attach(rolQuitarOpciones);
                foreach (var menu in menusConAcceso)
                {
                    var opcionQuitar = rolQuitarOpciones.MenuInicio.Single(o => o.IDMenu == menu.IDMenu);
                    bd.Menu.Attach(opcionQuitar);
                    rolQuitarOpciones.MenuInicio.Remove(opcionQuitar);
                }
                return bd.SaveChanges();
            }
        }
        
        public List<Menu> BuscarMenu(int idRol, bool soloVisibles)
        {
            using (var bd = new Contexto())
            {
                var opcionesMenu =
                    (from om in bd.Menu
                     where om.Rol.Any(r => r.IDRol == idRol) &&
                           om.Habilitado &&
                           om.Visible == soloVisibles
                     orderby om.Indice ascending
                     select om).ToList();
                return opcionesMenu;
            }
        }
        
        public List<Menu> BuscarPoseeOpcionMenu(int idPadreOpcionMenu, int idRol)
        {
            using (var bd = new Contexto())
            {
                return (from om in bd.Menu
                        where om.Padre == idPadreOpcionMenu &&
                              om.Habilitado
                        orderby om.Indice
                        select om).ToList();
            }
        }
        
        public List<Menu> BuscarPoseeOpcionMenu(int idPadreMenu, string nombreOpcion, int idRol)
        {
            using (var bd = new Contexto())
            {
                return (from om in bd.Menu
                        where om.Padre == idPadreMenu &&
                              om.Opcion.Contains(nombreOpcion) &&
                              om.Habilitado
                        orderby om.Indice
                        select om).ToList();
            }
        }
        
        public bool PoseeOpcionMenu(int idOpcionMenu, int idRol)
        {
            var rol = new Rol { IDRol = idRol };
            using (var bd = new Contexto())
            {
                bd.Rol.Attach(rol);
                var poseeMenuRol = (from om in bd.Menu
                                    where om.Rol.Any(x => x.IDRol == idRol) &&
                                          om.IDMenu == idOpcionMenu &&
                                          om.Habilitado
                                    select om);
                return poseeMenuRol.Any();
            }
        }
        
        public bool TienePermisoPagina(int idRol, string controlador, string accion)
        {
            using (var bd = new Contexto())
            {
                return (from r in bd.Rol
                        where r.IDRol == idRol &&
                              r.MenuInicio.Any(om => om.Controlador == controlador && om.Accion == accion)
                        select r
                       ).Any();
            }
        }
    }
}
