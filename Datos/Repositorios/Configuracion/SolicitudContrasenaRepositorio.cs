﻿using System;
using System.Data.Entity;
using System.Linq;
using Sistema.Extensiones;

namespace Datos.Repositorios.Configuracion
{
    public class SolicitudContrasenaRepositorio : Repositorio<SolicitudContrasena>
    {
        public SolicitudContrasenaRepositorio(Contexto contexto) 
            : base(contexto)
        {
        }

        public void Guardar(int idUsuario, string codigo)
        {
            var solicitudContrasena = new SolicitudContrasena
            {
                IDUsuario = idUsuario,
                Codigo = codigo,
                Usado = false,
                FechaRegistro = DateTime.Now,
                TipoUsuario = 1
            };
            Guardar(solicitudContrasena);
        }

        public Usuario ObtenerUsuario(string codigo)
        {
            return (from sc in Contexto.SolicitudContrasena
                    where sc.Codigo == codigo && !sc.Usado
                    select sc.Usuario).SingleOrDefault();
        }

        public int ObtenerIdUsuario(string codigo)
        {
            return (from sc in Contexto.SolicitudContrasena
                    where sc.Codigo == codigo && !sc.Usado
                    select sc.Usuario.IDUsuario).SingleOrDefault();
        }

        public int RegresoIdContrasena (string codigo)
        {
            return (from sc in Contexto.SolicitudContrasena
                where sc.Codigo == codigo && !sc.Usado
                select sc.IDSolicitudContrasena).SingleOrDefault();
        }

        public void DesactivarCodigo(int idSolicitudContrasena)
        {
            var solicitud = new SolicitudContrasena { IDSolicitudContrasena = idSolicitudContrasena};
            Contexto.SolicitudContrasena.Attach(solicitud);
            solicitud.Usado = true;
        }
    }
}