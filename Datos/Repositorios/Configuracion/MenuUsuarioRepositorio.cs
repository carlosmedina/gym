﻿using System.Collections.Generic;
using System.Linq;

namespace Datos.Repositorios.Configuracion
{
    public class MenuUsuarioRepositorio
    {
        public List<Menu> BuscarMenu(int idUsuario, bool soloVisibles)
        {
            using (var bd = new Contexto())
            {
                return (from om in bd.Menu
                        where om.Habilitado && om.Visible == soloVisibles
                        orderby om.Indice ascending
                        select om).ToList();
            }
        }
        
        public List<Menu> BuscarOpcionMenuPadrePosee(int idPadreOpcionMenu, int idEmpleado)
        {
            using (var bd = new Contexto())
            {
                return (from om in bd.Menu
                        where om.Padre == idPadreOpcionMenu &&
                              om.Habilitado
                        select om).ToList();
            }
        }
        
        public List<Menu> BuscarOpcionMenuPadrePosee(int idPadreOpcionMenu, string nombreOpcion, int idEmpleado)
        {
            using (var bd = new Contexto())
            {
                return (from om in bd.Menu
                        where om.Padre == idPadreOpcionMenu &&
                              om.Opcion.Contains(nombreOpcion) &&
                              om.Habilitado
                        select om).ToList();
            }
        }

        public bool BuscarPoseeOpcionMenu(int idOpcionMenu, int idEmpleado)
        {
            using (var bd = new Contexto())
            {
                return (from om in bd.Menu
                        where om.IDMenu == idOpcionMenu  &&
                              om.Habilitado
                        select om).Any();
            }
        }
        
        public int Insertar(int idUsuario, List<int> idOpcionesMenu)
        {
            var usuario = new Usuario { IDUsuario = idUsuario };
            using (var bd = new Contexto())
            {
                bd.Usuario.Attach(usuario);
                var menusSinAcceso = idOpcionesMenu.Where(idMenu => !BuscarPoseeOpcionMenu(idMenu, idUsuario))
                    .Select(m => new Menu() { IDMenu = m }).ToList();
                foreach (var menu in menusSinAcceso)
                {
                    bd.Menu.Attach(menu);
                }
                return bd.SaveChanges();
            }
        }
        
        public int Eliminar(int idUsuario, List<int> idOpcionesMenu)
        {
            var usuario = new Usuario { IDUsuario = idUsuario };
            using (var bd = new Contexto())
            {
                bd.Usuario.Attach(usuario);
                return bd.SaveChanges();
            }
        }

        public int AgregarOpcionMenu(int idUsuario, List<int> idOpcionesMenu)
        {
            var usuario = new Usuario { IDUsuario = idUsuario };
            using (var bd = new Contexto())
            {
                bd.Usuario.Attach(usuario);
                foreach (var opcionAgregar in idOpcionesMenu.Select(idOpcion => new Menu { IDMenu = idOpcion }))
                {
                    bd.Menu.Attach(opcionAgregar);
                }
                return bd.SaveChanges();
            }
        }

        public int QuitarOpcionMenu(int idEmpleado, List<int> idOpcionesMenu)
        {
            var usuario = new Usuario { IDUsuario = idEmpleado };
            using (var bd = new Contexto())
            {
                bd.Usuario.Attach(usuario);
                return bd.SaveChanges();
            }
        }

        public bool TieneMenuPersonalizado(int idUsuario)
        {
            using (var bd = new Contexto())
            {
                return (from p in bd.Usuario
                        where p.IDUsuario == idUsuario
                        select p.MenuPersonalizado).FirstOrDefault();
            }
        }
    }
}
