﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Datos.Repositorios.Reportes
{
    public class ReportesRepositorio
    {
        private readonly Contexto _contexto;

        public ReportesRepositorio(Contexto contexo)
        {
            _contexto = contexo;
        }

        public int ObtenerTotalDigitalizados(int idEntidad, DateTime fechaInicial, DateTime fechaFinal)
        {
            var resultado = _contexto.spObtenerNumerosExpedientesDigitalizadosMes(idEntidad, fechaInicial, fechaFinal);

            var total = resultado.ToList().Sum(r => r.Cantidad);

            return total ?? 0;
        }

        public List<SpResultadoConsultasExpedientesMes> ObtenerMesesDigitalizados(int idEntidad, DateTime fechaInicial, DateTime fechaFinal)
        {
            var resultado = _contexto.spObtenerNumerosExpedientesDigitalizadosMes(idEntidad, fechaInicial, fechaFinal);

            return resultado.ToList();
        }

        public int ObtenerDigitalizadosPromedioDia(int idEntidad, DateTime fechaInicial, DateTime fechaFinal)
        {
            var resultado = _contexto.spObtenerPromedioDigitalizacionDia(idEntidad, fechaInicial, fechaFinal);

            var total = resultado.SingleOrDefault();

            return total ?? 0;
        }

        public int ObtenerPromedioConsultasDiarias(int idEntidad)
        {
            var resultado = _contexto.spObtenerPromedioConsultasDia(idEntidad);

            var total = resultado.SingleOrDefault();

            return total ?? 0;
        }

        public int ObtenerPromedioConsultasMeses(int idEntidad)
        {
            var resultado = _contexto.spObtenerPromedioConsultasMes(idEntidad);

            var total = resultado.SingleOrDefault();

            return total ?? 0;
        }

        public List<SpResultadoMasConsultadoExpediente> ObtenerExpedientesMasConsultados(int idEntidad, DateTime fechaInicial,
            DateTime fechaFinal)
        {
            var resultado = _contexto.spObtenerMasConsultadosExpedientesIntervalo(idEntidad, fechaInicial, fechaFinal);

            return resultado.ToList();
        }

        public List<SpResultadoMasConsultadoExpediente> ObtenerExpedientesMasConsultados(int idEntidad)
        {
            var resultado = _contexto.spObtenerMasConsultadosExpedientes(idEntidad);

            return resultado.ToList();
        }

        public int ObtenerTotalExpedientes(int idEntidad)
        {
            var query = from e in _contexto.Expediente
                select e;

            if (idEntidad > 0)
            {
                query = query.Where(q => q.IDEntidad == idEntidad);
            }

            return query.Count();
        }

        public List<SpResultadoConsultasExpedientesMes> ObtenerMesesConsultasExpediente(int idExpediente, DateTime fechaInicial, DateTime fechaFinal)
        {
            var resultado = _contexto.spObtenerExpedienteConsultasPorMes(idExpediente, fechaInicial, fechaFinal);

            return resultado.ToList();
        }

        public List<SpResultadoConsultasExpedientesMes> ObtenerUsuarioMesesConsultasExpediente(int idUsuario, DateTime fechaInicial, DateTime fechaFinal)
        {
            var resultado = _contexto.spObtenerUsuarioNumerosExpedientesDigitalizadosMes(idUsuario, fechaInicial, fechaFinal);

            return resultado.ToList();
        }

        public int ObtenerUsuarioPromedioDigitalizacion(int idUsuario)
        {
            var resultado = _contexto.spObtenerUsuarioPromedioDigitalizacionDia(idUsuario);

            return resultado.SingleOrDefault() ?? 0;
        }
    }
}
