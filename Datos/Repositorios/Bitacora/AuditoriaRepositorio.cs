﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Recursos;

namespace Datos.Repositorios.Bitacora
{
    public class AuditoriaRepositorio: Repositorio<Auditoria>
    {
        public AuditoriaRepositorio(Contexto contexto) :
            base(contexto)
        {
        }

        public List<Auditoria> Buscar(AuditoriaViewModel criterios)
        {
            return ObtenerQuery(criterios, true).ToList();
        }

        public int ObtenerTotalRegistros(AuditoriaViewModel criterios)
        {
            return ObtenerQuery(criterios, false).Count();
        }

        private IQueryable<Auditoria> ObtenerQuery(AuditoriaViewModel criterios, bool paginar)
        {
            IQueryable<Auditoria> query = Contexto.Set<Auditoria>();

            query = query.Include(q => q.AccionAuditoria);
            query = query.Include(q => q.Usuario);
            query = query.Include(q => q.ModuloAuditoria);

            if (criterios.Auditoria.IDUsuario > 0)
            {
                query = query.Where(q => q.Usuario.IDUsuario == criterios.Auditoria.IDUsuario);
            }
            if (criterios.Auditoria.IDExpediente > 0)
            {
                query = query.Where(q => q.IDExpediente == criterios.Auditoria.IDExpediente);
            }
            if (criterios.Auditoria.IDAccionAuditoria > 0)
            {
                query = query.Where(q => q.AccionAuditoria.IDAccionAuditoria == criterios.Auditoria.IDAccionAuditoria);
            }
            if (criterios.Auditoria.IDModuloAuditoria > 0)
            {
                query = query.Where(q => q.ModuloAuditoria.IDModuloAuditoria == criterios.Auditoria.IDModuloAuditoria);
            }
            if (criterios.FechaInicio.HasValue)
            {
                query = query.Where(b => b.Fecha >= criterios.FechaInicio);
            }
            if (criterios.FechaFin.HasValue)
            {
                query = query.Where(b => b.Fecha <= criterios.FechaFin);
            }

            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.OrderByDescending(q => q.IDAuditoria);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }

            return query;
        }

        public static List<SelectListItem> BuscarUsuario()
        {
            List<SelectListItem> lista;
            using (var bd = new Contexto())
            {
                lista = (from m in bd.Usuario
                    where m.Habilitado
                    orderby m.Nombre
                    select new SelectListItem
                    {
                        Value = m.IDUsuario.ToString(),
                        Text = m.Nombre + @" " + m.ApellidoM + @" " + m.ApellidoP
                    }).ToList();

                if (lista.Count > 0)
                {
                    lista.Insert(0, new SelectListItem { Text = General.Seleccione, Value = string.Empty });
                }
            }
            return lista;
        }

        public static List<SelectListItem> BuscarAcciones()
        {
            List<SelectListItem> lista;
            using (var bd = new Contexto())
            {
                lista = (from m in bd.AccionAuditoria
                    orderby m.Nombre
                    select new SelectListItem
                    {
                        Value = m.IDAccionAuditoria.ToString(),
                        Text = m.Nombre
                    }).ToList();

                if (lista.Count > 0)
                {
                    lista.Insert(0, new SelectListItem { Text = General.Seleccione, Value = string.Empty });
                }
            }
            return lista;
        }

        public static List<SelectListItem> BuscarExpedienteAcciones()
        {
            var lista = new List<SelectListItem>
            {
                new SelectListItem { Text = General.Seleccione, Value = string.Empty },
                new SelectListItem
                {
                    Text = AccionesAuditoria.Consulta.ToString(),
                    Value = ((int) AccionesAuditoria.Consulta).ToString()
                },
                new SelectListItem
                {
                    Text = AccionesAuditoria.Guardar.ToString(),
                    Value = ((int) AccionesAuditoria.Guardar).ToString()
                },
                new SelectListItem
                {
                    Text = AccionesAuditoria.Modificar.ToString(),
                    Value = ((int) AccionesAuditoria.Guardar).ToString()
                }
            };

            return lista;
        }

        public static List<SelectListItem> BuscarModulos()
        {
            List<SelectListItem> lista;
            using (var bd = new Contexto())
            {
                lista = (from m in bd.ModuloAuditoria
                         orderby m.Nombre
                         select new SelectListItem
                         {
                             Value = m.IDModuloAuditoria.ToString(),
                             Text = m.Nombre
                         }).ToList();

                if (lista.Count > 0)
                {
                    lista.Insert(0, new SelectListItem { Text = General.Seleccione, Value = string.Empty });
                }
            }
            return lista;
        }

        public Auditoria BuscarPorIdAuditoria(int idAuditoria)
        {            
            var test = (from r in Contexto.Auditoria
                    .Include(x => x.AccionAuditoria)
                    .Include(x => x.Usuario)
                    .Include(x => x.ModuloAuditoria)
                        where r.IDAuditoria == idAuditoria
                select r).SingleOrDefault();
            return test;
        }
    }
}
