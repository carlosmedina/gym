﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Datos.DTO.Infraestructura;
using Datos.DTO.Infraestructura.Reportes;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Recursos;

namespace Datos.Repositorios.Catalogos
{
    public class CadidoRepositorio : Repositorio<Cadido>
    {
        public CadidoRepositorio(Contexto contexto) :
            base(contexto)
        {
        }

        public override void Modificar(Cadido cadido)
        {
            base.Modificar(cadido);

            Contexto.Entry(cadido).Property(c => c.Habilitado).IsModified = false;
            Contexto.Entry(cadido).Property(c => c.FechaCreacion).IsModified = false;

            if (cadido.IDClasificacionFundamentoLegal.HasValue)
            {
                var fundamentoLegal = new FundamentoLegal
                {
                    IDFundamentoLegal = cadido.IDClasificacionFundamentoLegal.Value
                };

                Contexto.FundamentoLegal.Attach(fundamentoLegal);

                cadido.FundamentoLegal = fundamentoLegal;
            }
            else
            {
                Contexto.Entry(cadido).Property(c => c.IDClasificacionFundamentoLegal).IsModified = true;
            }
        }

        public Cadido BuscarPorIdCadido(object idCadido)
        {
            var test = (from r in Contexto.Cadido
                    .Include(x => x.CuadroArchivistico.TipoCuadroArchivistico)
                    .Include(x => x.CuadroArchivistico)
                    .Include(x => x.TipoClasificacionInformacion)
                    .Include(x => x.FundamentoLegal)
                    .Include(x => x.DisposicionDocumental)
                    .Include(x => x.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                    .PadreCuadroArchivistico.PadreCuadroArchivistico)                                                   
                where r.IDCadido == (int)idCadido
                select r).SingleOrDefault();
            return test;
        }

        public void CambiarHabilitado(int idCadido, bool habilitado)
        {
            var eventoCambiarHabilitado = new Cadido { IDCadido = idCadido };
            Contexto.Cadido.Attach(eventoCambiarHabilitado);
            eventoCambiarHabilitado.Habilitado = habilitado;
            Contexto.Entry(eventoCambiarHabilitado).Property(r => r.Habilitado).IsModified = true;
        }

        public List<Cadido> Buscar(CadidoViewModel criterios)
        {
            return ObtenerQuery(criterios, true).ToList();
        }

        public int ObtenerTotalRegistros(CadidoViewModel criterios)
        {
            return ObtenerQuery(criterios, false).Count();
        }

        private IQueryable<Cadido> ObtenerQuery(CadidoViewModel criterios, bool paginar)
        {
            IQueryable<Cadido> query = Contexto.Set<Cadido>();

            query = query.Where(b => b.Habilitado == criterios.Cadido.Habilitado);
            query = query.Include(b => b.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                .PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico);

            if (criterios.IdSubserie > 0)
            {
                query = query.Where(b => b.IDCuadroArchivistico == criterios.IdSubserie);
            }
            else if (criterios.IdSerie > 0)
            {
                query = query.Where(b => b.IDCuadroArchivistico == criterios.IdSerie ||
                                         b.CuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSerie);
            }
            else if (criterios.IdSubseccion > 0)
            {
                query = query.Where(
                    b => b.CuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSubseccion ||
                         b.CuadroArchivistico.PadreCuadroArchivistico.IDPadreCuadroArchivistico ==
                         criterios.IdSubseccion);
            }
            else if (criterios.IdSeccion > 0)
            {
                query = query.Where(b => b.CuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSeccion ||
                                         b.CuadroArchivistico.PadreCuadroArchivistico
                                             .IDPadreCuadroArchivistico == criterios.IdSeccion ||
                                         b.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .IDPadreCuadroArchivistico == criterios.IdSeccion);
            }
            else if (criterios.IdSubfondo > 0)
            {
                query = query.Where(b => b.CuadroArchivistico
                                             .PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSubfondo ||
                                         b.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .IDPadreCuadroArchivistico == criterios.IdSubfondo ||
                                         b.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSubfondo);
            }
            else if (criterios.IdFondo > 0)
            {
                query = query.Where(b => b.CuadroArchivistico
                                             .PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdFondo ||
                                         b.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .IDPadreCuadroArchivistico == criterios.IdFondo ||
                                         b.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdFondo ||
                                         b.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .IDPadreCuadroArchivistico == criterios.IdFondo);
            }                        

            if (criterios.Cadido.EsAdministrativo == false)
            {
                query = query.Where(b => b.EsAdministrativo == false);
            }
            if (criterios.Cadido.EsLegal == false)
            {
                query = query.Where(b => b.EsLegal == false);
            }
            if (criterios.Cadido.EsContableFiscal == false)
            {
                query = query.Where(b => b.EsContableFiscal == false);
            }

            if (criterios.Cadido.IDTipoClasificacionInformacion > 0)
            {
                query = query.Where(b => b.IDTipoClasificacionInformacion == criterios.Cadido.IDTipoClasificacionInformacion);
            }
            if (criterios.Cadido.IDDisposicionDocumental > 0)
            {
                query = query.Where(b => b.IDDisposicionDocumental == criterios.Cadido.IDDisposicionDocumental);
            }

            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.Include(b => b.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                    .PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico);

                query = query.OrderBy(q => q.IDCadido);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }

            return query;
        }

        public int ObtenerIdCuadroArchivistico(int idCadido)
        {
            return (from c in Contexto.Cadido
                where c.IDCadido == idCadido
                select c.IDCuadroArchivistico).Single();
        }

        public Cadido ObtenerVigenciaMasLejos(int idCuadroArchvistico)
        {
            return (from c in Contexto.Cadido.Include(i => i.FundamentoLegal)
                    .Include(i => i.TipoClasificacionInformacion)
                    .Include(i => i.DisposicionDocumental)
                where c.IDCuadroArchivistico == idCuadroArchvistico                      
                select c).FirstOrDefault();
        }

        public static List<SelectListItem> BuscarFundamentoLegal()
        {
            List<SelectListItem> lista;
            using (var bd = new Contexto())
            {
                lista = (from m in bd.FundamentoLegal
                    orderby m.Nombre
                    select new SelectListItem
                    {
                        Value = m.IDFundamentoLegal.ToString(),
                        Text = m.Nombre
                    }).ToList();

                if (lista.Count > 0)
                {
                    lista.Insert(0, new SelectListItem { Text = General.Seleccione, Value = string.Empty });
                }
            }
            return lista;
        }

        public List<ReporteCadido> ObtenerReporte(CadidoViewModel criterios)
        {
            var query = ObtenerQuery(criterios, false);

            return (from q in query
                join ce in Contexto.CuadroArchivisticoEntidad on q.IDCuadroArchivistico equals ce.IDCuadroArchivistico
                into cadidoEntidad
                from ce in cadidoEntidad.DefaultIfEmpty()
                where criterios.IdEntidad == 0 || ce.IDEntidad == criterios.IdEntidad
                orderby q.CuadroArchivistico.Clave
                select new ReporteCadido
                {
                    Clave = q.CuadroArchivistico.Clave,
                    Nombre = q.CuadroArchivistico.Nombre,
                    EsAdministrativo = q.EsAdministrativo,
                    EsContableFiscal = q.EsContableFiscal,
                    EsLegal = q.EsLegal,
                    IDDisposicionDocumental = q.IDDisposicionDocumental,
                    IDPadreCuadroArchivistico = q.CuadroArchivistico.IDPadreCuadroArchivistico ?? 0,
                    PadreClave = q.CuadroArchivistico.PadreCuadroArchivistico.Clave,
                    PadreNombre = q.CuadroArchivistico.PadreCuadroArchivistico.Nombre,
                    IDTipoCuadroArchivistico = q.CuadroArchivistico.IDTipoCuadroArchivistico,
                    TiempoTramite = q.TiempoTramite,
                    TiempoConcentracion = q.TiempoConcentracion
                }).ToList();
        }
    }
}
