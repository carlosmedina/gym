﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web.Mvc;
using Datos.DTO.Infraestructura.ViewModels;

namespace Datos.Repositorios.Catalogos
{
    public class LocalidadRepositorio : Repositorio<Localidad>
    {
        public LocalidadRepositorio(Contexto contexto) : base(contexto)
        {
        }

        public override void Modificar(Localidad localidad)
        {
            Contexto.Entry(localidad).State = EntityState.Modified;

            Contexto.Entry(localidad).Property(p => p.FechaCreacion).IsModified = false;
        }
        
        public List<Localidad> Buscar(LocalidadViewModel localidadViewModel)
        {
            return ObtenerQuery(localidadViewModel, true).ToList();
        }
        
        public int ObtenerTotalRegistros(LocalidadViewModel coloniaViewModel)
        {
            return ObtenerQuery(coloniaViewModel, false).Count();
        }

        public IQueryable<Localidad> ObtenerQuery(LocalidadViewModel criterios, bool paginar)
        {
            IQueryable<Localidad> query = Contexto.Set<Localidad>();

            query = query.Where(c => c.Habilitado == criterios.Localidad.Habilitado);

            if (!string.IsNullOrEmpty(criterios.Localidad.Nombre))
            {
                query = query.Where(c => c.Nombre.Contains(criterios.Localidad.Nombre));
            }
            if (criterios.Localidad.Municipio.IDEstado > 0)
            {
                query = query.Where(c => c.IDEstado == criterios.Localidad.Municipio.IDEstado);
            }
            if (criterios.Localidad.Municipio.IDMunicipio > 0)
            {
                query = query.Where(c => c.IDMunicipio == criterios.Localidad.Municipio.IDMunicipio);
            }
            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.OrderBy(q => q.Nombre + "-" +
                                           q.Municipio.Nombre);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }

            query = query.Include(c => c.Municipio);
            query = query.Include(c => c.Estado);

            return query;
        }
        
        public Localidad BuscarPorId(int idLocalidad)
        {
            return (from c in Contexto.Localidad.Include(x => x.Municipio).Include(x => x.Estado)
                    where c.IDLocalidad == idLocalidad
                    select c).FirstOrDefault();
        }
        
        public static List<SelectListItem> Buscar(int idEstado, int idMunicipio, string buscar, int idEntidad)
        {
            if (idEntidad == 0) return Buscar(idEstado, idMunicipio, buscar);

            List<SelectListItem> lista;
            using (var bd = new Contexto())
            {
                lista = (from l in bd.Localidad
                    where l.IDMunicipio == idMunicipio &&
                          l.IDEstado == idEstado &&
                          l.Habilitado &&
                          l.Nombre.Contains(buscar) &&
                          (from c in bd.Contenedores
                              join ec in bd.ContenedorEntidad on c.IDContenedor equals ec.IDContenedor
                              join m in bd.Mobilarios on c.IDMobilario equals m.IDMobilario
                              join a in bd.Areas on m.IDArea equals a.IDArea
                              join n in bd.Niveles on a.IDNivel equals n.IDNivel
                              join e in bd.Edificios on n.IDEdificio equals e.IDEdificio
                              where l.IDLocalidad == e.IDLocalidad && ec.IDEntidad == idEntidad
                              select c).Any()
                    orderby l.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?) l.IDLocalidad).Trim(),
                        Text = l.Nombre
                    }).ToList();
            }
            return lista;
        }

        public static List<SelectListItem> Buscar(int idEstado, int idMunicipio, string buscar)
        {
            List<SelectListItem> lista;
            using (var bd = new Contexto())
            {
                lista = (from l in bd.Localidad
                    where l.IDMunicipio == idMunicipio &&
                          l.IDEstado == idEstado &&
                          l.Habilitado &&
                          l.Nombre.Contains(buscar)
                    orderby l.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?)l.IDLocalidad).Trim(),
                        Text = l.Nombre
                    }).ToList();
            }
            return lista;
        }

        public void CambiarHabilitado(int idLocalidad, bool habilitado)
        {
            var localidad = Contexto.Localidad.Find(idLocalidad);
            Contexto.Entry(localidad).State = EntityState.Detached;
            localidad.Habilitado = habilitado;
            localidad.FechaModificacion = DateTime.Now;
            Contexto.Entry(localidad).State = EntityState.Modified;
        }
    }
}
