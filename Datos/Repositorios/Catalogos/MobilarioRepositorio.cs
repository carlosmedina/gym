﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web.Mvc;
using Datos.DTO.Infraestructura.ViewModels;

namespace Datos.Repositorios.Catalogos
{
    public class MobilarioRepositorio : Repositorio<Mobilario>
    {
        public MobilarioRepositorio(Contexto contexto) : base(contexto)
        {
        }

        public override void Modificar(Mobilario mobilario)
        {
            Contexto.Entry(mobilario).State = EntityState.Unchanged;

            Contexto.Entry(mobilario).Property(p => p.Nombre).IsModified = true;
            Contexto.Entry(mobilario).Property(p => p.IDArea).IsModified = true;
            Contexto.Entry(mobilario).Property(p => p.FechaModificacion).IsModified = true;
        }

        public List<Mobilario> Buscar(MobilarioViewModel criterios)
        {
            return ObtenerQuery(criterios, true).ToList();
        }

        public int ObtenerTotalRegistros(MobilarioViewModel criterios)
        {
            return ObtenerQuery(criterios, false).Count();
        }

        public IQueryable<Mobilario> ObtenerQuery(MobilarioViewModel criterios, bool paginar)
        {
            IQueryable<Mobilario> query = Contexto.Set<Mobilario>();

            query = query.Where(c => c.Habilitado == criterios.Mobilario.Habilitado);

            if (!string.IsNullOrEmpty(criterios.Mobilario.Nombre))
            {
                query = query.Where(c => c.Nombre.Contains(criterios.Mobilario.Nombre));
            }
            if (criterios.Mobilario.Area.IDArea > 0)
            {
                query = query.Where(c => c.IDArea == criterios.Mobilario.Area.IDArea);
            }
            if (criterios.Mobilario.Area.IDNivel > 0)
            {
                query = query.Where(c => c.Area.IDNivel == criterios.Mobilario.Area.IDNivel);
            }
            if (criterios.Mobilario.Area.Nivel.IDEdificio > 0)
            {
                query = query.Where(c => c.Area.Nivel.IDEdificio == criterios.Mobilario.Area.Nivel.IDEdificio);
            }
            if (criterios.Mobilario.Area.Nivel.Edificio.IDLocalidad > 0)
            {
                query = query.Where(c => c.Area.Nivel.Edificio.IDLocalidad == criterios.Mobilario.Area.Nivel.Edificio.IDLocalidad);
            }
            if (criterios.Mobilario.Area.Nivel.Edificio.Localidad.IDMunicipio > 0)
            {
                query = query.Where(c => c.Area.Nivel.Edificio.Localidad.IDMunicipio ==
                                         criterios.Mobilario.Area.Nivel.Edificio.Localidad.IDMunicipio);
            }
            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.OrderBy(q => q.Nombre + "-" +
                                           q.Area.Nombre + "-" +
                                           q.Area.Nivel.Nombre + "-" +
                                           q.Area.Nivel.Edificio.Nombre + "-" +
                                           q.Area.Nivel.Edificio.Localidad.Nombre + "-" +
                                           q.Area.Nivel.Edificio.Localidad.Municipio.Nombre);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }

            query = query.Include(c => c.Area.Nivel.Edificio.Localidad.Municipio);

            return query;
        }

        public void CambiarHabilitado(int idMobilario, bool habilitado)
        {
            var mobilario = new Mobilario {IDMobilario = idMobilario };

            Contexto.Mobilarios.Attach(mobilario);

            mobilario.Habilitado = habilitado;
            mobilario.FechaModificacion = DateTime.Now;

            Contexto.Entry(mobilario).Property(p => p.Habilitado).IsModified = true;
        }

        public static List<SelectListItem> Buscar(int idArea, string buscar)
        {
            using (var contexto = new Contexto())
            {
                return (from m in contexto.Mobilarios
                    where m.IDArea == idArea &&
                          m.Nombre.Contains(buscar) &&
                          m.Habilitado
                    orderby m.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?)m.IDMobilario).Trim(),
                        Text = m.Nombre
                    }).ToList();
            }
        }

        public static List<SelectListItem> Buscar(int idArea, string buscar, int idEntidad)
        {
            if (idEntidad == 0) return Buscar(idArea, buscar);

            using (var contexto = new Contexto())
            {
                return (from m in contexto.Mobilarios
                    where m.IDArea == idArea &&
                          m.Nombre.Contains(buscar) &&
                          m.Habilitado &&
                          (from c in contexto.Contenedores
                              join ec in contexto.ContenedorEntidad on c.IDContenedor equals ec.IDContenedor
                              where c.IDMobilario == m.IDMobilario && ec.IDEntidad == idEntidad
                              select c).Any()
                    orderby m.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?) m.IDMobilario).Trim(),
                        Text = m.Nombre
                    }).ToList();
            }
        }
    }
}
