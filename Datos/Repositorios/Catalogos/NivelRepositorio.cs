﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web.Mvc;
using Datos.DTO.Infraestructura.ViewModels;

namespace Datos.Repositorios.Catalogos
{
    public class NivelRepositorio : Repositorio<Nivel>
    {
        public NivelRepositorio(Contexto contexto) : base(contexto)
        {
        }

        public override void Modificar(Nivel nivel)
        {
            Contexto.Entry(nivel).State = EntityState.Unchanged;

            Contexto.Entry(nivel).Property(p => p.Nombre).IsModified = true;
            Contexto.Entry(nivel).Property(p => p.IDEdificio).IsModified = true;
            Contexto.Entry(nivel).Property(p => p.FechaModificacion).IsModified = true;
        }

        public List<Nivel> Buscar(NivelViewModel criterios)
        {
            return ObtenerQuery(criterios, true).ToList();
        }

        public int ObtenerTotalRegistros(NivelViewModel criterios)
        {
            return ObtenerQuery(criterios, false).Count();
        }

        public IQueryable<Nivel> ObtenerQuery(NivelViewModel criterios, bool paginar)
        {
            IQueryable<Nivel> query = Contexto.Set<Nivel>();

            query = query.Where(c => c.Habilitado == criterios.Nivel.Habilitado);

            if (!string.IsNullOrEmpty(criterios.Nivel.Nombre))
            {
                query = query.Where(c => c.Nombre.Contains(criterios.Nivel.Nombre));
            }
            if (criterios.Nivel.Edificio.IDEdificio > 0)
            {
                query = query.Where(c => c.IDEdificio == criterios.Nivel.Edificio.IDEdificio);
            }
            if (criterios.Nivel.Edificio.IDLocalidad > 0)
            {
                query = query.Where(c => c.Edificio.IDLocalidad == criterios.Nivel.Edificio.IDLocalidad);
            }
            if (criterios.Nivel.Edificio.Localidad.IDMunicipio > 0)
            {
                query = query.Where(c => c.Edificio.Localidad.IDMunicipio ==
                                         criterios.Nivel.Edificio.Localidad.IDMunicipio);
            }
            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.OrderBy(q => q.Nombre + "-" +
                                           q.Edificio.Nombre + "-" +
                                           q.Edificio.Localidad.Nombre + "-" +
                                           q.Edificio.Localidad.Municipio.Nombre);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }

            query = query.Include(c => c.Edificio.Localidad.Municipio);

            return query;
        }

        public void CambiarHabilitado(int idNivel, bool habilitado)
        {
            var nivel = new Nivel {IDNivel = idNivel};

            Contexto.Niveles.Attach(nivel);

            nivel.Habilitado = habilitado;
            nivel.FechaModificacion = DateTime.Now;

            Contexto.Entry(nivel).Property(p => p.Habilitado).IsModified = true;
        }

        public static List<SelectListItem> Buscar(int idEdificio, string buscar)
        {
            using (var contexto = new Contexto())
            {
                return (from c in contexto.Niveles
                    where c.IDEdificio == idEdificio
                          && c.Nombre.Contains(buscar)
                    orderby c.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?)c.IDNivel).Trim(),
                        Text = c.Nombre
                    }).ToList();
            }
        }

        public static List<SelectListItem> Buscar(int idEdificio, string buscar, int idEntidad)
        {
            if (idEntidad == 0) return Buscar(idEdificio, buscar);

            using (var contexto = new Contexto())
            {
                return (from n in contexto.Niveles
                    where n.IDEdificio == idEdificio &&
                          n.Nombre.Contains(buscar) &&
                          (from c in contexto.Contenedores
                              join ec in contexto.ContenedorEntidad on c.IDContenedor equals ec.IDContenedor
                              join m in contexto.Mobilarios on c.IDMobilario equals m.IDMobilario
                              join a in contexto.Areas on m.IDArea equals a.IDArea
                              where a.IDNivel == n.IDNivel && ec.IDEntidad == idEntidad
                              select c).Any()
                    orderby n.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?) n.IDNivel).Trim(),
                        Text = n.Nombre
                    }).ToList();
            }
        }
    }
}
