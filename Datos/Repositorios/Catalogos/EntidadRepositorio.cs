﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web.Mvc;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Recursos;

namespace Datos.Repositorios.Catalogos
{
    public class EntidadRepositorio : Repositorio<Entidad>
    {
        public EntidadRepositorio(Contexto contexto) : base(contexto)
        {
        }

        public override void Modificar(Entidad entidad)
        {
            Contexto.Entry(entidad).State = EntityState.Modified;

            Contexto.Entry(entidad).Property(p => p.FechaCreacion).IsModified = false;
            Contexto.Entry(entidad).Property(p => p.IDEntidadPadre).IsModified = false;
            Contexto.Entry(entidad).Property(p => p.IDLocalidad).IsModified = false;
        }

        public List<Entidad> Buscar(EntidadViewModel criterios)
        {
            return ObtenerQuery(criterios, true).ToList();
        }

        public int ObtenerTotalRegistros(EntidadViewModel criterios)
        {
            return ObtenerQuery(criterios, false).Count();
        }

        public IQueryable<Entidad> ObtenerQuery(EntidadViewModel criterios, bool paginar)
        {
            IQueryable<Entidad> query = Contexto.Set<Entidad>();

            query = query.Where(c => c.Habilitado == criterios.Entidad.Habilitado);

            if (!string.IsNullOrEmpty(criterios.Entidad.Nombre))
            {
                query = query.Where(c => c.Nombre.Contains(criterios.Entidad.Nombre) ||
                                         c.Responsable.Contains(criterios.Entidad.Nombre) ||
                                         c.Puesto.Contains(criterios.Entidad.Nombre));
            }

            if (criterios.Entidad.IDEntidadPadre == null || criterios.Entidad.IDEntidadPadre > 0)
            {
                query = query.Where(c => c.IDEntidadPadre == criterios.Entidad.IDEntidadPadre);
            }

            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.OrderBy(q => q.Nombre);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }

            query = query.Include(q => q.Localidad);
            
            return query;
        }

        public void CambiarHabilitado(int idEntidad, bool habilitado)
        {
            var entidad = new Entidad {IDEntidad = idEntidad};
            Contexto.Entry(entidad).State = EntityState.Unchanged;

            entidad.Habilitado = habilitado;
            entidad.FechaModificacion = DateTime.Now;

            Contexto.Entry(entidad).Property(p => p.Habilitado).IsModified = true;
        }

        public static List<SelectListItem> Buscar(string buscar)
        {
            using (var contexto = new Contexto())
            {
                var entidades = (from e in contexto.Entidad
                    where e.Nombre.Contains(buscar) && e.Habilitado
                    orderby e.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?) e.IDEntidad).Trim(),
                        Text = e.Nombre
                    }).ToList();

                if (entidades.Count > 0)
                {
                    entidades.Insert(0, new SelectListItem { Text = General.Seleccione, Value = string.Empty });
                }

                return entidades;
            }
        }
    }
}
