﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web.Mvc;
using Datos.DTO.Infraestructura.ViewModels;

namespace Datos.Repositorios.Catalogos
{
    public class ContenedorRepositorio : Repositorio<Contenedor>
    {
        public ContenedorRepositorio(Contexto contexto) : base(contexto)
        {
        }

        public override void Modificar(Contenedor contenedor)
        {
            Contexto.Entry(contenedor).State = EntityState.Unchanged;

            Contexto.Entry(contenedor).Property(p => p.Nombre).IsModified = true;
            Contexto.Entry(contenedor).Property(p => p.IDMobilario).IsModified = true;
            Contexto.Entry(contenedor).Property(p => p.FechaModificacion).IsModified = true;
        }

        public List<ContenedorBuscarViewModel> Buscar(ContenedorViewModel criterios)
        {
            var query = ObtenerQuery(criterios, true).ToList();

            if (criterios.IdEntidad > 0)
            {
                return (from q in query
                    select new ContenedorBuscarViewModel
                    {
                        IDContenedor = q.IDContenedor,
                        Nombre = q.Nombre,
                        Mobilario = q.Mobilario.Nombre,
                        Area = q.Mobilario.Area.Nombre,
                        Nivel = q.Mobilario.Area.Nivel.Nombre,
                        Edificio = q.Mobilario.Area.Nivel.Edificio.Nombre,
                        Localidad = q.Mobilario.Area.Nivel.Edificio.Localidad.Nombre,
                        Municipio = q.Mobilario.Area.Nivel.Edificio.Localidad.Municipio.Nombre,
                        Agregado = (from c in Contexto.ContenedorEntidad
                            where c.IDEntidad == criterios.IdEntidad &&
                                  c.IDContenedor == q.IDContenedor
                            select c).Any(),
                        Habilitado = q.Habilitado
                    }).ToList();
            }

            return (from q in query
                select new ContenedorBuscarViewModel
                {
                    IDContenedor = q.IDContenedor,
                    Nombre = q.Nombre,
                    Mobilario = q.Mobilario.Nombre,
                    Area = q.Mobilario.Area.Nombre,
                    Nivel = q.Mobilario.Area.Nivel.Nombre,
                    Edificio = q.Mobilario.Area.Nivel.Edificio.Nombre,
                    Localidad = q.Mobilario.Area.Nivel.Edificio.Localidad.Nombre,
                    Municipio = q.Mobilario.Area.Nivel.Edificio.Localidad.Municipio.Nombre,
                    Habilitado = q.Habilitado
                }).ToList();
        }

        public int ObtenerTotalRegistros(ContenedorViewModel criterios)
        {
            return ObtenerQuery(criterios, false).Count();
        }

        public IQueryable<Contenedor> ObtenerQuery(ContenedorViewModel criterios, bool paginar)
        {
            IQueryable<Contenedor> query = Contexto.Set<Contenedor>();

            query = query.Where(c => c.Habilitado == criterios.Contenedor.Habilitado);

            if (!string.IsNullOrEmpty(criterios.Contenedor.Nombre))
            {
                query = query.Where(c => c.Nombre.Contains(criterios.Contenedor.Nombre));
            }
            if (criterios.Contenedor.Mobilario.IDMobilario > 0)
            {
                query = query.Where(c => c.IDMobilario == criterios.Contenedor.Mobilario.IDMobilario);
            }
            if (criterios.Contenedor.Mobilario.IDArea > 0)
            {
                query = query.Where(c => c.Mobilario.IDArea == criterios.Contenedor.Mobilario.IDArea);
            }
            if (criterios.Contenedor.Mobilario.Area.IDNivel > 0)
            {
                query = query.Where(c => c.Mobilario.Area.IDNivel == criterios.Contenedor.Mobilario.Area.IDNivel);
            }
            if (criterios.Contenedor.Mobilario.Area.Nivel.IDEdificio > 0)
            {
                query = query.Where(c => c.Mobilario.Area.Nivel.IDEdificio == criterios.Contenedor.Mobilario.Area.Nivel.IDEdificio);
            }
            if (criterios.Contenedor.Mobilario.Area.Nivel.Edificio.IDLocalidad > 0)
            {
                query = query.Where(c => c.Mobilario.Area.Nivel.Edificio.IDLocalidad == criterios.Contenedor.Mobilario.Area.Nivel.Edificio.IDLocalidad);
            }
            if (criterios.Contenedor.Mobilario.Area.Nivel.Edificio.Localidad.IDMunicipio > 0)
            {
                query = query.Where(c => c.Mobilario.Area.Nivel.Edificio.Localidad.IDMunicipio ==
                                         criterios.Contenedor.Mobilario.Area.Nivel.Edificio.Localidad.IDMunicipio);
            }
            if (criterios.SoloAgregado)
            {
                query = query.Where(c => c.ContenedorEntidad.Any(e => e.IDEntidad == criterios.IdEntidad));
            }
            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.OrderBy(q => q.Nombre + "-" +
                                           q.Mobilario.Nombre + "-" +
                                           q.Mobilario.Area.Nombre + "-" +
                                           q.Mobilario.Area.Nivel.Nombre + "-" +
                                           q.Mobilario.Area.Nivel.Edificio.Nombre + "-" +
                                           q.Mobilario.Area.Nivel.Edificio.Localidad.Nombre + "-" +
                                           q.Mobilario.Area.Nivel.Edificio.Localidad.Municipio.Nombre);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }

            query = query.Include(c => c.Mobilario.Area.Nivel.Edificio.Localidad.Municipio);

            return query;
        }

        public void CambiarHabilitado(int idContenedor, bool habilitado)
        {
            var contenedor = new Contenedor {IDContenedor = idContenedor };

            Contexto.Contenedores.Attach(contenedor);

            contenedor.Habilitado = habilitado;
            contenedor.FechaModificacion = DateTime.Now;

            Contexto.Entry(contenedor).Property(p => p.Habilitado).IsModified = true;
        }

        public static List<SelectListItem> Buscar(int idMobilario, string buscar)
        {
            using (var contexto = new Contexto())
            {
                return (from c in contexto.Contenedores
                    where c.IDMobilario == idMobilario
                          && c.Nombre.Contains(buscar) &&
                          c.Habilitado
                    orderby c.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?)c.IDContenedor).Trim(),
                        Text = c.Nombre
                    }).ToList();
            }
        }

        public static List<SelectListItem> Buscar(int idMobilario, string buscar, int idEntidad)
        {
            if (idEntidad == 0) return Buscar(idMobilario, buscar);

            using (var contexto = new Contexto())
            {
                return (from c in contexto.Contenedores
                    where c.IDMobilario == idMobilario
                          && c.Nombre.Contains(buscar) &&
                          c.Habilitado &&
                          c.ContenedorEntidad.Any(e => e.IDEntidad == idEntidad)
                    orderby c.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?)c.IDContenedor).Trim(),
                        Text = c.Nombre
                    }).ToList();
            }
        }

        public void AgregarContenedorEntidad(int idEntidad, int idContenedor)
        {
            var entidad = new Entidad { IDEntidad = idEntidad };
            var contenedorEntidad = new ContenedorEntidad
            {
                IDEntidad = idEntidad,
                IDContenedor = idContenedor,
                FechaCreacion = DateTime.Now
            };
            
            Contexto.Entidad.Attach(entidad);

            entidad.ContenedorEntidad.Add(contenedorEntidad);
        }

        public void QuitarContenedorEntidad(int idEntidad, int idContenedor)
        {
            var entidad = new Entidad { IDEntidad = idEntidad };
            var contenedorEntidad = new ContenedorEntidad {IDEntidad = idEntidad, IDContenedor = idContenedor};

            entidad.ContenedorEntidad.Add(contenedorEntidad);

            Contexto.Entidad.Attach(entidad);

            entidad.ContenedorEntidad.Remove(contenedorEntidad);
        }
    }
}