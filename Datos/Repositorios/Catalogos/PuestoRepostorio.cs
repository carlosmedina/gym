﻿using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web.Mvc;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Recursos;

namespace Datos.Repositorios.Catalogos
{
    public class PuestoRepostorio : Repositorio<Puesto>
    {
        public PuestoRepostorio(Contexto contexto) : base(contexto)
        {
        }
        
        public void CambiarHabilitado(int idPuesto, bool habilitado)
        {
            var puestoCambiarHabilitado = new Puesto { IDPuesto = idPuesto };
            Contexto.Puesto.Attach(puestoCambiarHabilitado);
            puestoCambiarHabilitado.Habilitado = habilitado;
            Contexto.Entry(puestoCambiarHabilitado).Property(r => r.Habilitado).IsModified = true;
        }
        
        public List<Puesto> Buscar(PuestoViewModel viewModel)
        {
            return ObtenerQuery(viewModel, true).ToList();
        }
        
        public int ObtenerTotalRegistros(PuestoViewModel viewModel)
        {
            return ObtenerQuery(viewModel, false).Count();
        }

        public IQueryable<Puesto> ObtenerQuery(PuestoViewModel criterios, bool paginar)
        {
            IQueryable<Puesto> query = Contexto.Set<Puesto>();
            query = query.Where(c => c.Habilitado == criterios.Puesto.Habilitado);
            if (!string.IsNullOrEmpty(criterios.Puesto.Nombre))
            {
                query = query.Where(c => c.Nombre.Contains(criterios.Puesto.Nombre));
            }
            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.OrderBy(q => q.Nombre);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }
            return query;
        }
    }
}
