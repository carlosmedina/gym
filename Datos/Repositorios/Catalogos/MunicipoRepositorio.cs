﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web.Mvc;
using Datos.Recursos;
using Datos.DTO.Infraestructura.ViewModels;

namespace Datos.Repositorios.Catalogos
{
    public class MunicipioRepositorio : Repositorio<Municipio>
    {
        public MunicipioRepositorio(Contexto contexto) : base(contexto)
        {
        }
        
        public List<Municipio> Buscar(MunicipioViewModel municipioViewModel)
        {
            return ObtenerQuery(municipioViewModel, true).ToList();
        }
        
        public int ObtenerTotalRegistros(MunicipioViewModel municipioViewModel)
        {
            return ObtenerQuery(municipioViewModel, false).Count();
        }

        public IQueryable<Municipio> ObtenerQuery(MunicipioViewModel criterios, bool paginar)
        {
            IQueryable<Municipio> query = Contexto.Set<Municipio>();

            if (!string.IsNullOrEmpty(criterios.Municipio.Nombre))
            {
                query = query.Where(c => c.Nombre.Contains(criterios.Municipio.Nombre));
            }
            if (criterios.Municipio.Estado != null && criterios.Municipio.Estado.IDEstado > 0)
            {
                query = query.Where(c => c.Estado.IDEstado == criterios.Municipio.Estado.IDEstado);
            }
            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.OrderBy(q => q.Nombre);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }
            query = query.Include(c => c.Estado);
            return query;
        }
        
        public List<SelectListItem> Buscar(int idEstado)
        {
            return (from m in Contexto.Municipio
                    where m.IDEstado == idEstado
                    orderby m.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?)m.IDMunicipio).Trim(),
                        Text = m.Nombre
                    }).ToList();
        }
        
        public Municipio BuscarPorId(int idMunicipio, int idEstado)
        {
            return (from c in Contexto.Municipio
                    where c.IDMunicipio == idMunicipio && c.IDEstado == idEstado
                    select c).Include(m => m.Estado).FirstOrDefault();
        }

        public static List<SelectListItem> BuscarPorEstado(int idEstado)
        {
            List<SelectListItem> lista;
            using (var bd = new Contexto())
            {
                lista = (from m in bd.Municipio
                         where m.IDEstado == idEstado
                         orderby m.Nombre
                         select new SelectListItem
                         {
                             Value = SqlFunctions.StringConvert((decimal?)m.IDMunicipio).Trim(),
                             Text = m.Nombre
                         }).ToList();

                if (lista.Count > 0)
                {
                    lista.Insert(0, new SelectListItem { Text = General.Seleccione, Value = string.Empty });
                }
            }
            return lista;
        }

        public static List<SelectListItem> ObtenerPredeterminados(bool agregarSeleccion = true, bool soloConLocalidad = true)
        {
            List<SelectListItem> lista;
            using (var bd = new Contexto())
            {
                lista = (from m in bd.Municipio
                    where m.Estado.Predeterminado &&
                          (soloConLocalidad == false || m.Localidad.Any(l => l.Habilitado))
                    orderby m.Nombre ascending
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?) m.IDMunicipio).Trim(),
                        Text = m.Nombre
                    }).ToList();

                if (lista.Count > 0 && agregarSeleccion)
                {
                    lista.Insert(0, new SelectListItem { Text = General.Seleccione, Value = string.Empty });
                }
            }
            return lista;
        }

        public static List<SelectListItem> ObtenerPredeterminados(int idEntidad, bool agregarSeleccione = true)
        {
            if (idEntidad == 0) return ObtenerPredeterminados(agregarSeleccione);

            List<SelectListItem> lista;
            using (var bd = new Contexto())
            {
                lista = (from mu in bd.Municipio
                    where mu.Estado.Predeterminado &&
                          (from c in bd.Contenedores
                              join ec in bd.ContenedorEntidad on c.IDContenedor equals ec.IDContenedor
                              join m in bd.Mobilarios on c.IDMobilario equals m.IDMobilario
                              join a in bd.Areas on m.IDArea equals a.IDArea
                              join n in bd.Niveles on a.IDNivel equals n.IDNivel
                              join e in bd.Edificios on n.IDEdificio equals e.IDEdificio
                              join l in bd.Localidad on e.IDLocalidad equals l.IDLocalidad
                              where mu.IDMunicipio == l.IDMunicipio && ec.IDEntidad == idEntidad
                              select c).Any()
                    orderby mu.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?) mu.IDMunicipio).Trim(),
                        Text = mu.Nombre
                    }).ToList();

                if (lista.Count > 0 && agregarSeleccione)
                {
                    lista.Insert(0, new SelectListItem {Text = General.Seleccione, Value = string.Empty});
                }
            }
            return lista;
        }
    }
}
