﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Datos.DTO.Infraestructura.ViewModels;
using System.Web.Mvc;
using Datos.Recursos;

namespace Datos.Repositorios.Catalogos
{
    public class EstadoRepositorio : Repositorio<Estado>
    {
        public EstadoRepositorio(Contexto contexto) : base(contexto)
        {
        }

        public override void Modificar(Estado estado)
        {
            Contexto.Entry(estado).State = EntityState.Unchanged;

            Contexto.Entry(estado).Property(p => p.Predeterminado).IsModified = true;
        }

        public List<Estado> Buscar(EstadoViewModel estadoViewModel)
        {
            return ObtenerQuery(estadoViewModel, true).ToList();
        }
        
        public int ObtenerTotalRegistros(EstadoViewModel estadoViewModel)
        {
            return ObtenerQuery(estadoViewModel, false).Count();
        }

        public IQueryable<Estado> ObtenerQuery(EstadoViewModel criterios, bool paginar)
        {
            IQueryable<Estado> query = Contexto.Set<Estado>();

            if (!string.IsNullOrEmpty(criterios.Estado.Nombre))
            {
                query = query.Where(c => c.Nombre.Contains(criterios.Estado.Nombre));
            }
            if (!string.IsNullOrEmpty(criterios.Estado.Abreviatura))
            {
                query = query.Where(c => c.Abreviatura.Contains(criterios.Estado.Abreviatura));
            }
            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.OrderBy(q => q.Nombre);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }
            return query;
        }
        
        public Estado BuscarPorId(int idEstado)
        {
            return (from c in Contexto.Estado
                    where c.IDEstado == idEstado
                    select c).FirstOrDefault();
        }

        public int ObtenerIdEstadoPredeterminado()
        {
            return (from e in Contexto.Estado
                where e.Predeterminado
                select e.IDEstado).Single();
        }

        public static List<SelectListItem> BuscarEstados()
        {
            List<SelectListItem> lista;
            using (var bd = new Contexto())
            {
                lista = (from m in bd.Estado
                    where m.IDEstado > 0
                    orderby m.Nombre
                    select new SelectListItem
                    {
                        Value = m.IDEstado.ToString(),
                        Text = m.Nombre
                    }).ToList();

                if (lista.Count > 0)
                {
                    lista.Insert(0, new SelectListItem { Text = General.Seleccione, Value = string.Empty });
                }
            }
            return lista;
        }

        public void QuitarPredeterminado()
        {
            Contexto.Database.ExecuteSqlCommand("UPDATE Estado SET Predeterminado = 0");
        }
    }
}
