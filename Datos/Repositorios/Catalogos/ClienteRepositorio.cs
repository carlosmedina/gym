﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web.Mvc;
using Datos.DTO.Infraestructura.ViewModels;

namespace Datos.Repositorios.Catalogos
{
    public class ClienteRepositorio : Repositorio<Cliente>
    {
        public ClienteRepositorio(Contexto contexto) : base(contexto)
        {
        }

        public override void Modificar(Cliente area)
        {
            Contexto.Entry(area).State = EntityState.Unchanged;

            Contexto.Entry(area).Property(p => p.Nombre).IsModified = true;
        }

        public List<Cliente> Buscar(ClienteViewModel criterios)
        {
            return ObtenerQuery(criterios, true).ToList();
        }

        public int ObtenerTotalRegistros(ClienteViewModel criterios)
        {
            return ObtenerQuery(criterios, false).Count();
        }

        public IQueryable<Cliente> ObtenerQuery(ClienteViewModel criterios, bool paginar)
        {
            IQueryable<Cliente> query = Contexto.Set<Cliente>();

            query = query.Where(c => c.Habilitado == criterios.Cliente.Habilitado);

            if (!string.IsNullOrEmpty(criterios.Cliente.Nombre))
            {
                query = query.Where(c => c.Nombre.Contains(criterios.Cliente.Nombre));
            }
            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.OrderBy(q => q.Nombre);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }

            return query;
        }

        public void CambiarHabilitado(int idCliente, bool habilitado)
        {
            var cliente = new Cliente {IdCliente = idCliente };

            Contexto.Cliente.Attach(cliente);

            cliente.Habilitado = habilitado;

            Contexto.Entry(cliente).Property(p => p.Habilitado).IsModified = true;
        }

        public static List<SelectListItem> Buscar(int idNivel, string buscar)
        {
            using (var contexto = new Contexto())
            {
                return (from c in contexto.Areas
                    where c.IDNivel == idNivel
                          && c.Nombre.Contains(buscar) &&
                          c.Habilitado
                        orderby c.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?)c.IDArea).Trim(),
                        Text = c.Nombre
                    }).ToList();
            }
        }

        public static List<SelectListItem> Buscar(int idNivel, string buscar, int idEntidad)
        {
            if (idEntidad == 0) return Buscar(idNivel, buscar);

            using (var contexto = new Contexto())
            {
                return (from a in contexto.Areas
                    where a.IDNivel == idNivel
                          && a.Nombre.Contains(buscar) &&
                          a.Habilitado &&
                          (from c in contexto.Contenedores
                              join ec in contexto.ContenedorEntidad on c.IDContenedor equals ec.IDContenedor
                              join m in contexto.Mobilarios on c.IDMobilario equals m.IDMobilario
                              where m.IDArea == a.IDArea && ec.IDEntidad == idEntidad
                              select c).Any()
                    orderby a.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?) a.IDArea).Trim(),
                        Text = a.Nombre
                    }).ToList();
            }
        }
    }
}
