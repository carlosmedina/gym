﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web.Mvc;
using Datos.DTO.Infraestructura.ViewModels;

namespace Datos.Repositorios.Catalogos
{
    public class EdificioRepositorio : Repositorio<Edificio>
    {
        public EdificioRepositorio(Contexto contexto) : base(contexto)
        {
        }

        public override void Modificar(Edificio edificio)
        {
            Contexto.Entry(edificio).State = EntityState.Unchanged;

            Contexto.Entry(edificio).Property(p => p.Nombre).IsModified = true;
            Contexto.Entry(edificio).Property(p => p.IDLocalidad).IsModified = true;
            Contexto.Entry(edificio).Property(p => p.FechaModificacion).IsModified = true;
        }

        public List<Edificio> Buscar(EdificioViewModel criterios)
        {
            return ObtenerQuery(criterios, true).ToList();
        }

        public int ObtenerTotalRegistros(EdificioViewModel criterios)
        {
            return ObtenerQuery(criterios, false).Count();
        }

        public IQueryable<Edificio> ObtenerQuery(EdificioViewModel criterios, bool paginar)
        {
            IQueryable<Edificio> query = Contexto.Set<Edificio>();

            query = query.Where(c => c.Habilitado == criterios.Edificio.Habilitado);

            if (!string.IsNullOrEmpty(criterios.Edificio.Nombre))
            {
                query = query.Where(c => c.Nombre.Contains(criterios.Edificio.Nombre));
            }
            if (criterios.Edificio.Localidad.IDLocalidad > 0)
            {
                query = query.Where(c => c.IDLocalidad == criterios.Edificio.Localidad.IDLocalidad);
            }
            if (criterios.Edificio.Localidad.IDMunicipio > 0)
            {
                query = query.Where(c => c.Localidad.IDMunicipio == criterios.Edificio.Localidad.IDMunicipio &&
                                         c.Localidad.IDEstado == criterios.Edificio.Localidad.IDEstado);
            }
            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.OrderBy(q => q.Nombre + "-" +
                                           q.Localidad.Nombre + "-" +
                                           q.Localidad.Municipio.Nombre);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }

            query = query.Include(c => c.Localidad.Municipio);

            return query;
        }

        public void CambiarHabilitado(int idEdificio, bool habilitado)
        {
            var edificio = new Edificio {IDEdificio = idEdificio};

            Contexto.Edificios.Attach(edificio);

            edificio.Habilitado = habilitado;
            edificio.FechaModificacion = DateTime.Now;

            Contexto.Entry(edificio).Property(p => p.Habilitado).IsModified = true;
        }

        public static List<SelectListItem> Buscar(int idLocalidad, string buscar)
        {
            using (var contexto = new Contexto())
            {
                return (from c in contexto.Edificios
                    where c.IDLocalidad == idLocalidad
                          && c.Nombre.Contains(buscar) &&
                          c.Habilitado
                        orderby c.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?)c.IDEdificio).Trim(),
                        Text = c.Nombre
                    }).ToList();
            }
        }

        public static List<SelectListItem> Buscar(int idLocalidad, string buscar, int idEntidad)
        {
            if (idEntidad == 0) return Buscar(idLocalidad, buscar);

            using (var contexto = new Contexto())
            {
                return (from e in contexto.Edificios
                    where e.IDLocalidad == idLocalidad &&
                          e.Nombre.Contains(buscar) &&
                          e.Habilitado &&
                          (from c in contexto.Contenedores
                              join ec in contexto.ContenedorEntidad on c.IDContenedor equals ec.IDContenedor
                              join m in contexto.Mobilarios on c.IDMobilario equals m.IDMobilario
                              join a in contexto.Areas on m.IDArea equals a.IDArea
                              join n in contexto.Niveles on a.IDNivel equals n.IDNivel
                              where n.IDEdificio == e.IDEdificio && ec.IDEntidad == idEntidad
                              select c).Any()
                    orderby e.Nombre
                    select new SelectListItem
                    {
                        Value = SqlFunctions.StringConvert((decimal?) e.IDEdificio).Trim(),
                        Text = e.Nombre
                    }).ToList();
            }
        }
    }
}
