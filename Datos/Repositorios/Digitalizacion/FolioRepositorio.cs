﻿
using System.Linq;

namespace Datos.Repositorios.Digitalizacion
{
    public class FolioRepositorio : Repositorio<Folio>
    {
        public FolioRepositorio(Contexto contexto) :
            base(contexto)
        {
        }

        public int ProbableSiguienteFolio(int idEntidad, int idCuadroArchivistico)
        {
            return Contexto.SpProbableSiguienteFolio(idEntidad, idCuadroArchivistico).Single().Value;
        }

        public int SiguienteFolio(int idEntidad, int idCuadroArchivistico)
        {
            return Contexto.SpSiguienteFolio(idEntidad, idCuadroArchivistico).Single().Value;
        }
    }
}
