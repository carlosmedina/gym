﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios.Digitalizacion
{
    public class AdjuntoRepositorio : Repositorio<Adjunto>
    {
        public AdjuntoRepositorio(Contexto contexto) :
            base(contexto)
        {

        }
    }
}
