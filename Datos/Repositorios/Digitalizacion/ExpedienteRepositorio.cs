﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Datos.DTO.Infraestructura.Reportes;
using Datos.DTO.Infraestructura.ViewModels;
using Sistema;
using Datos.Recursos;

namespace Datos.Repositorios.Digitalizacion
{
    public class ExpedienteRepositorio : Repositorio<Expediente>
    {
        public ExpedienteRepositorio(Contexto contexto) :
            base(contexto)
        {
        }

        public override void Modificar(Expediente expediente)
        {
            //Se saco de expediente por problemas de restriccion de EF
            var legajos = expediente.Legajo.ToList();
            var adjuntos = expediente.Adjunto.ToList();
            expediente.Legajo.Clear();
            expediente.Adjunto.Clear();

            Contexto.Entry(expediente).State = EntityState.Modified;

            Contexto.Entry(expediente).Property(p => p.IDUsuario).IsModified = false;
            Contexto.Entry(expediente).Property(p => p.IDEntidad).IsModified = false;
            Contexto.Entry(expediente).Property(p => p.NumeroExpediente).IsModified = false;
            Contexto.Entry(expediente).Property(p => p.FechaCreacion).IsModified = false;
            Contexto.Entry(expediente).Property(p => p.Ubicacion).IsModified = false;
            Contexto.Entry(expediente).Property(p => p.CantidadConsulta).IsModified = false;

            var contenedor = new Contenedor {IDContenedor = expediente.IDContenedor};
            Contexto.Contenedores.Attach(contenedor);
            expediente.Contenedor = contenedor;


            var idLegajosActuales = (from l in Contexto.Legajos
                where l.IDExpediente == expediente.IDExpediente
                select l.IDLegajo).ToList();

            //Eliminar
            foreach (var idLegajo in idLegajosActuales)
            {
                if(legajos.Any(l => l.IDLegajo == idLegajo)) continue;

                var eliminarLegajo = new Legajo {IDLegajo = idLegajo};
                Contexto.Legajos.Attach(eliminarLegajo);
                Contexto.Legajos.Remove(eliminarLegajo);
            }

            //Nuevos/modificar
            foreach (var legajoGuardar in legajos)
            {
                if (legajoGuardar.IDLegajo > 0)
                {
                    Contexto.Entry(legajoGuardar).Property(p => p.FolioInicial).IsModified = true;
                    Contexto.Entry(legajoGuardar).Property(p => p.FolioFinal).IsModified = true;
                    Contexto.Entry(legajoGuardar).Property(p => p.FechaModificacion).IsModified = true;
                    Contexto.Entry(legajoGuardar).Property(p => p.FechaCreacion).IsModified = false;
                }
                else
                {
                    Contexto.Entry(legajoGuardar).State = EntityState.Added;
                }
            }

            //Nuevos/modificar
            foreach (var adjuntoGuardar in adjuntos)
            {
                if (adjuntoGuardar.IDAdjunto > 0)
                {
                    Contexto.Entry(adjuntoGuardar).Property(p => p.Descripcion).IsModified = true;
                    Contexto.Entry(adjuntoGuardar).Property(p => p.Fecha).IsModified = true;
                    Contexto.Entry(adjuntoGuardar).Property(p => p.Ubicacion).IsModified = false;
                }
                else
                {
                    Contexto.Entry(adjuntoGuardar).State = EntityState.Added;
                }
            }
        }

        public override Expediente ObtenerPorId(object idExpediente)
        {
            return (from e in Contexto.Expediente
                    .Include(i => i.Legajo)
                    .Include(i => i.Adjunto)
                    .Include(i => i.Cadido.TipoClasificacionInformacion)
                    .Include(i => i.Cadido.FundamentoLegal)
                    .Include(i => i.Cadido.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                   .PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico)
                    .Include(i => i.Contenedor.Mobilario.Area.Nivel.Edificio.Localidad.Municipio)
                    .Include(i => i.Cadido.DisposicionDocumental)
                    .Include(i => i.Cadido.CuadroArchivistico)
                    .Include(i => i.Entidad)
                    .Include(i => i.Contenedor)
                    .Include(i => i.Usuario)
                    .Include(i => i.EstadoExpediente)
                        where e.IDExpediente == (int) idExpediente 
                select e).SingleOrDefault();
        }

        public List<Expediente> Buscar(ExpedienteBuscarViewModel criterios)
        {
            return ObtenerQuery(criterios, true).ToList();
        }

        public int ObtenerTotalRegistros(ExpedienteBuscarViewModel criterios)
        {
            return ObtenerQuery(criterios, false).Count();
        }

        private IQueryable<Expediente> ObtenerQuery(ExpedienteBuscarViewModel criterios, bool paginar)
        {
            IQueryable<Expediente> query = Contexto.Set<Expediente>();
            
            if (criterios.IdSubserie > 0)
            {
                query = query.Where(b => b.Cadido.IDCuadroArchivistico == criterios.IdSubserie);
            }
            else if (criterios.IdSerie > 0)
            {
                query = query.Where(b => b.Cadido.IDCuadroArchivistico == criterios.IdSerie ||
                                         b.Cadido.CuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSerie);
            }
            else if (criterios.IdSubseccion > 0)
            {
                query = query.Where(
                    b => b.Cadido.CuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSubseccion ||
                         b.Cadido.CuadroArchivistico.PadreCuadroArchivistico.IDPadreCuadroArchivistico ==
                         criterios.IdSubseccion);
            }
            else if (criterios.IdSeccion > 0)
            {
                query = query.Where(b => b.Cadido.CuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSeccion ||
                                         b.Cadido.CuadroArchivistico.PadreCuadroArchivistico
                                             .IDPadreCuadroArchivistico == criterios.IdSeccion ||
                                         b.Cadido.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .IDPadreCuadroArchivistico == criterios.IdSeccion);
            }
            else if (criterios.IdSubfondo > 0)
            {
                query = query.Where(b => b.Cadido.CuadroArchivistico
                                             .PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSubfondo ||
                                         b.Cadido.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .IDPadreCuadroArchivistico == criterios.IdSubfondo ||
                                         b.Cadido.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdSubfondo);
            }
            else if (criterios.IdFondo > 0)
            {
                query = query.Where(b => b.Cadido.CuadroArchivistico
                                             .PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdFondo ||
                                         b.Cadido.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .IDPadreCuadroArchivistico == criterios.IdFondo ||
                                         b.Cadido.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .PadreCuadroArchivistico.IDPadreCuadroArchivistico == criterios.IdFondo ||
                                         b.Cadido.CuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .PadreCuadroArchivistico.PadreCuadroArchivistico
                                             .IDPadreCuadroArchivistico == criterios.IdFondo);
            }

            if (criterios.Expediente.IDContenedor > 0)
            {
                query = query.Where(c => c.IDContenedor == criterios.Expediente.IDContenedor);
            }
            if (criterios.Expediente.Contenedor.IDMobilario > 0)
            {
                query = query.Where(c => c.Contenedor.IDMobilario == criterios.Expediente.Contenedor.IDMobilario);
            }
            if (criterios.Expediente.Contenedor.Mobilario.IDArea > 0)
            {
                query = query.Where(c => c.Contenedor.Mobilario.IDArea ==
                                         criterios.Expediente.Contenedor.Mobilario.IDArea);
            }
            if (criterios.Expediente.Contenedor.Mobilario.Area.IDNivel > 0)
            {
                query = query.Where(c => c.Contenedor.Mobilario.Area.IDNivel ==
                                         criterios.Expediente.Contenedor.Mobilario.Area.IDNivel);
            }
            if (criterios.Expediente.Contenedor.Mobilario.Area.Nivel.IDEdificio > 0)
            {
                query = query.Where(c => c.Contenedor.Mobilario.Area.Nivel.IDEdificio ==
                                         criterios.Expediente.Contenedor.Mobilario.Area.Nivel.IDEdificio);
            }
            if (criterios.Expediente.Contenedor.Mobilario.Area.Nivel.Edificio.IDLocalidad > 0)
            {
                query = query.Where(c => c.Contenedor.Mobilario.Area.Nivel.Edificio.IDLocalidad ==
                                         criterios.Expediente.Contenedor.Mobilario.Area.Nivel.Edificio.IDLocalidad);
            }
            if (criterios.Expediente.Contenedor.Mobilario.Area.Nivel.Edificio.Localidad.IDMunicipio > 0)
            {
                query = query.Where(c => c.Contenedor.Mobilario.Area.Nivel.Edificio.Localidad.IDMunicipio ==
                                         criterios.Expediente.Contenedor.Mobilario.Area.Nivel.Edificio.Localidad.IDMunicipio);
            }

            if (criterios.Expediente.IDEstadoExpediente > 0)
            {
                query = query.Where(c => c.IDEstadoExpediente == criterios.Expediente.IDEstadoExpediente);
            }

            if (criterios.Expediente.Titulo != null)
            {
                query = query.Where(c => c.Titulo.Contains(criterios.Expediente.Titulo) ||
                                         c.DescripcionAsunto.Contains(criterios.Expediente.Titulo) ||
                                         c.FolioInicial.ToString().Contains(criterios.Expediente.Titulo) ||
                                         c.FolioFinal.ToString().Contains(criterios.Expediente.Titulo));
            }
            if (criterios.Expediente.Cadido.IDDisposicionDocumental > 0)
            {
                query = query.Where(c => c.Cadido.IDDisposicionDocumental ==
                                         criterios.Expediente.Cadido.IDDisposicionDocumental);
            }
            if (criterios.Expediente.Cadido.EsAdministrativo == false)
            {
                query = query.Where(c => c.Cadido.EsAdministrativo == false);
            }
            if (criterios.Expediente.Cadido.EsContableFiscal == false)
            {
                query = query.Where(c => c.Cadido.EsContableFiscal == false);
            }
            if (criterios.Expediente.Cadido.EsLegal == false)
            {
                query = query.Where(c => c.Cadido.EsContableFiscal == false);
            }
            if (criterios.Expediente.Cadido.IDTipoClasificacionInformacion > 0)
            {
                query = query.Where(c => c.Cadido.IDTipoClasificacionInformacion ==
                                         criterios.Expediente.Cadido.IDTipoClasificacionInformacion);
            }
            if (criterios.Expediente.IDUsuario > 0)
            {
                query = query.Where(c => c.IDUsuario == criterios.Expediente.IDUsuario);
            }
            if (criterios.Expediente.IDEntidad > 0)
            {
                query = query.Where(c => c.IDEntidad == criterios.Expediente.IDEntidad);
            }
            if (criterios.NumeroExpediente > 0)
            {
                query = query.Where(c => c.NumeroExpediente == criterios.Expediente.NumeroExpediente);
            }
            if (criterios.FechaAperturaInicio.HasValue)
            {
                query = query.Where(c => c.FechaApertura >= criterios.FechaAperturaInicio);
            }
            if (criterios.FechaAperturaFin.HasValue)
            {
                query = query.Where(c => c.FechaApertura <= criterios.FechaAperturaFin);
            }
            if (criterios.FechaCierreInicio.HasValue)
            {
                query = query.Where(c => c.FechaCierre >= criterios.FechaCierreInicio);
            }
            if (criterios.FechaCierreFin.HasValue)
            {
                query = query.Where(c => c.FechaCierre <= criterios.FechaCierreFin);
            }

            if (paginar && criterios.TamanoPagina > 0 && criterios.PaginaActual > 0)
            {
                query = query.Include(b => b.Cadido.CuadroArchivistico);
                query = query.Include(c => c.Cadido.TipoClasificacionInformacion);
                query = query.Include(c => c.Cadido.DisposicionDocumental);
                query = query.Include(c => c.Entidad);
                query = query.Include(c => c.EstadoExpediente);
                query = query.Include(c => c.Legajo);

                query = query.OrderByDescending(q => q.FechaCreacion);
                query = query.Skip((criterios.PaginaActual - 1) * criterios.TamanoPagina).Take(criterios.TamanoPagina);
            }

            return query;
        }

        public PartesUbicacionExpediente ObtenerUbicacion(int idEntidad, int idCuadroArchivistico)
        {
            var partes = (from c in Contexto.CuadroArchivisticoEntidad
                where c.IDEntidad == idEntidad &&
                      c.IDCuadroArchivistico == idCuadroArchivistico
                select new PartesUbicacionExpediente
                {
                    EntidadClave = c.Entidad.Abreviacion + "." + c.Entidad.Clave
                }).SingleOrDefault();

            if (partes != null)
            {
                var cuadroArchivistico = Contexto.SpObtenerAncestrosCuadroArchivistico(idCuadroArchivistico).ToList();

                foreach (var tipo in cuadroArchivistico)
                {
                    switch (tipo.IDTipoCuadroArchivistico)
                    {
                        case (int)TiposCuadroArchivistico.Fondo:
                            partes.ClaveFondo = tipo.Clave;
                            break;
                        case (int)TiposCuadroArchivistico.Seccion:
                            partes.ClaveSeccion = tipo.Clave;
                            break;
                        case (int)TiposCuadroArchivistico.Serie:
                            partes.ClaveSerie = tipo.Clave;
                            break;
                        case (int)TiposCuadroArchivistico.Subserie:
                            partes.ClaveSubserie = tipo.Clave;
                            break;
                    }
                }
            }

            return partes;
        }

        public void AumentarConsulta(int idExpediente)
        {
            Contexto.Database.ExecuteSqlCommand(
                "UPDATE Expediente SET CantidadConsulta = CantidadConsulta + 1 WHERE IDExpediente = " + idExpediente);
        }

        public int TotalExpedientesDigitalizados(int idUsuario)
        {
            return (from e in Contexto.Expediente
                where e.IDUsuario == idUsuario
                select e.IDExpediente).Count();
        }

        public int MesExpedientesDigitalizados(int idUsuario)
        {
            var anio = DateTime.Now.Year;
            var mes = DateTime.Now.Month;

            var fechaInicial = new DateTime(anio, mes, 1);
            var fechaFinal = new DateTime(anio, mes, DateTime.DaysInMonth(anio, mes)).AddDays(1).AddSeconds(-1);

            return (from e in Contexto.Expediente
                where e.IDUsuario == idUsuario &&
                      e.FechaCreacion >= fechaInicial && e.FechaCreacion <= fechaFinal
                select e.IDExpediente).Count();
        }

        public List<ReporteExpediente> ObtenerReporte(ExpedienteBuscarViewModel criterios)
        {
            var query = ObtenerQuery(criterios, false);

            return (from q in query
                select new ReporteExpediente
                {
                    IdExpediente = q.IDExpediente,
                    Titulo = q.Titulo,
                    Asunto = q.DescripcionAsunto,
                    ClaveCuadroArchivistico = q.Cadido.CuadroArchivistico.Clave,
                    NombreCuadroArchivistico = q.Cadido.CuadroArchivistico.Nombre,
                    NumeroExpediente = q.NumeroExpediente,
                    NumeroLegajo = q.Legajo.Count,
                    FechaInicio = q.FechaApertura,
                    FechaTermino = q.FechaCierre,
                    FoliosLegajo = 1,
                    Clasificacion = q.Cadido.TipoClasificacionInformacion.Nombre,
                    Estado = q.EstadoExpediente.Nombre,
                    TiempoTramite = q.Cadido.TiempoTramite,
                    TiempoConcentracion = q.Cadido.TiempoConcentracion,
                    EsAdministrativo = q.Cadido.EsAdministrativo,
                    EsLegal = q.Cadido.EsLegal,
                    EsContableFiscal = q.Cadido.EsContableFiscal,
                    Contenedor = q.Contenedor.Nombre
                }).ToList();
        }

        public static List<SelectListItem> ObtenerEstados()
        {
            return new List<SelectListItem>
            {
                new SelectListItem {Text = General.Seleccione, Value = string.Empty},
                new SelectListItem
                {
                    Text = EstadosExpediente.Proceso.ToString(),
                    Value = ((int) EstadosExpediente.Proceso).ToString()
                },
                new SelectListItem
                {
                    Text = EstadosExpediente.Concluido.ToString(),
                    Value = ((int) EstadosExpediente.Proceso).ToString()
                }
            };
        }

        public List<Legajo> Consultarlegajos(int idExpediente)
        {
            return (from e in Contexto.Legajos
                where e.IDExpediente == idExpediente
                select e).ToList();
        }

        public int TotalLegajos(int idExpediente)
        {
            return (from e in Contexto.Legajos
                where e.IDExpediente == idExpediente
               select e).Count();
        }

        public List<Adjunto> BuscarAdjunto(int idExpediente)
        {
            return (from e in Contexto.Adjunto
                where e.IDExpediente == idExpediente
                select e).ToList();
        }
    }
}