﻿using System;

namespace Datos.DTO.Infraestructura.Reportes
{
    public class ReporteExpediente
    {
        public int IdExpediente { get; set; }
        public string Titulo { get; set; }
        public string Asunto { get; set; }
        public string ClaveCuadroArchivistico { get; set; }
        public string NombreCuadroArchivistico { get; set; }
        public int NumeroExpediente { get; set; }
        public int NumeroLegajo { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaTermino { get; set; }
        public int FoliosLegajo { get; set; }
        public string Clasificacion { get; set; }
        public string Estado { get; set; }
        public int TiempoTramite { get; set; }
        public int TiempoConcentracion { get; set; }
        public bool EsAdministrativo { get; set; }
        public bool EsLegal { get; set; }
        public bool EsContableFiscal { get; set; }
        public string Contenedor { get; set; }
        public string Fondo { get; set; }
        public string Seccion { get; set; }
        public string SerieDocumental { get; set; }
        public string AreaProductora { get; set; }
        public string SignaturaTopografica { get; set; }

    }
}
