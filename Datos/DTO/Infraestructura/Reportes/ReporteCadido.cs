﻿namespace Datos.DTO.Infraestructura.Reportes
{
    public class ReporteCadido
    {
        public string Clave { get; set; }
        public string Nombre { get; set; }
        public string PadreClave { get; set; }
        public string PadreNombre { get; set; }
        public int IDPadreCuadroArchivistico { get; set; }
        public bool EsAdministrativo { get; set; }
        public bool EsLegal { get; set; }
        public bool EsContableFiscal { get; set; }
        public int IDTipoCuadroArchivistico { get; set; }
        public byte TiempoTramite { get; set; }
        public byte TiempoConcentracion { get; set; }
        public byte IDDisposicionDocumental { get; set; }
    }
}
