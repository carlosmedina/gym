﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Sistema;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class DetallesExpedienteViewModel
    {
        public int IdExpediente { get; set; }
        public string Fondo { get; set; }
        public string Subfondo { get; set; }
        public string Seccion { get; set; }
        public string Subseccion { get; set; }
        public string Serie { get; set; }
        public string Subserie { get; set; }
        public string ValorDocumental { get; set; }
        public string Clasificacion { get; set; }
        public string DisposicionDocumental { get; set; }
        public string FundamentoLegal { get; set; }
        public string TiempoTramite { get; set; }
        public string TiempoConcentracion { get; set; }
        public string NumeroExpediente { get; set; }
        public string FechaApertura { get; set; }
        public string FechaCierre { get; set; }
        public string Titulo { get; set; }
        public string DescripcionAsunto { get; set; }      
        public string Vigencia { get; set; }
        public string Municipio { get; set; }
        public string Localidad { get; set; }
        public string Edificio { get; set; }
        public string Nivel { get; set; }
        public string Area { get; set; }
        public string Mobiliario { get; set; }
        public string Contenedor { get; set; }
        public string UbicacionAdjunto { get; set; }
        public string DescripcionAdjunto { get; set; }
        public string Usuario { get; set; }
        public int TotalConsultas { get; set; }
        public string MesActual { get; set; }
        public int ConsultasMes { get; set; }
        public List<Auditoria> Auditoria { get; set; }
        public string NombreEstadoExpediente { get; set; }
        public int DescripcionEstadoExpediente { get; set; }
        public List<Legajo> DetallesLegajos { get; set; }

        public static DetallesExpedienteViewModel Obtener(Expediente expediente)
        {
            var expedienteDetalles = new DetallesExpedienteViewModel();

            if (expediente == null) return null;

            expedienteDetalles.Subfondo = string.Empty;
            expedienteDetalles.Subseccion = string.Empty;
            expedienteDetalles.Subserie = string.Empty;

            var cuadroArchivistico = expediente.Cadido.CuadroArchivistico;
            while (true)
            {
                if (cuadroArchivistico == null) break;

                switch (cuadroArchivistico.IDTipoCuadroArchivistico)
                {
                    case (int)TiposCuadroArchivistico.Fondo:
                        expedienteDetalles.Fondo = cuadroArchivistico.Nombre;
                        break;
                    case (int)TiposCuadroArchivistico.Subfondo:
                        expedienteDetalles.Subfondo = cuadroArchivistico.Nombre;
                        break;
                    case (int)TiposCuadroArchivistico.Seccion:
                        expedienteDetalles.Seccion = cuadroArchivistico.Nombre;
                        break;
                    case (int)TiposCuadroArchivistico.Subseccion:
                        expedienteDetalles.Subseccion = cuadroArchivistico.Nombre;
                        break;
                    case (int)TiposCuadroArchivistico.Serie:
                        expedienteDetalles.Serie = cuadroArchivistico.Nombre;
                        break;
                    case (int)TiposCuadroArchivistico.Subserie:
                        expedienteDetalles.Subserie = cuadroArchivistico.Nombre;
                        break;
                }

                cuadroArchivistico = cuadroArchivistico.PadreCuadroArchivistico;
            }
            
            expedienteDetalles.IdExpediente = expediente.IDExpediente;
            expedienteDetalles.NumeroExpediente = expediente.NumeroExpediente.ToString();
            expedienteDetalles.NombreEstadoExpediente = expediente.EstadoExpediente.Nombre;
            expedienteDetalles.DescripcionEstadoExpediente = expediente.IDEstadoExpediente;
            expedienteDetalles.FechaApertura = expediente.FechaApertura.ToShortDateString();
            expedienteDetalles.FechaCierre = expediente.FechaCierre.ToShortDateString();
            expedienteDetalles.Titulo = expediente.Titulo;
            expedienteDetalles.DescripcionAsunto = expediente.DescripcionAsunto;            
            expedienteDetalles.Vigencia = expediente.ExtensionVigencia?.ToShortDateString();
            expedienteDetalles.Municipio = expediente.Contenedor.Mobilario.Area.Nivel.Edificio.Localidad.Municipio
                .Nombre;
            expedienteDetalles.Localidad = expediente.Contenedor.Mobilario.Area.Nivel.Edificio.Localidad.Nombre;
            expedienteDetalles.Edificio = expediente.Contenedor.Mobilario.Area.Nivel.Edificio.Nombre;
            expedienteDetalles.Nivel = expediente.Contenedor.Mobilario.Area.Nivel.Nombre;
            expedienteDetalles.Area = expediente.Contenedor.Mobilario.Area.Nombre;
            expedienteDetalles.Mobiliario = expediente.Contenedor.Mobilario.Nombre;
            expedienteDetalles.Contenedor = expediente.Contenedor.Nombre;
            expedienteDetalles.UbicacionAdjunto = expediente.Ubicacion;            
            expedienteDetalles.ValorDocumental = expediente.Cadido.ObtenerValorDocumental();
            expedienteDetalles.Clasificacion = expediente.Cadido.TipoClasificacionInformacion.Nombre;
            expedienteDetalles.DisposicionDocumental = expediente.Cadido.DisposicionDocumental.Nombre;           
            expedienteDetalles.FundamentoLegal = expediente.Cadido.FundamentoLegal != null
                ? expediente.Cadido.FundamentoLegal.Nombre
                : string.Empty;
            expedienteDetalles.TiempoTramite = expediente.Cadido.TiempoTramite.ToString();
            expedienteDetalles.TiempoConcentracion = expediente.Cadido.TiempoConcentracion.ToString();
            expedienteDetalles.Usuario = expediente.Usuario.Nombre + " " + expediente.Usuario.ApellidoP + " " +
                                         expediente.Usuario.ApellidoM;
            expedienteDetalles.TotalConsultas = expediente.CantidadConsulta;
            expedienteDetalles.MesActual = FechaUtilidades.ObtenerNombreMes(DateTime.Now.Month);
            expedienteDetalles.TotalConsultas = expediente.CantidadConsulta;
            expedienteDetalles.MesActual = FechaUtilidades.ObtenerNombreMes(DateTime.Now.Month);
            expedienteDetalles.UbicacionAdjunto = expediente.Ubicacion;

            expedienteDetalles.Auditoria = expediente.Auditoria?.ToList() ?? new List<Auditoria>();

            expedienteDetalles.DetallesLegajos = expediente.Legajo.ToList();

            return expedienteDetalles;
        }
    }
}
