﻿using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class MunicipioViewModel : PaginacionViewModel
    {
        public MunicipioViewModel()
        {
        }

        public MunicipioViewModel(string campoMunicipio, string campoEstado)
        {
            CampoMunicipio = campoMunicipio;
            CampoEstado = campoEstado;
        }

        public string CampoMunicipio { get; set; }
        public string CampoEstado { get; set; }
        public Municipio Municipio { get; set; }
        public List<Municipio> Municipios { get; set; }
    }
}
