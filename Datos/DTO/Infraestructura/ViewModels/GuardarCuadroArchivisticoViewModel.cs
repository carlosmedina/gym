﻿namespace Datos.DTO.Infraestructura.ViewModels
{
    public class GuardarCuadroArchivisticoViewModel
    {
        public CuadroArchivistico CuadroArchivistico { get; set; }
        public int IdFondo { get; set; }
        public int IdSubfondo { get; set; }
        public int IdSeccion { get; set; }
        public int IdSubseccion { get; set; }
        public int IdSerie { get; set; }
        public int IdPadre { get; set; }

        public void EstablecerIdAntecesores(CuadroArchivistico cuadro)
        {
            while (true)
            {
                if (cuadro == null) break;

                switch (cuadro.IDTipoCuadroArchivistico)
                {
                    case (int)TiposCuadroArchivistico.Fondo:
                        IdFondo = cuadro.IDCuadroArchivistico;
                        break;
                    case (int)TiposCuadroArchivistico.Subfondo:
                        IdSubfondo = cuadro.IDCuadroArchivistico;
                        break;
                    case (int)TiposCuadroArchivistico.Seccion:
                        IdSeccion = cuadro.IDCuadroArchivistico;
                        break;
                    case (int)TiposCuadroArchivistico.Subseccion:
                        IdSubseccion = cuadro.IDCuadroArchivistico;
                        break;
                    case (int)TiposCuadroArchivistico.Serie:
                        IdSerie = cuadro.IDCuadroArchivistico;
                        break;
                }

                cuadro = cuadro.PadreCuadroArchivistico;
            }
        }
    }
}
