﻿using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class AuditoriaViewModel:PaginacionViewModel
    {
        public Auditoria Auditoria { get; set; }
        public List<Auditoria> Auditorias { get; set; }
        public string IdAuditoria { get; set; }
        public System.DateTime? FechaInicio { get; set; }

        public System.DateTime? FechaFin { get; set; }
    }
}
