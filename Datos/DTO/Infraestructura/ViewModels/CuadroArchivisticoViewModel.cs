﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class CuadroArchivisticoViewModel : PaginacionViewModel
    {
        public CuadroArchivistico CuadroArchivistico { get; set; }
        public List<CuadroArchivisticoBuscar> CuadroArchivisticos { get; set; }
        public string IdPadre { get; set; }
        public int IdFondo { get; set; }
        public int IdSubfondo { get; set; }
        public int IdSeccion { get; set; }
        public int IdSubseccion { get; set; }
        public int IdSerie { get; set; }

        public int IdEntidad { get; set; }
        public Entidad Entidad { get; set; }
        [DisplayName("Mostar sólo agregados")]
        public bool SoloAgregado { get; set; }

        public bool ExisteSubfondo { get; set; }
        public bool ExisteSubseccion { get; set; }

        public class RutaCuadroArchivistico
        {
            public int Id { get; set; }
            public string Tipo { get; set; }
            public string Nombre { get; set; }
        }

        public CuadroArchivistico ObtenerTipo(CuadroArchivistico cuadro, TiposCuadroArchivistico tipo)
        {
            while (true)
            {
                if (cuadro.PadreCuadroArchivistico == null) return null;

                if (cuadro.PadreCuadroArchivistico.IDTipoCuadroArchivistico == (int) tipo)
                    return cuadro.PadreCuadroArchivistico;

                cuadro = cuadro.PadreCuadroArchivistico;
            }
        }

        public void EstablecerIdAntecesores(CuadroArchivistico cuadro)
        {
            while (true)
            {
                if (cuadro == null) break;

                switch (cuadro.IDTipoCuadroArchivistico)
                {
                    case (int)TiposCuadroArchivistico.Fondo:
                        IdFondo = cuadro.IDCuadroArchivistico;
                        break;
                    case (int)TiposCuadroArchivistico.Subfondo:
                        IdSubfondo = cuadro.IDCuadroArchivistico;
                        break;
                    case (int)TiposCuadroArchivistico.Seccion:
                        IdSeccion = cuadro.IDCuadroArchivistico;
                        break;
                    case (int)TiposCuadroArchivistico.Subseccion:
                        IdSubseccion = cuadro.IDCuadroArchivistico;
                        break;
                    case (int)TiposCuadroArchivistico.Serie:
                        IdSerie = cuadro.IDCuadroArchivistico;
                        break;
                }

                cuadro = cuadro.PadreCuadroArchivistico;
            }
        }

        public List<RutaCuadroArchivistico> ObtenerRuta()
        {
            var ruta = new List<RutaCuadroArchivistico>();

            var antecesor = CuadroArchivistico.PadreCuadroArchivistico;
            var sucesor = CuadroArchivistico;
            while (true)
            {
                if (antecesor == null) break;
                
                ruta.Insert(0, new RutaCuadroArchivistico
                {
                    Id = antecesor.IDCuadroArchivistico,
                    Tipo = ((TiposCuadroArchivistico)sucesor.IDTipoCuadroArchivistico).ToString(),
                    Nombre = antecesor.Nombre
                });

                sucesor = antecesor;
                antecesor = antecesor.PadreCuadroArchivistico;
            }

            return ruta;
        }
    }
}
