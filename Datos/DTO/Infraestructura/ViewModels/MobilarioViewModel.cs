﻿using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class MobilarioViewModel : PaginacionViewModel
    {
        public Mobilario Mobilario { get; set; }
        public List<Mobilario> Mobilarios { get; set; }
        public string IdArea { get; set; }
    }
}