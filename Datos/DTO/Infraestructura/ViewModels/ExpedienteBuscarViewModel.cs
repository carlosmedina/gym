﻿using System;
using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class ExpedienteBuscarViewModel : PaginacionViewModel
    {
        public Expediente Expediente { get; set; }
        public ICollection<Legajo> Legajos { get; set; }
        public List<Expediente> Expedientes { get; set; }
        public int IdFondo { get; set; }
        public int IdSubfondo { get; set; }
        public int IdSeccion { get; set; }
        public int IdSubseccion { get; set; }
        public int IdSerie { get; set; }
        public int IdSubserie { get; set; }
        public bool MostrarProceso { get; set; }
        public bool MostrarConcluido { get; set; }
        public string IdMobiliario { get; set; }
        public int? NumeroExpediente { get; set; }
        public DateTime? FechaAperturaInicio { get; set; }
        public DateTime? FechaAperturaFin { get; set; }
        public DateTime? FechaCierreInicio { get; set; }
        public DateTime? FechaCierreFin { get; set; }
    }
}
