﻿using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class LocalidadViewModel : PaginacionViewModel
    {
        public LocalidadViewModel()
        {
        }

        public LocalidadViewModel(string campoMunicipio, string campoLocalidad)
        {
            CampoMunicipio = campoMunicipio;
            CampoLocalidad = campoLocalidad;
        }
        
        public Localidad Localidad { get; set; }
        public List<Localidad> Localidades { get; set; }
        public string CampoMunicipio { get; set; }
        public string CampoLocalidad { get; set; }
        public string IdMunicipio { get; set; }
        public bool SoloAgregados { get; set; }
    }
}
