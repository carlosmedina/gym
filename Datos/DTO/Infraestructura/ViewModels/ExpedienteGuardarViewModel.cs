﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class ExpedienteGuardarViewModel : PaginacionViewModel
    {
        public ExpedienteGuardarViewModel()
        {
            Legajos = new List<Legajo>();
            Adjuntos = new List<Adjunto>();
        }

        public Expediente Expediente { get; set; }
        public ICollection<Legajo> Legajos { get; set; }
        public int IdFondo { get; set; }
        public int IdSubfondo { get; set; }
        public int IdSeccion { get; set; }
        public int IdSubseccion { get; set; }
        public int IdSerie { get; set; }
        public int IdSubserie { get; set; }
        public int? NumeroExpediente { get; set; }
        public DateTime? FechaApertura { get; set; }
        public DateTime? FechaCierre { get; set; }
        public string Mantener { get; set; }
        public bool PuedeSeleccionarEntidad { get; set; }
        public string[] DescripcionAdjunto { get; set; }
        public ICollection<Adjunto> Adjuntos { get; set; }

        public void AsignarTipoCuadroArchivistico(CuadroArchivistico cuadro)
        {
            while (true)
            {
                switch (cuadro.IDTipoCuadroArchivistico)
                {
                    case (int) TiposCuadroArchivistico.Fondo:
                        IdFondo = cuadro.IDCuadroArchivistico;
                        break;
                    case (int) TiposCuadroArchivistico.Subfondo:
                        IdSubfondo = cuadro.IDCuadroArchivistico;
                        break;
                    case (int) TiposCuadroArchivistico.Seccion:
                        IdSeccion = cuadro.IDCuadroArchivistico;
                        break;
                    case (int) TiposCuadroArchivistico.Subseccion:
                        IdSubseccion = cuadro.IDCuadroArchivistico;
                        break;
                    case (int) TiposCuadroArchivistico.Serie:
                        IdSerie = cuadro.IDCuadroArchivistico;
                        break;
                    case (int) TiposCuadroArchivistico.Subserie:
                        IdSubserie = cuadro.IDCuadroArchivistico;
                        break;
                }

                if (cuadro.PadreCuadroArchivistico == null) break;

                cuadro = cuadro.PadreCuadroArchivistico;
            }
        }
    }
}
