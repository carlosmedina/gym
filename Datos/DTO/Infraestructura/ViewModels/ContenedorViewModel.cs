﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class ContenedorViewModel : PaginacionViewModel
    {
        public Contenedor Contenedor { get; set; }
        public List<ContenedorBuscarViewModel> Contenedores { get; set; }
        public string IdMobilario { get; set; }

        public int IdEntidad { get; set; }
        public Entidad Entidad { get; set; }
        [DisplayName("Mostar sólo agregados")]
        public bool SoloAgregado { get; set; }
    }
}