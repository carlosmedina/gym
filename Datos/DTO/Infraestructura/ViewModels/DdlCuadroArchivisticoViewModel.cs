﻿namespace Datos.DTO.Infraestructura.ViewModels
{
    public class DdlCuadroArchivisticoViewModel
    {
        public TiposCuadroArchivistico Tipo { get; set; }
        public bool SoloAgregados { get; set; }
    }
}
