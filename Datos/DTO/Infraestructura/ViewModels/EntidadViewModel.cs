﻿using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class EntidadViewModel : PaginacionViewModel
    {
        public Entidad Entidad { get; set; }
        public List<Entidad> Entidades { get; set; }
        public string IdPadre { get; set; }
    }
}
