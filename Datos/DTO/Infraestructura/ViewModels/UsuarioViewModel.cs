﻿using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class UsuarioViewModel: PaginacionViewModel
    {
        public Usuario Usuario { get; set; }
        public List<Usuario> Usuarios { get; set; }
        public string IdEntidad { get; set; }
    }
}
