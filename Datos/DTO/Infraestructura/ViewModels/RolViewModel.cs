﻿using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class RolViewModel
    {
        public Rol Rol { get; set; }
        public List<Rol> Roles { get; set; }
    }
}
