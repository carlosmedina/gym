﻿using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class EstadoViewModel : PaginacionViewModel
    {
        public Estado Estado { get; set; }
        public List<Estado> Estados { get; set; }
    }
}
