﻿using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class ClienteViewModel : PaginacionViewModel
    {
        public Cliente Cliente { get; set; }
        public List<Cliente> Clientes { get; set; }
    }
}
