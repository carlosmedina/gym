﻿using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class EdificioViewModel : PaginacionViewModel
    {
        public Edificio Edificio { get; set; }
        public List<Edificio> Edificios { get; set; }
        public string IdLocalidad { get; set; }
    }
}