﻿using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class InicioViewModel
    {
        public int CantidadMesDigitalizados { get; set; }
        public int TotalExpedientes { get; set; }
        public int CantidadPromedioDigitalizadosDia { get; set; }
        public int PromedioConsultasDiarias { get; set; }
        public int PromedioConsultasMes { get; set; }
        public List<SpResultadoMasConsultadoExpediente> MásConsultados { get; set; }
        public List<SpResultadoConsultasExpedientesMes> CantidadDigitalizadosPorMes { get; set; }
        public string NombreEntidad { get; set; }
        public string MesActual { get; set; }
    }
}
