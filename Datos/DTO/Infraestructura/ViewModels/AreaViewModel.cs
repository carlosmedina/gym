﻿using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class AreaViewModel : PaginacionViewModel
    {
        public Area Area { get; set; }
        public List<Area> Areas { get; set; }
        public string IdNivel { get; set; }
    }
}