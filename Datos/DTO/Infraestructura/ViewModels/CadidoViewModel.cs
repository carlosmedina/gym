﻿using System;
using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class CadidoViewModel : PaginacionViewModel
    {
        public Cadido Cadido { get; set; }
        public Expediente Expediente { get; set; }
        public List<CuadroArchivistico> CuadroArchivisticos { get; set; }
        public List<Cadido> Cadidos { get; set; }
        public string IdPadre { get; set; }
        public int IdFondo { get; set; }
        public int IdSubfondo { get; set; }
        public int IdSeccion { get; set; }
        public int IdSubseccion { get; set; }
        public int IdSerie { get; set; }
        public int IdSubserie { get; set; }
        public DateTime? FechaVigenciaInicio { get; set; }
        public DateTime? FechaVigenciaFin { get; set; }
        public int IdEntidad { get; set; }

        public bool ExisteSubfondo { get; set; }
        public bool ExisteSubseccion { get; set; }
        public bool ExisteSubserie { get; set; }

        public CuadroArchivistico ObtenerTipo(CuadroArchivistico cuadro, TiposCuadroArchivistico tipo)
        {
            while (true)
            {
                if (cuadro.PadreCuadroArchivistico == null) return null;

                if (cuadro.IDTipoCuadroArchivistico == (int)tipo)
                    return cuadro;
                if (cuadro.PadreCuadroArchivistico.IDTipoCuadroArchivistico == (int) tipo)
                    return cuadro.PadreCuadroArchivistico;

                    cuadro = cuadro.PadreCuadroArchivistico;
            }
        }
    }
}