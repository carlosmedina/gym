﻿using System.Collections.Generic;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class NivelViewModel : PaginacionViewModel
    {
        public Nivel Nivel { get; set; }
        public List<Nivel> Niveles { get; set; }
        public string IdEdificio { get; set; }
    }
}