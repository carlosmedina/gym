﻿namespace Datos.DTO.Infraestructura.ViewModels
{
    public class ContenedorBuscarViewModel
    {
        public int IDContenedor { get; set; }
        public string Nombre { get; set; }
        public string Mobilario { get; set; }
        public string Area { get; set; }
        public string Nivel { get; set; }
        public string Edificio { get; set; }
        public string Localidad { get; set; }
        public string Municipio { get; set; }
        public bool Agregado { get; set; }
        public bool Habilitado { get; set; }
    }
}
