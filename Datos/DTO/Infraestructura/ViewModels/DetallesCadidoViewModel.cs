﻿using System;

namespace Datos.DTO.Infraestructura.ViewModels
{
    public class DetallesCadidoViewModel
    {
        public int IdCadido { get; set; }
        public string IdCuadro { get; set; }
        public int IdDisposicionDocumental { get; set; }
        public string TiempoTramite { get; set; }
        public string TiempoConcentracion { get; set; }
        public string ValorDocumental { get; set; }
        public string DocumentalFundamentoLegal { get; set; }
        public string ClasificacionInformacion { get; set; }
        public string DisposicionDocumental { get; set; }
        public string FundamentoLegal { get; set; }
        public string Fondo { get; set; }
        public string Subfondo { get; set; }
        public string Seccion { get; set; }
        public string Subseccion { get; set; }
        public string Serie { get; set; }
        public string Subserie { get; set; }
        public string Titulo { get; set; }
        public string FechaCreacion { get; set; }
        public string FechaModificacion { get; set; }

        public static DetallesCadidoViewModel Obtener(Cadido cadido)
        {
            var cadidoDetalles = new DetallesCadidoViewModel();

            if (cadido == null) return null;

            cadidoDetalles.Subfondo = string.Empty;
            cadidoDetalles.Subseccion = string.Empty;
            cadidoDetalles.Subserie = string.Empty;

            var cuadroArchivistico = cadido.CuadroArchivistico;
            while (true)
            {
                if (cuadroArchivistico == null) break;

                switch (cuadroArchivistico.IDTipoCuadroArchivistico)
                {
                    case (int)TiposCuadroArchivistico.Fondo:
                        cadidoDetalles.Fondo = cuadroArchivistico.Nombre;
                        break;
                    case (int)TiposCuadroArchivistico.Subfondo:
                        cadidoDetalles.Subfondo = cuadroArchivistico.Nombre;
                        break;
                    case (int)TiposCuadroArchivistico.Seccion:
                        cadidoDetalles.Seccion = cuadroArchivistico.Nombre;
                        break;
                    case (int)TiposCuadroArchivistico.Subseccion:
                        cadidoDetalles.Subseccion = cuadroArchivistico.Nombre;
                        break;
                    case (int)TiposCuadroArchivistico.Serie:
                        cadidoDetalles.Serie = cuadroArchivistico.Nombre;
                        break;
                    case (int)TiposCuadroArchivistico.Subserie:
                        cadidoDetalles.Subserie = cuadroArchivistico.Nombre;
                        break;
                }

                cuadroArchivistico = cuadroArchivistico.PadreCuadroArchivistico;
            }

            cadidoDetalles.IdCadido = cadido.IDCadido;
            cadidoDetalles.IdCuadro = cadido.IDCuadroArchivistico.ToString();
            cadidoDetalles.TiempoTramite = cadido.TiempoTramite.ToString();
            cadidoDetalles.TiempoConcentracion = cadido.TiempoConcentracion.ToString();
            cadidoDetalles.FundamentoLegal = cadido.FundamentoLegal != null ? cadido.FundamentoLegal.Nombre : "-------";
            cadidoDetalles.ValorDocumental = cadido.ObtenerValorDocumental();           
            cadidoDetalles.DocumentalFundamentoLegal = cadido.DocumentalFundamentoLegal;
            cadidoDetalles.ClasificacionInformacion = cadido.TipoClasificacionInformacion.Nombre;
            cadidoDetalles.DisposicionDocumental = cadido.DisposicionDocumental.Nombre;
            cadidoDetalles.FechaCreacion = cadido.FechaCreacion.ToString("dd/MM/yyyy HH:mm:ss");
            cadidoDetalles.FechaModificacion = cadido.FechaModificacion.ToString("dd/MM/yyyy HH:mm:ss");
            cadidoDetalles.IdDisposicionDocumental = cadido.IDDisposicionDocumental;

            return cadidoDetalles;
        }
    }
}
