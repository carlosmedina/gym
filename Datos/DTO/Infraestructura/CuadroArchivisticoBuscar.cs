﻿using System;

namespace Datos.DTO.Infraestructura
{
    public class CuadroArchivisticoBuscar
    {
        public CuadroArchivistico CuadroArchivistico { get; set; }

        internal int IDCuadroArchivistico { get; set; }
        internal int? IDPadreCuadroArchivistico { get; set; }
        internal string Clave { get; set; }
        internal string Nombre { get; set; }
        internal CuadroArchivistico NivelUno { get; set; }
        internal CuadroArchivistico NivelDos { get; set; }
        internal CuadroArchivistico NivelTres { get; set; }
        internal CuadroArchivistico NivelCuatro { get; set; }
        internal CuadroArchivistico NivelCinco { get; set; }
        internal DateTime FechaModificacion { get; set; }
        internal DateTime FechaCreacion { get; set; }
        internal bool Habilitado { get; set; }
        public bool Agregado { get; set; }

        public void GenerarJerarquia()
        {
            CuadroArchivistico = new CuadroArchivistico
            {
                IDCuadroArchivistico = IDCuadroArchivistico,
                IDPadreCuadroArchivistico = IDPadreCuadroArchivistico,
                Clave = Clave,
                Nombre = Nombre,
                Habilitado = Habilitado,
                FechaCreacion = FechaCreacion,
                FechaModificado = FechaModificacion
            };

            var actual = CuadroArchivistico;
            var i = 1;
            while (true)
            {
                CuadroArchivistico siguiente = null;

                if (i == 1) siguiente = NivelUno;
                if (i == 2) siguiente = NivelDos;
                if (i == 3) siguiente = NivelTres;
                if (i == 4) siguiente = NivelCuatro;
                if (i == 5) siguiente = NivelCinco;

                if (siguiente == null) break;

                actual.PadreCuadroArchivistico = siguiente;

                actual = actual.PadreCuadroArchivistico;

                i++;
            }
        }
    }
}
