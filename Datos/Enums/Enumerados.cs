﻿namespace Datos.Enums
{
    public class Enumerados
    {
        public enum VariablesSesion
        {
            Aplicacion,
            Menu
        }

        public enum Genero
        {
            Femenino,
            Masculino
        }

        public enum GestionMenu
        {
            Menu = 1,
            Rol = 2,
            Usuario = 3
        }

        public enum TempData
        {
            Breadcrumbs,
            IdGestionarMenu,
            TipoGestion,
            Error,
            Mensaje,
            NombreTipoGestion,
            MenuBreadcrumbs,
            IdModificar,
            IdEmpleado
        }

        public enum Parametro
        {
            IdMenu,
            IdUsuario,
            IdRol,
            Habilitado,
            IdMenuPadre,
            IdLocalidad,
            IdEntidad,
            IdPuesto,
            IdEmpleado,
            IdEntidadOrganigrama,
            IdPadreEntidadOrganigrama,
            IdNivelOrganigrama,
            IdPadreNivelOrganigrama,
            IdEstado,
            IdMunicipio,
            IdEdificio,
            IdCuadroArchivistico,
            IdExpediente,
            IdCadido,
            IdCliente,
            IdNivel,
            IdArea,
            IdMobilario,
            IdContenedor,
            Mantener
        }
    }
}
