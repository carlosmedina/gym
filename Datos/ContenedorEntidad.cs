//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class ContenedorEntidad
    {
        public int IDEntidad { get; set; }
        public int IDContenedor { get; set; }
        public System.DateTime FechaCreacion { get; set; }
    
        public virtual Contenedor Contenedor { get; set; }
        public virtual Entidad Entidad { get; set; }
    }
}
