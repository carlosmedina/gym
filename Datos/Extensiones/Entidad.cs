﻿using System.ComponentModel.DataAnnotations;

namespace Datos
{
    [MetadataType(typeof(EntidadMedaData))]
    public partial class Entidad
    {
    }

    public class EntidadMedaData
    {
        [Display(Name = @"Fecha Creación")]
        public string FechaCreacion { get; set; }
        [Display(Name = @"Fecha Modificación")]
        public string FechaModificacion { get; set; }
    }
}
