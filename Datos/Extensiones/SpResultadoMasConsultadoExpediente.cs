﻿using Sistema.Extensiones;

namespace Datos
{
    public partial class SpResultadoMasConsultadoExpediente
    {
        public string Id64Expediente => IDExpediente.ToString().EncodeTo64();
    }
}
