﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Datos
{
    [MetadataType(typeof(CadidoMedaData))]
    public partial class Cadido
    {
        public string ObtenerValorDocumental()
        {
            var valorDocumental = new List<string>();

            if (EsAdministrativo) valorDocumental.Add("Administrativo");

            if (EsLegal) valorDocumental.Add("Legal");

            if (EsContableFiscal) valorDocumental.Add("Contable Fiscal");

            return string.Join(", ", valorDocumental);
        }

        public string ValorDocumental => ObtenerValorDocumental();
    }

    public class CadidoMedaData
    {
        [Display(Name = @"Admtvo.")]
        public bool EsAdministrativo { get; set; }
        [Display(Name = @"Legal")]
        public bool EsLegal { get; set; }
        [Display(Name = @"C. Fiscal")]
        public bool EsContableFiscal { get; set; }
    }
}
