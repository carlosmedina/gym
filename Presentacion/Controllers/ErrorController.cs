﻿
using System.Web.Mvc;

namespace Presentacion.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Error404()
        {
            return View();
        }
        public ActionResult Error403()
        {
            return View();
        }

        public ActionResult Errordefault()
        {
            return View();
        }
    }
}