﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Services;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Repositorios.Catalogos;
using Datos.Repositorios.Reportes;
using Negocio.Servicios;
using Sistema;

namespace Presentacion.Controllers
{
    public class InicioController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index()
        {
            var viewModel = new InicioViewModel();

            var idEntidad = ObtenerSesion().IdEntidad;
            var fechaInicio = DateTime.Now.AddDays(-31);
            var fechaFin = DateTime.Now.Date.AddDays(1).AddMilliseconds(-1);

            var anio = DateTime.Now.Year;
            var mes = DateTime.Now.Month;

            var fechaMesInicio = new DateTime(anio, mes, 1);
            var fechaMesFin = new DateTime(anio, mes, DateTime.DaysInMonth(anio, mes), 23, 59, 59);

            var fechaInicioGrafica = fechaMesInicio.AddMonths(-6);

            using (var contexto = new Contexto())
            {
                if (idEntidad > 0)
                {
                    var repositorioEntidad = new EntidadRepositorio(contexto);
                    viewModel.NombreEntidad = repositorioEntidad.ObtenerPropiedadUno(e => e.IDEntidad == idEntidad,
                        e => e.Nombre);
                }

                var repositorio = new ReportesRepositorio(contexto);

                viewModel.MesActual = FechaUtilidades.ObtenerNombreMes(DateTime.Now.Month);

                viewModel.CantidadMesDigitalizados = repositorio.ObtenerTotalDigitalizados(idEntidad, fechaMesInicio, fechaMesFin);
                viewModel.TotalExpedientes = repositorio.ObtenerTotalExpedientes(idEntidad);
                viewModel.CantidadPromedioDigitalizadosDia = repositorio.ObtenerDigitalizadosPromedioDia(idEntidad, fechaInicio, fechaFin);
                viewModel.PromedioConsultasDiarias = repositorio.ObtenerPromedioConsultasDiarias(idEntidad);
                viewModel.PromedioConsultasMes = repositorio.ObtenerPromedioConsultasMeses(idEntidad);
                viewModel.MásConsultados = repositorio.ObtenerExpedientesMasConsultados(idEntidad);

                var consultaMeses = repositorio.ObtenerMesesDigitalizados(idEntidad, fechaInicioGrafica, fechaMesFin);
                viewModel.CantidadDigitalizadosPorMes = ServiciosEstadisticas.RellenarMeses(consultaMeses, fechaInicioGrafica, fechaMesFin);
            }

            return View(viewModel);
        }

        [WebMethod]
        [HttpPost]
        public JsonResult ObtenerExpedientesMasConsultados(string tipo)
        {
            var fechaInicio = DateTime.Now;
            var fechaFin = DateTime.Now;

            var anio = fechaInicio.Year;
            var mes = fechaInicio.Month;

            var idEntidad = ObtenerSesion().IdEntidad;

            switch (tipo)
            {
                case "Dia":
                    fechaInicio = DateTime.Now.Date;
                    fechaFin = fechaInicio.AddDays(1).AddMilliseconds(-1);
                    break;
                case "Semana":
                    fechaInicio = DateTime.Now.Date;
                    fechaFin = fechaInicio.AddDays(1).AddMilliseconds(-1);
                    fechaInicio = fechaInicio.AddDays(-6);
                    break;
                case "Mes":
                    fechaInicio = new DateTime(anio, mes, 1);
                    fechaFin = new DateTime(anio, mes, DateTime.DaysInMonth(anio, mes), 23, 59, 59);
                    break;
                case "Anio":
                    fechaInicio = new DateTime(anio, 1, 1);
                    fechaFin = new DateTime(anio, 12, 31, 23, 59, 59);
                    break;
            }

            using (var bd = new Contexto())
            {
                var repositorio = new ReportesRepositorio(bd);

                var digitalizacionMeses = tipo == "Siempre"
                    ? repositorio.ObtenerExpedientesMasConsultados(idEntidad)
                    : repositorio.ObtenerExpedientesMasConsultados(idEntidad, fechaInicio, fechaFin);

                return Json(digitalizacionMeses);
            }
        }
    }
}