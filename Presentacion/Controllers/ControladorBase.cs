﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Datos;
using Datos.Enums;
using Datos.Repositorios.Configuracion;
using Datos.DTO.Infraestructura;
using Datos.Repositorios.Bitacora;
using Datos.Repositorios.Catalogos;
using Datos.Repositorios.Digitalizacion;
using Sistema.Extensiones;

namespace Presentacion.Controllers
{
    public class ControladorBase : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controlador = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var action = filterContext.ActionDescriptor.ActionName;
            var sesion = ObtenerSesion();

            base.OnActionExecuting(filterContext);

            if (sesion != null)
            {
                var permiso = TienePermiso(controlador, action);
                if (!permiso)
                {
                    if (controlador == "Login" && action == "CerrarSesion") return;
                    filterContext.Result = ObtenerRedireccionamiento("Inicio", "Index");
                }
                else
                {
                    if (controlador != "Menu") LimpiarTemporalesMenu();
                    if (controlador == "Login") return;
                }

                if (controlador != "Inicio") CargarBreadCrumbs(controlador, action);
                else ObtenerSesion().MenuBreadcrumb = null;
            }
            else if (controlador != "Login")
            {
                filterContext.Result = ObtenerRedireccionamiento("Login", "Index");
            }
        }

        private bool TienePermiso(string controlador, string accion)
        {
            var menuRolRepositorio = new MenuRolRepositorio();
            return menuRolRepositorio.TienePermisoPagina(ObtenerSesion().IdRol, controlador, accion);
        }
        
        public SistemaUsuario ObtenerSesion()
        {
            return ((SistemaUsuario)Session[Enumerados.VariablesSesion.Aplicacion.ToString()]);
        }
        
        private static RedirectToRouteResult ObtenerRedireccionamiento(string controlador, string accion, string area = null)
        {
            return new RedirectToRouteResult(new RouteValueDictionary(new
            {
                controller = controlador,
                action = accion,
                area
            }));
        }
        
        private void CargarBreadCrumbs(string controlador, string accion)
        {
            using (var bd = new Contexto())
            {
                var menuRepositorio = new MenuRepositorio(bd);
                var idMenu = menuRepositorio.ObtenerId(controlador, accion);
                var menus = menuRepositorio.BuscarOpcionMenuYOpcionesSuperiores(idMenu);
                ObtenerSesion().MenuBreadcrumb = menus;
            }
        }
        
        private void LimpiarTemporalesMenu()
        {
            LimpiarDatoTemporal(Enumerados.TempData.IdGestionarMenu);
            LimpiarDatoTemporal(Enumerados.TempData.TipoGestion);
            LimpiarDatoTemporal(Enumerados.TempData.NombreTipoGestion);
        }
        
        private void LimpiarDatoTemporal(Enumerados.TempData tempData)
        {
            TempData[tempData.ToString()] = null;
        }

        public void EscribirCorrectoMensaje(string mensaje)
        {
            TempData[Enumerados.TempData.Mensaje.ToString()] = mensaje;
        }

        public void EscribirErrorMensaje(string mensaje)
        {
            TempData[Enumerados.TempData.Mensaje.ToString()] = mensaje;
        }

        public T ObtenerDatoTemporal<T>(Enumerados.TempData tempData)
        {
            return (T)TempData[tempData.ToString()];
        }

        public T ObtenerDatoTemporal<T>(Enumerados.TempData tempData, bool mantener)
        {
            var regresar = (T)TempData[tempData.ToString()];
            if (mantener) GuardarDatoTemporal(tempData, regresar, true);
            return regresar;
        }

        public int ObtenerParametroGetEnInt(Enumerados.Parametro parametro)
        {
            return Request.QueryString[parametro.ToString()].DecodeFrom64().TryToInt();
        }

        public void GuardarDatoTemporal<T>(Enumerados.TempData tempData, T datoGuardar, bool mantener)
        {
            TempData[tempData.ToString()] = datoGuardar;

            if (mantener)
            {
                TempData.Keep(tempData.ToString());
            }
        }

        public void LimpiarModelState()
        {
            foreach (var model in ModelState.Values)
            {
                model.Errors.Clear();
            }
        }

        public void GuardarMovimientoAuditoria(int idEntidad, ModulosAuditoria modulo, AccionesAuditoria accion, string descripcion = null)
        {
            using (var contexto = new Contexto())
            {
                if (descripcion == null)
                {
                    switch (modulo)
                    {
                        case ModulosAuditoria.Roles:
                            var repositorioRoles = new RolRepositorio(contexto);
                            descripcion = repositorioRoles.ObtenerPropiedadUno(r => r.IDRol == idEntidad,
                                                                                    r => r.Nombre);
                            break;
                        case ModulosAuditoria.Areas:
                            var repositorioArea = new ClienteRepositorio(contexto);
                            descripcion = repositorioArea.ObtenerPropiedadUno(a => a.IdCliente == idEntidad,
                                a => a.Nombre);
                            break;
                        case ModulosAuditoria.Contenedores:
                            var repositorioContenedor = new ContenedorRepositorio(contexto);
                            descripcion = repositorioContenedor.ObtenerPropiedadUno(c => c.IDContenedor == idEntidad,
                                c => c.Nombre);
                            break;
                        case ModulosAuditoria.Edificios:
                            var repositorioEdificio = new EdificioRepositorio(contexto);
                            descripcion = repositorioEdificio.ObtenerPropiedadUno(e => e.IDEdificio == idEntidad,
                                e => e.Nombre);
                            break;
                        case ModulosAuditoria.Mobiliarios:
                            var repositorioMobilario = new MobilarioRepositorio(contexto);
                            descripcion = repositorioMobilario.ObtenerPropiedadUno(m => m.IDMobilario == idEntidad,
                                m => m.Nombre);
                            break;
                        case ModulosAuditoria.Niveles:
                            var repositorioNivel = new NivelRepositorio(contexto);
                            descripcion = repositorioNivel.ObtenerPropiedadUno(n => n.IDNivel == idEntidad,
                                n => n.Nombre);
                            break;
                        case ModulosAuditoria.Localidades:
                            var repositorioLocalidad = new LocalidadRepositorio(contexto);
                            descripcion = repositorioLocalidad.ObtenerPropiedadUno(l => l.IDLocalidad == idEntidad,
                                l => l.Nombre);
                            break;
                        case ModulosAuditoria.Fondos:
                        case ModulosAuditoria.SubFondos:
                        case ModulosAuditoria.Secciones:
                        case ModulosAuditoria.SubSecciones:
                        case ModulosAuditoria.Series:
                        case ModulosAuditoria.SubSeries:
                            var repositorioCuadro = new CuadroArchivisticoRepositorio(contexto);
                            descripcion = repositorioCuadro.ObtenerPropiedadUno(c => c.IDCuadroArchivistico == idEntidad,
                                c => c.Nombre);
                            break;
                        case ModulosAuditoria.Usuarios:
                            var repositorioUsuario = new UsuarioRepositorio(contexto);
                            descripcion = repositorioUsuario.ObtenerPropiedadUno(u => u.IDUsuario == idEntidad,
                                u => u.Nombre);
                            break;
                        case ModulosAuditoria.Puestos:
                            var repositorioPuesto = new PuestoRepostorio(contexto);
                            descripcion = repositorioPuesto.ObtenerPropiedadUno(p => p.IDPuesto == idEntidad,
                                p => p.Nombre);
                            break;
                        case ModulosAuditoria.Entidades:
                            var repositorioEntidad = new EntidadRepositorio(contexto);
                            descripcion = repositorioEntidad.ObtenerPropiedadUno(e => e.IDEntidad == idEntidad,
                                e => e.Nombre);
                            break;
                        case ModulosAuditoria.Cadido:
                            var repositorioCadido = new CadidoRepositorio(contexto);
                            descripcion = repositorioCadido.ObtenerPropiedadUno(c => c.IDCadido == idEntidad,
                                c => c.CuadroArchivistico.Clave + " - " + c.CuadroArchivistico.Nombre);
                            break;
                    }
                }

                var auditoria = new Auditoria
                {
                    IDEntidad = idEntidad,
                    Fecha = DateTime.Now,
                    IDModuloAuditoria = (byte) modulo,
                    IDAccionAuditoria = (byte) accion,
                    IDUsuario = ObtenerSesion().IdUsuario,
                    Descripcion = descripcion
                };

                if (modulo == ModulosAuditoria.Expedientes) auditoria.IDExpediente = idEntidad;

                var repositorio = new AuditoriaRepositorio(contexto);
                repositorio.Guardar(auditoria);

                if (modulo == ModulosAuditoria.Expedientes && accion == AccionesAuditoria.Consulta)
                {
                    var repositorioExpedientes = new ExpedienteRepositorio(contexto);
                    repositorioExpedientes.AumentarConsulta(idEntidad);
                }

                contexto.SaveChanges();
            }
        }

        public string ObtenerUrlAbsoluta(string rutaRelativa)
        {
            var urlBase = $"{Request.Url.Scheme}://{Request.Url.Authority}{Url.Content("~")}";

            return urlBase + rutaRelativa;
        }
    }
}