﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Resources;
using System.Web.Mvc;
using Datos;
using Datos.Enums;
using Datos.DTO.Infraestructura;
using Datos.Repositorios.Configuracion;
using Sistema;
using Sistema.Seguridad;

namespace Presentacion.Controllers
{
    public class LoginController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View("Index","Layouts/_LayoutLogin");
        }

        [HttpPost]
        public ActionResult Index(Usuario usuario)
        {
            if (!ModelState.IsValid) return null;

            var contraseñaHash = Encriptacion.GetHashSha512(usuario.Contrasena);
            
            using (var bd = new Contexto())
            {
                var usuarioRepositorio = new UsuarioRepositorio(bd);
                var usuarioEncontrado = usuarioRepositorio.BuscarUsuarioPorCorreoYContrasena(usuario.NombreUsuario,
                    contraseñaHash);

                if (usuarioEncontrado == null)
                {
                    EscribirErrorMensaje(General.ErrorUsuarioIncorrecto);
                }
                else if (!usuarioEncontrado.Habilitado)
                {
                    EscribirErrorMensaje(General.ErrorUsuarioInhabilitado);
                }
                else
                {
                    GuardarUsuarioEnSesion(usuarioEncontrado);
                    return RedirectToAction("Index", "Inicio");
                }
            }

            return View("Index", "Layouts/_LayoutLogin");
        }

        [HttpGet]
        public ActionResult CerrarSesion()
        {
            GuardarMovimientoAuditoria(ObtenerSesion().IdUsuario, ModulosAuditoria.Sesion,
                AccionesAuditoria.SalidaSesion, null);
            Session.Abandon();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult SolicitarContrasena()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> SolicitarContrasena(string email)
        {
            using (var bd = new Contexto())
            {
                var usuarioRepositorio = new UsuarioRepositorio(bd);
                var usuarioBd = usuarioRepositorio.BuscarPorCorreo(email);

                if (usuarioBd != null)
                {
                    var codigo = Comun.GenerarGuid();
                    var solicitudRepositorio = new SolicitudContrasenaRepositorio(bd);

                    solicitudRepositorio.Guardar(usuarioBd.IDUsuario, codigo);

                    await Correo.RestablecerContrasena(usuarioBd.Email, General.RestablecerContrasena, codigo);

                    EscribirCorrectoMensaje(General.CorreoEnviado);
                    bd.SaveChanges();
                }
                else
                {
                    EscribirErrorMensaje(General.NoExisteCorreoElectronico);
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult RestablecerContrasena(string c)
        {
            Usuario usuario;
            using (var bd = new Contexto())
            {
                var solicitudRepositorio = new SolicitudContrasenaRepositorio(bd);
                usuario = solicitudRepositorio.ObtenerUsuario(c);
                if (usuario == null) return RedirectToAction("Index");
            }
            return View(usuario);
        }

        [HttpPost]
        public ActionResult RestablecerContrasena(string contrasena, string c)
        {
            using (var bd = new Contexto())
            {
                var solicitudRepositorio = new SolicitudContrasenaRepositorio(bd);
                var usuarioRepositorio = new UsuarioRepositorio(bd);
                usuarioRepositorio.ActualizarContrasena(solicitudRepositorio.ObtenerIdUsuario(c), contrasena);
                solicitudRepositorio.DesactivarCodigo(solicitudRepositorio.RegresoIdContrasena(c));
                bd.SaveChanges();
            }
            EscribirCorrectoMensaje(General.GuardadoCorrecto);
            return RedirectToAction("Index");
        }
        
        private void GuardarUsuarioEnSesion(Usuario usuarioEncontrado)
        {
            var usuario = new SistemaUsuario
            {
                Nombre = usuarioEncontrado.NombreUsuario,
                CorreoElectronico = usuarioEncontrado.Email,
                IdUsuario = usuarioEncontrado.IDUsuario,
                IdRol = usuarioEncontrado.Rol.IDRol,
                NombrePuesto = usuarioEncontrado.Rol.Nombre,
                UrlFoto = usuarioEncontrado.UrlFoto,
                TieneMenuPersonalizado = usuarioEncontrado.MenuPersonalizado,
                TipoUsuario = ObtenerTipoUsuario(usuarioEncontrado),
                UrlPaginaInicio = usuarioEncontrado.Rol.Menu.Destino,
                IdEntidad = usuarioEncontrado.IDEntidad ?? 0,
                Menu = usuarioEncontrado.MenuPersonalizado != true
                    ? ObtenerMenuUsuario(usuarioEncontrado.IDRol, 0)
                    : ObtenerMenuUsuario(0, usuarioEncontrado.IDUsuario),
                Permisos = usuarioEncontrado.Rol.Permisos,
            };

            Session[Enumerados.VariablesSesion.Aplicacion.ToString()] = usuario;

            GuardarMovimientoAuditoria(usuario.IdUsuario, ModulosAuditoria.Sesion, AccionesAuditoria.InicioSesion, null);
        }

        private static TipoUsuario ObtenerTipoUsuario(Usuario usuario)
        {
            return usuario.NombreUsuario != null ? TipoUsuario.Empleado : 0;
        }

        private static List<Menu> ObtenerMenuUsuario(int idRol, int idUsuario)
        {
            if (idUsuario != 0)
            {
                var menuUsuarioModelo = new MenuUsuarioRepositorio();
                return menuUsuarioModelo.BuscarMenu(idUsuario, true);
            }
            var menuRolModelo = new MenuRolRepositorio();
            return menuRolModelo.BuscarMenu(idRol, true);
        }
    }
}