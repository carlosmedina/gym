﻿using System.Web.Mvc;
using System.Web.Services;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Repositorios.Bitacora;
using Presentacion.Controllers;
using Presentacion.Fabrica;
using Sistema.Extensiones;
using Sistema.Paginador;

namespace Presentacion.Areas.Bitacora.Controllers
{
    public class AuditoriaController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index(AuditoriaViewModel auditoriaViewModel)
        {
            auditoriaViewModel.Auditoria = new Auditoria {Usuario = new Usuario(), AccionAuditoria = new AccionAuditoria()};
            using (var bd = new Contexto())
            {
                var auditoriaRepositorio = new AuditoriaRepositorio(bd);
                auditoriaViewModel.Auditorias = auditoriaRepositorio.Buscar(auditoriaViewModel);
                auditoriaViewModel.TotalEncontrados = auditoriaRepositorio.ObtenerTotalRegistros(auditoriaViewModel);
                auditoriaViewModel.Paginas = Paginar.ObtenerCantidadPaginas(auditoriaViewModel.TotalEncontrados,
                    auditoriaViewModel.TamanoPagina);
            }
            return View(auditoriaViewModel);
        }

        [HttpPost]
        public ActionResult Index(AuditoriaViewModel auditoriaViewModel, string pagina)
        {
            if (string.IsNullOrEmpty(pagina)) pagina = "1";
            auditoriaViewModel.PaginaActual = pagina.TryToInt();

            if (auditoriaViewModel.FechaFin.HasValue)
                auditoriaViewModel.FechaFin = auditoriaViewModel.FechaFin.Value.AddDays(1).AddSeconds(-1);

            using (var bd = new Contexto())
            {
                var auditoriaRepositorio = new AuditoriaRepositorio(bd);
                auditoriaViewModel.Auditorias = auditoriaRepositorio.Buscar(auditoriaViewModel);
                auditoriaViewModel.TotalEncontrados = auditoriaRepositorio.ObtenerTotalRegistros(auditoriaViewModel);
                auditoriaViewModel.Paginas = Paginar.ObtenerCantidadPaginas(auditoriaViewModel.TotalEncontrados,
                    auditoriaViewModel.TamanoPagina);
            }

            LimpiarModelState();

            return View(auditoriaViewModel);
        }

        [HttpPost]
        [WebMethod]
        public JsonResult Obtener(string idAuditoria)
        {
            Auditoria auditoria;
            using (var bd = new Contexto())
            {
                var auditoriaRepositorio = new AuditoriaRepositorio(bd);
                auditoria = auditoriaRepositorio.BuscarPorIdAuditoria(idAuditoria.DecodeFrom64().TryToInt());
            }
            return Json(Factory.Obtener(auditoria));
        }
    }
}