﻿using System;
using System.Web.Mvc;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Repositorios.Catalogos;
using Presentacion.Controllers;
using Resources;
using Sistema.Extensiones;
using Sistema.Paginador;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class EntidadController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index(EntidadViewModel viewModel)
        {
            var idPadre = viewModel.IdPadre.DecodeFrom64().TryToInt();

            viewModel.Entidad = new Entidad
            {
                IDEntidadPadre = idPadre > 0 ? idPadre : (int?) null,
                Habilitado = true,
                Localidad = new Localidad()
            };

            using (var bd = new Contexto())
            {
                var repositorio = new EntidadRepositorio(bd);
                viewModel.Entidades = repositorio.Buscar(viewModel);
                viewModel.TotalEncontrados = repositorio.ObtenerTotalRegistros(viewModel);
                viewModel.Paginas = Paginar.ObtenerCantidadPaginas(viewModel.TotalEncontrados,
                    viewModel.TamanoPagina);

                if (viewModel.Entidad.IDEntidadPadre > 0)
                {
                    viewModel.Entidad.EntidadPadre = repositorio
                        .BuscarUnoSolo(e => e.IDEntidad == viewModel.Entidad.IDEntidadPadre);
                }
            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(EntidadViewModel viewModel, string pagina)
        {
            viewModel.PaginaActual = pagina.TryToInt();

            if (viewModel.PaginaActual <= 0) viewModel.PaginaActual = 1;

            using (var bd = new Contexto())
            {
                var repositorio = new EntidadRepositorio(bd);
                viewModel.Entidades = repositorio.Buscar(viewModel);
                viewModel.TotalEncontrados = repositorio.ObtenerTotalRegistros(viewModel);
                viewModel.Paginas = Paginar.ObtenerCantidadPaginas(viewModel.TotalEncontrados,
                    viewModel.TamanoPagina);

                if (viewModel.Entidad.IDEntidadPadre > 0)
                {
                    viewModel.Entidad.EntidadPadre = repositorio
                        .BuscarUnoSolo(e => e.IDEntidad == viewModel.Entidad.IDEntidadPadre);
                }
            }

            LimpiarModelState();

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Guardar(string idEntidad, string idPadre)
        {
            var entidad = new Entidad
            {
                IDEntidad = idEntidad.DecodeFrom64().TryToInt(),
                Localidad = new Localidad(),
                IDEntidadPadre = idPadre.DecodeFrom64().TryToInt()
            };
            
            using (var bd = new Contexto())
            {
                var repositorioEstado = new EstadoRepositorio(bd);
                var repositorio = new EntidadRepositorio(bd);

                entidad.Localidad.IDEstado = repositorioEstado.ObtenerIdEstadoPredeterminado();

                if (entidad.IDEntidad > 0)
                {
                    entidad = repositorio.BuscarUnoSolo(e => e.IDEntidad == entidad.IDEntidad,
                        new[] {"Localidad.Municipio", "EntidadPadre"});
                }
                else if(entidad.IDEntidadPadre > 0)
                {
                    entidad.EntidadPadre = repositorio.BuscarUnoSolo(e => e.IDEntidad == entidad.IDEntidadPadre);
                }
            }

            return View(entidad);
        }

        [HttpPost]
        public ActionResult Guardar(Entidad entidad, string idPadre)
        {
            entidad.IDEntidad = Request.QueryString["idEntidad"].DecodeFrom64().TryToInt();
            
            var intIdPadre = idPadre.DecodeFrom64().TryToInt();
            if (intIdPadre > 0) entidad.IDEntidadPadre = intIdPadre;

            ModelState["idEntidad"]?.Errors.Clear();

            entidad.Localidad = null;

            AccionesAuditoria accion;
            if (ModelState.IsValid)
            {
                using (var bd = new Contexto())
                {
                    var localidadRepositorio = new EntidadRepositorio(bd);

                    if (entidad.IDEntidad > 0)
                    {
                        entidad.Habilitado = true;
                        entidad.FechaModificacion = DateTime.Now;
                        localidadRepositorio.Modificar(entidad);
                        accion = AccionesAuditoria.Modificar;
                    }
                    else
                    {
                        entidad.Habilitado = true;
                        entidad.FechaCreacion = DateTime.Now;
                        entidad.FechaModificacion = entidad.FechaCreacion;
                        localidadRepositorio.Guardar(entidad);
                        accion = AccionesAuditoria.Guardar;
                    }

                    GuardarMovimientoAuditoria(entidad.IDEntidad, ModulosAuditoria.Entidades, accion, entidad.Nombre);
                    bd.SaveChanges();
                }

                EscribirCorrectoMensaje(General.GuardadoCorrecto);
            }
            else
            {
                EscribirErrorMensaje(string.Format(General.FalloGuardado, "Entidad"));
            }

            if (intIdPadre > 0)
                return RedirectToAction("Guardar",
                    new {idEntidad = entidad.IDEntidad.ToString().EncodeTo64(), idPadre});
            
            return RedirectToAction("Guardar", new {idEntidad = entidad.IDEntidad.ToString().EncodeTo64()});
        }

        [HttpGet]
        public ActionResult Habilitar(string idEntidad)
        {
            var idEntidadCambiar = idEntidad.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var localidadRepositorio = new EntidadRepositorio(bd);
                localidadRepositorio.CambiarHabilitado(idEntidadCambiar, true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);

                GuardarMovimientoAuditoria(idEntidadCambiar, ModulosAuditoria.Entidades, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Deshabilitar(string idEntidad)
        {
            var idEntidadCambiar = idEntidad.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var localidadRepositorio = new EntidadRepositorio(bd);
                localidadRepositorio.CambiarHabilitado(idEntidadCambiar, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);

                GuardarMovimientoAuditoria(idEntidadCambiar, ModulosAuditoria.Entidades, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}