﻿using System.Web.Mvc;
using System.Web.Services;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Enums;
using Datos.Repositorios.Catalogos;
using Presentacion.Controllers;
using Resources;
using Sistema.Extensiones;
using Sistema.Paginador;
using System.Linq;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class ClienteController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index(ClienteViewModel clienteViewModel)
        {
            clienteViewModel.Cliente = new Cliente { Habilitado = true };
            using (var bd = new Contexto())
            {                
                var clienteRepositorio = new ClienteRepositorio(bd);
                clienteViewModel.Clientes = clienteRepositorio.Buscar(clienteViewModel);
                clienteViewModel.TotalEncontrados = clienteRepositorio.ObtenerTotalRegistros(clienteViewModel);
                clienteViewModel.Paginas = Paginar.ObtenerCantidadPaginas(clienteViewModel.TotalEncontrados,
                   clienteViewModel.TamanoPagina);
            }

            return View(clienteViewModel);
        }

        [HttpPost]
        public ActionResult Index(ClienteViewModel clienteViewModel, string pagina)
        {

            if (clienteViewModel.PaginaActual <= 0) clienteViewModel.PaginaActual = 1;

            using (var bd = new Contexto())
            {
                var clienteRepositorio = new ClienteRepositorio(bd);
                clienteViewModel.Clientes = clienteRepositorio.Buscar(clienteViewModel);
                clienteViewModel.TotalEncontrados = clienteRepositorio.ObtenerTotalRegistros(clienteViewModel);
                clienteViewModel.Paginas = Paginar.ObtenerCantidadPaginas(clienteViewModel.TotalEncontrados,
                    clienteViewModel.TamanoPagina);
            }

            LimpiarModelState();

            return View(clienteViewModel);
        }

        [HttpGet]
        public ActionResult Guardar(string idCliente)
        {
            var cliente = new Cliente {IdCliente = idCliente.DecodeFrom64().TryToInt()};

            using (var bd = new Contexto())
            {
                var clienteRepositorio = new ClienteRepositorio(bd);
                cliente = clienteRepositorio.BuscarUnoSolo(e => e.IdCliente == cliente.IdCliente);
            }

            return View(cliente);
        }

        [HttpPost]
        public ActionResult Guardar(Cliente cliente)
        {
            cliente.IdCliente = Request.QueryString["idCliente"].DecodeFrom64().TryToInt();

            ModelState["idCliente"]?.Errors.Clear();

            if (ModelState.IsValid)
            {
                cliente.IdCliente = ObtenerParametroGetEnInt(Enumerados.Parametro.IdCliente);

                using (var bd = new Contexto())
                {
                    var areaRepositorio = new ClienteRepositorio(bd);
                    AccionesAuditoria accion;
                    if (cliente.IdCliente > 0)
                    {
                        cliente.Habilitado = true;
                        areaRepositorio.Modificar(cliente);
                        accion = AccionesAuditoria.Modificar;
                    }
                    else
                    {
                        cliente.Habilitado = true;
                        areaRepositorio.Guardar(cliente);
                        accion = AccionesAuditoria.Guardar;
                    }
                    //GuardarMovimientoAuditoria(cliente.IdCliente, ModulosAuditoria.Areas, accion, area.Nombre);
                    bd.SaveChanges();
                }

                EscribirCorrectoMensaje(General.GuardadoCorrecto);
            }
            else
            {
                EscribirErrorMensaje(string.Format(General.FalloGuardado, "Cliente"));
            }

            dynamic parametrosGet;


            parametrosGet = new { idCliente = cliente.IdCliente.ToString().EncodeTo64() };

            return RedirectToAction("Guardar", parametrosGet);
        }

        [WebMethod]
        [HttpPost]
        public JsonResult ListaAreas(int idNivel, string buscar, bool filtrarAgregados = false)
        {
            var lista = filtrarAgregados
                ? Json(ClienteRepositorio.Buscar(idNivel, buscar, ObtenerSesion().IdEntidad))
                : Json(ClienteRepositorio.Buscar(idNivel, buscar));
            return lista;
        }

        [HttpGet]
        public ActionResult Habilitar(string idCliente)
        {
            var idClienteCambiar = idCliente.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var areaRepositorio = new ClienteRepositorio(bd);
                areaRepositorio.CambiarHabilitado(idClienteCambiar, true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);

                //GuardarMovimientoAuditoria(idClienteCambiar, ModulosAuditoria.Areas, AccionesAuditoria.Habilitar);

                bd.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Deshabilitar(string idCliente, string idNivelRegresar)
        {
            var idClienteCambiar = idCliente.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var areaRepositorio = new ClienteRepositorio(bd);
                areaRepositorio.CambiarHabilitado(idClienteCambiar, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);

                //GuardarMovimientoAuditoria(idClienteCambiar, ModulosAuditoria.Areas, AccionesAuditoria.Habilitar);

                bd.SaveChanges();
            }
            return RedirectToAction("Index", new { idNivel = idNivelRegresar });
        }
    }
}