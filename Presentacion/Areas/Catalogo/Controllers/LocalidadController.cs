﻿using System.Web.Mvc;
using System.Web.Services;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Enums;
using Datos.Repositorios.Catalogos;
using Presentacion.Controllers;
using Resources;
using Sistema.Extensiones;
using Sistema.Paginador;
using System;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class LocalidadController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index(LocalidadViewModel localidadViewModel)
        {
            localidadViewModel.Localidad = new Localidad {Habilitado = true, Municipio = new Municipio()};
            var idMunicipio = localidadViewModel.IdMunicipio.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                var estadoRepositorio = new EstadoRepositorio(bd);
                localidadViewModel.Localidad.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();

                if (idMunicipio > 0)
                {
                    var municipioRepositorio = new MunicipioRepositorio(bd);
                    var municipio =
                        municipioRepositorio.BuscarUnoSolo(
                            m => m.IDMunicipio == idMunicipio && m.IDEstado == localidadViewModel.Localidad.IDEstado);
                    if (municipio != null) localidadViewModel.Localidad.Municipio = municipio;
                }

                var localidadRepositorio = new LocalidadRepositorio(bd);
                localidadViewModel.Localidades = localidadRepositorio.Buscar(localidadViewModel);
                localidadViewModel.TotalEncontrados = localidadRepositorio.ObtenerTotalRegistros(localidadViewModel);
                localidadViewModel.Paginas = Paginar.ObtenerCantidadPaginas(localidadViewModel.TotalEncontrados,
                    localidadViewModel.TamanoPagina);
            }

            return View(localidadViewModel);
        }

        [HttpPost]
        public ActionResult Index(LocalidadViewModel localidadViewModel, string pagina)
        {
            localidadViewModel.Localidad.Municipio = new Municipio();
            localidadViewModel.PaginaActual = pagina.TryToInt();
            var idMunicipio = localidadViewModel.IdMunicipio.DecodeFrom64().TryToInt();

            if (localidadViewModel.PaginaActual <= 0) localidadViewModel.PaginaActual = 1;

            using (var bd = new Contexto())
            {
                var estadoRepositorio = new EstadoRepositorio(bd);
                localidadViewModel.Localidad.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();

                if (idMunicipio > 0)
                {
                    var municipioRepositorio = new MunicipioRepositorio(bd);
                    var municipio =
                        municipioRepositorio.BuscarUnoSolo(
                            m => m.IDMunicipio == idMunicipio && m.IDEstado == localidadViewModel.Localidad.IDEstado);
                    if (municipio != null) localidadViewModel.Localidad.Municipio = municipio;
                }

                var localidadRepositorio = new LocalidadRepositorio(bd);
                localidadViewModel.Localidades = localidadRepositorio.Buscar(localidadViewModel);
                localidadViewModel.TotalEncontrados = localidadRepositorio.ObtenerTotalRegistros(localidadViewModel);
                localidadViewModel.Paginas = Paginar.ObtenerCantidadPaginas(localidadViewModel.TotalEncontrados,
                                                    localidadViewModel.TamanoPagina);
            }
            
            LimpiarModelState();
            
            return View(localidadViewModel);
        }

        [HttpGet]
        public ActionResult Guardar(string idLocalidad, string idMunicipioRegresar)
        {
            var localidad = new Localidad {IDLocalidad = idLocalidad.DecodeFrom64().TryToInt()};
            ViewBag.IdMunicipioRegresar = idMunicipioRegresar.DecodeFrom64().TryToInt();

            if (localidad.IDLocalidad > 0)
            {
                using (var bd = new Contexto())
                {
                    var localidadRepositorio = new LocalidadRepositorio(bd);
                    localidad = localidadRepositorio.BuscarPorId(localidad.IDLocalidad);
                }
            }
            else
            {
                localidad = new Localidad
                {
                    IDMunicipio = ViewBag.IdMunicipioRegresar,
                    Estado = new Estado(),
                    Municipio = new Municipio()
                };
            }
            return View(localidad);
        }

        [HttpPost]
        public ActionResult Guardar(Localidad localidad)
        {
            localidad.IDLocalidad = Request.QueryString["idLocalidad"].DecodeFrom64().TryToInt();
            var idMunicipio = Request.QueryString["idMunicipioRegresar"].DecodeFrom64().TryToInt();
            localidad.Municipio = null;

            ModelState["idLocalidad"]?.Errors.Clear();

            if (ModelState.IsValid)
            {
                localidad.IDLocalidad = ObtenerParametroGetEnInt(Enumerados.Parametro.IdLocalidad);
                AccionesAuditoria accion;
                using (var bd = new Contexto())
                {
                    var localidadRepositorio = new LocalidadRepositorio(bd);
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    localidad.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();

                    localidad.FechaModificacion = DateTime.Now;

                    if (localidad.IDLocalidad > 0)
                    {
                        localidad.Habilitado = true;
                        localidadRepositorio.Modificar(localidad);
                        accion = AccionesAuditoria.Modificar;
                    }
                    else
                    {
                        localidad.Habilitado = true;
                        localidad.FechaCreacion = localidad.FechaModificacion;
                        localidadRepositorio.Guardar(localidad);
                        accion = AccionesAuditoria.Guardar;
                    }

                    GuardarMovimientoAuditoria(localidad.IDLocalidad, ModulosAuditoria.Localidades, accion, localidad.Nombre);
                    bd.SaveChanges();
                }

                EscribirCorrectoMensaje(General.GuardadoCorrecto);
            }
            else
            {
                EscribirErrorMensaje(string.Format(General.FalloGuardado, "Localidad"));
            }

            dynamic parametrosGet;

            if (idMunicipio > 0)
                parametrosGet = new
                {
                    idLocalidad = localidad.IDLocalidad.ToString().EncodeTo64(),
                    idMunicipioRegresar = idMunicipio.ToString().EncodeTo64()
                };
            else
                parametrosGet = new {idLocalidad = localidad.IDLocalidad.ToString().EncodeTo64()};

            return RedirectToAction("Guardar", parametrosGet);
        }

        [WebMethod]
        [HttpPost]
        public JsonResult ListaLocalidades(int idMunicipio, int idEstado, string buscar, bool filtrarAgregados = false)
        {
            var lista = filtrarAgregados
                ? Json(LocalidadRepositorio.Buscar(idEstado, idMunicipio, buscar, ObtenerSesion().IdEntidad))
                : Json(LocalidadRepositorio.Buscar(idEstado, idMunicipio, buscar));
            return lista;
        }

        [HttpGet]
        public ActionResult Habilitar(string idLocalidad, string idMunicipioRegresar)
        {
            var idLocalidadCambiar = idLocalidad.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                
                var localidadRepositorio = new LocalidadRepositorio(bd);
                localidadRepositorio.CambiarHabilitado(idLocalidadCambiar, true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);

                GuardarMovimientoAuditoria(idLocalidadCambiar, ModulosAuditoria.Localidades, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }
            return RedirectToAction("Index", new {IdMunicipio = idMunicipioRegresar});
        }

        [HttpGet]
        public ActionResult Deshabilitar(string idLocalidad, string idMunicipioRegresar)
        {
            var idLocalidadCambiar = idLocalidad.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
               
                var localidadRepositorio = new LocalidadRepositorio(bd);
                localidadRepositorio.CambiarHabilitado(idLocalidadCambiar, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);

                GuardarMovimientoAuditoria(idLocalidadCambiar, ModulosAuditoria.Localidades, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }
            return RedirectToAction("Index", new {IdMunicipio = idMunicipioRegresar});
        }
    }
}
