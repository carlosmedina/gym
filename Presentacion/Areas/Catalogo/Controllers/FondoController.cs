﻿using System;
using System.Web.Mvc;
using System.Web.Services;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Enums;
using Datos.Recursos;
using Sistema.Paginador;
using Sistema.Extensiones;
using Presentacion.Controllers;
using Datos.Repositorios.Configuracion;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class FondoController : ControladorBase
    {
        public ActionResult Index(CuadroArchivisticoViewModel cuadroArchivisticoViewModel)
        {
            cuadroArchivisticoViewModel.CuadroArchivistico = new CuadroArchivistico { Habilitado = true };
            var idPadre = cuadroArchivisticoViewModel.IdPadre.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                var archivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);
                cuadroArchivisticoViewModel.CuadroArchivistico.IDPadreCuadroArchivistico = idPadre;
                cuadroArchivisticoViewModel.CuadroArchivistico.IDTipoCuadroArchivistico = (int)TiposCuadroArchivistico.Fondo;
                cuadroArchivisticoViewModel.CuadroArchivisticos = archivisticoRepositorio.Buscar(cuadroArchivisticoViewModel);
                cuadroArchivisticoViewModel.TotalEncontrados = archivisticoRepositorio.ObtenerTotalRegistros(cuadroArchivisticoViewModel);
                cuadroArchivisticoViewModel.Paginas = Paginar.ObtenerCantidadPaginas(cuadroArchivisticoViewModel.TotalEncontrados, cuadroArchivisticoViewModel.TamanoPagina);
            }

            return View(cuadroArchivisticoViewModel);
        }

        [HttpPost]
        public ActionResult Index(CuadroArchivisticoViewModel cuadroArchivisticoViewModel, string pagina)
        {
            if (string.IsNullOrEmpty(pagina)) pagina = "1";
            cuadroArchivisticoViewModel.PaginaActual = pagina.TryToInt();

            var idPadre = cuadroArchivisticoViewModel.IdPadre.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                var archivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);
                cuadroArchivisticoViewModel.CuadroArchivistico.IDPadreCuadroArchivistico = idPadre;
                cuadroArchivisticoViewModel.CuadroArchivistico.IDTipoCuadroArchivistico = (int)TiposCuadroArchivistico.Fondo;
                cuadroArchivisticoViewModel.CuadroArchivisticos = archivisticoRepositorio.Buscar(cuadroArchivisticoViewModel);
                cuadroArchivisticoViewModel.TotalEncontrados = archivisticoRepositorio.ObtenerTotalRegistros(cuadroArchivisticoViewModel);
                cuadroArchivisticoViewModel.Paginas = Paginar.ObtenerCantidadPaginas(cuadroArchivisticoViewModel.TotalEncontrados,
                    cuadroArchivisticoViewModel.TamanoPagina);
            }

            LimpiarModelState();

            return View(cuadroArchivisticoViewModel);
        }

        public ActionResult Guardar(string idCuadroArchivistico)
        {
            var idCuadroArchivisticoBuscar = idCuadroArchivistico.DecodeFrom64().TryToInt();
            
            using (var bd = new Contexto())
            {
                var archivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);

                CuadroArchivistico cuadroArchivistico;
                if (idCuadroArchivisticoBuscar > 0)
                {
                    cuadroArchivistico = archivisticoRepositorio
                        .BuscarPorIdCuadroArchivistico(idCuadroArchivisticoBuscar);
                }
                else
                {
                    cuadroArchivistico = new CuadroArchivistico();
                }

                return View(cuadroArchivistico);
            }
        }

        [HttpPost]
        public ActionResult Guardar(CuadroArchivistico cuadroArchivistico, string idPadre)
        {
            var idCuadroArchivistico = ObtenerParametroGetEnInt(Enumerados.Parametro.IdCuadroArchivistico);
            
            using (var bd = new Contexto())
            {
                var cuadroarchivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);
                cuadroArchivistico.IDCuadroArchivistico = idCuadroArchivistico;
                cuadroArchivistico.IDTipoCuadroArchivistico = (int)TiposCuadroArchivistico.Fondo;
                AccionesAuditoria accion;
                if (cuadroArchivistico.IDCuadroArchivistico > 0)
                {
                    cuadroArchivistico.FechaModificado = DateTime.Now;
                    cuadroarchivisticoRepositorio.Modificar(cuadroArchivistico);
                    EscribirCorrectoMensaje(General.GuardadoCorrecto);
                    accion = AccionesAuditoria.Modificar;
                }
                else
                {
                    cuadroArchivistico.Habilitado = true;
                    cuadroArchivistico.FechaCreacion = DateTime.Now;
                    cuadroArchivistico.FechaModificado = cuadroArchivistico.FechaCreacion;
                    cuadroarchivisticoRepositorio.Guardar(cuadroArchivistico);
                    EscribirCorrectoMensaje(General.GuardadoCorrecto);

                    accion = AccionesAuditoria.Guardar;
                }
                GuardarMovimientoAuditoria(cuadroArchivistico.IDCuadroArchivistico, ModulosAuditoria.Roles, accion, cuadroArchivistico.Nombre);
                bd.SaveChanges();
            }

            var parametrosGet = new
            {
                idCuadroArchivistico = cuadroArchivistico.IDCuadroArchivistico.ToString().EncodeTo64()
            };

            return RedirectToAction("Guardar", parametrosGet);
        }

        public ActionResult Habilitar(string idCuadroarchivistico, string idPadre)
        {
            var idCuadroarchivisticoHabilitar = idCuadroarchivistico.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                var cuadroarchivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);
                cuadroarchivisticoRepositorio.CambiarHabilitado(idCuadroarchivisticoHabilitar, true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);

                GuardarMovimientoAuditoria(idCuadroarchivisticoHabilitar, ModulosAuditoria.Roles, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }

            return RedirectToAction("Index", new { idPadre });
        }

        public ActionResult Deshabilitar(string idCuadroarchivistico, string idPadre)
        {
            var idCuadroarchivisticoHabilitar = idCuadroarchivistico.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                var cuadroarchivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);
                cuadroarchivisticoRepositorio.CambiarHabilitado(idCuadroarchivisticoHabilitar, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);

                GuardarMovimientoAuditoria(idCuadroarchivisticoHabilitar, ModulosAuditoria.Roles, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }

            return RedirectToAction("Index", new { idPadre });
        }

        [WebMethod]
        [HttpPost]
        public JsonResult ListaCuadroArchvistico(int idCuadroArchvistico, TiposCuadroArchivistico tipo, bool filtrarAgregados = false, int idEntidad = 0)
        {
            var idEntidadUsuario = ObtenerSesion().IdEntidad;

            if (idEntidadUsuario > 0) idEntidad = idEntidadUsuario;

            var cuadroArchvisitico = filtrarAgregados
                ? CuadroArchivisticoRepositorio.Buscar(idCuadroArchvistico, tipo, idEntidad)
                : CuadroArchivisticoRepositorio.Buscar(idCuadroArchvistico, tipo);
            return Json(cuadroArchvisitico);
        }

        [WebMethod]
        [HttpPost]
        public JsonResult AgregarCuadroArchivistico(int idEntidad, int idCuadroArchivistico)
        {
            using (var bd = new Contexto())
            {
                var repositorio = new CuadroArchivisticoRepositorio(bd);

                repositorio.AgregarCuadroArchivisticoEntidad(idEntidad, idCuadroArchivistico);

                bd.SaveChanges();

                return Json(true);
            }
        }

        [WebMethod]
        [HttpPost]
        public JsonResult QuitarCuadroArchivistico(int idEntidad, int idCuadroArchivistico)
        {
            using (var bd = new Contexto())
            {
                var repositorio = new CuadroArchivisticoRepositorio(bd);

                repositorio.QuitarCuadroArchivisticoEntidad(idEntidad, idCuadroArchivistico);

                bd.SaveChanges();

                return Json(true);
            }
        }
    }
}