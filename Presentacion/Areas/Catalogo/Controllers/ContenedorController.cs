﻿using System;
using System.Net;
using System.Web.Mvc;
using System.Web.Services;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Enums;
using Datos.Repositorios.Catalogos;
using Presentacion.Controllers;
using Resources;
using Sistema.Extensiones;
using Sistema.Paginador;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class ContenedorController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index(ContenedorViewModel contenedorViewModel)
        {
            contenedorViewModel.Contenedor = new Contenedor
            {
                Habilitado = true,
                Mobilario = new Mobilario
                {
                    Area = new Area {Nivel = new Nivel {Edificio = new Edificio {Localidad = new Localidad()}}}
                }
            };

            var idMobilario = contenedorViewModel.IdMobilario.DecodeFrom64().TryToInt();
            contenedorViewModel.IdEntidad = ObtenerParametroGetEnInt(Enumerados.Parametro.IdEntidad);

            using (var bd = new Contexto())
            {
                if (idMobilario > 0)
                {
                    var mobilarioRepositorio = new MobilarioRepositorio(bd);
                    var mobilario = mobilarioRepositorio
                        .BuscarUnoSolo(l => l.IDMobilario == idMobilario, new[] { "Area.Nivel.Edificio.Localidad.Municipio" });
                    if (mobilario != null) contenedorViewModel.Contenedor.Mobilario = mobilario;
                }
                else
                {
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    contenedorViewModel.Contenedor.Mobilario.Area.Nivel.Edificio.Localidad.IDEstado =
                        estadoRepositorio.ObtenerIdEstadoPredeterminado();
                }

                if (contenedorViewModel.IdEntidad > 0)
                {
                    var repositorioEntidad = new EntidadRepositorio(bd);
                    contenedorViewModel.Entidad = repositorioEntidad.BuscarUnoSolo(e =>
                        e.IDEntidad == contenedorViewModel.IdEntidad);
                }

                var contenedorRepositorio = new ContenedorRepositorio(bd);
                contenedorViewModel.Contenedores = contenedorRepositorio.Buscar(contenedorViewModel);
                contenedorViewModel.TotalEncontrados = contenedorRepositorio.ObtenerTotalRegistros(contenedorViewModel);
                contenedorViewModel.Paginas = Paginar.ObtenerCantidadPaginas(contenedorViewModel.TotalEncontrados,
                    contenedorViewModel.TamanoPagina);
            }

            return View(contenedorViewModel);
        }

        [HttpPost]
        public ActionResult Index(ContenedorViewModel contenedorViewModel, string pagina)
        {
            contenedorViewModel.PaginaActual = pagina.TryToInt();

            var idMobilario = contenedorViewModel.IdMobilario.DecodeFrom64().TryToInt();
            contenedorViewModel.IdEntidad = ObtenerParametroGetEnInt(Enumerados.Parametro.IdEntidad);

            if (contenedorViewModel.PaginaActual <= 0) contenedorViewModel.PaginaActual = 1;

            using (var bd = new Contexto())
            {
                if (idMobilario > 0)
                {
                    var mobilarioRepositorio = new MobilarioRepositorio(bd);
                    var mobilario = mobilarioRepositorio.BuscarUnoSolo(l => l.IDMobilario == idMobilario,
                        new[] {"Area.Nivel.Edificio.Localidad.Municipio"});
                    if (mobilario != null) contenedorViewModel.Contenedor.Mobilario = mobilario;
                }
                else
                {
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    contenedorViewModel.Contenedor.Mobilario.Area.Nivel.Edificio.Localidad.IDEstado = estadoRepositorio
                        .ObtenerIdEstadoPredeterminado();
                }

                if (contenedorViewModel.IdEntidad > 0)
                {
                    var repositorioEntidad = new EntidadRepositorio(bd);
                    contenedorViewModel.Entidad = repositorioEntidad.BuscarUnoSolo(e =>
                        e.IDEntidad == contenedorViewModel.IdEntidad);
                }

                var contenedorRepositorio = new ContenedorRepositorio(bd);
                contenedorViewModel.Contenedores = contenedorRepositorio.Buscar(contenedorViewModel);
                contenedorViewModel.TotalEncontrados = contenedorRepositorio.ObtenerTotalRegistros(contenedorViewModel);
                contenedorViewModel.Paginas = Paginar.ObtenerCantidadPaginas(contenedorViewModel.TotalEncontrados,
                    contenedorViewModel.TamanoPagina);
            }

            LimpiarModelState();

            return View(contenedorViewModel);
        }

        [HttpGet]
        public ActionResult Guardar(string idContenedor, string idMobilarioRegresar, string idEntidadRegresar)
        {
            var contenedor = new Contenedor {IDContenedor = idContenedor.DecodeFrom64().TryToInt()};
            var idMobilario = idMobilarioRegresar.DecodeFrom64().TryToInt();
            var idEntidad = idEntidadRegresar.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                if (contenedor.IDContenedor > 0)
                {
                    var contenedorRepositorio = new ContenedorRepositorio(bd);
                    contenedor = contenedorRepositorio.BuscarUnoSolo(e => e.IDContenedor == contenedor.IDContenedor,
                        new[] { "Mobilario.Area.Nivel.Edificio.Localidad" });
                }
                else if (idMobilario > 0)
                {
                    var repositorioMobilario = new MobilarioRepositorio(bd);
                    contenedor.Mobilario = repositorioMobilario.BuscarUnoSolo(m => m.IDMobilario == idMobilario,
                        new[] { "Area.Nivel.Edificio.Localidad" });
                    contenedor.IDMobilario = idMobilario;
                }
                else
                {
                    contenedor.Mobilario = new Mobilario
                    {
                        Area = new Area {Nivel = new Nivel {Edificio = new Edificio {Localidad = new Localidad()}}}
                    };
                    var repositorioEstado = new EstadoRepositorio(bd);
                    contenedor.Mobilario.Area.Nivel.Edificio.Localidad.IDEstado =
                        repositorioEstado.ObtenerIdEstadoPredeterminado();
                }
            }

            ViewBag.IdMobilarioRegresar = idMobilario;
            ViewBag.IdEntidadRegresar = idEntidad;

            return View(contenedor);
        }

        [HttpPost]
        public ActionResult Guardar(Contenedor contenedor)
        {
            contenedor.IDContenedor = Request.QueryString["idContenedor"].DecodeFrom64().TryToInt();

            var idMobilario = Request.QueryString["idMobilarioRegresar"].DecodeFrom64().TryToInt();
            var idEntidad = Request.QueryString["idEntidadRegresar"].DecodeFrom64().TryToInt();

            contenedor.Mobilario = null;

            ModelState["idContenedor"]?.Errors.Clear();
            AccionesAuditoria accion;
            if (ModelState.IsValid)
            {
                contenedor.IDContenedor = ObtenerParametroGetEnInt(Enumerados.Parametro.IdContenedor);

                using (var bd = new Contexto())
                {
                    var contenedorRepositorio = new ContenedorRepositorio(bd);

                    if (contenedor.IDContenedor > 0)
                    {
                        contenedor.Habilitado = true;
                        contenedor.FechaModificacion = DateTime.Now;
                        contenedorRepositorio.Modificar(contenedor);
                        accion = AccionesAuditoria.Modificar;
                    }
                    else
                    {
                        contenedor.Habilitado = true;
                        contenedor.FechaCreacion = DateTime.Now;
                        contenedor.FechaModificacion = contenedor.FechaCreacion;
                        contenedorRepositorio.Guardar(contenedor);
                        accion = AccionesAuditoria.Guardar;
                    }
                    GuardarMovimientoAuditoria(contenedor.IDContenedor, ModulosAuditoria.Contenedores, accion, contenedor.Nombre);
                    bd.SaveChanges();

                    contenedor.Mobilario = new Mobilario
                    {
                        Area = new Area {Nivel = new Nivel {Edificio = new Edificio {Localidad = new Localidad()}}}
                    };
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    contenedor.Mobilario.Area.Nivel.Edificio.Localidad.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();
                }

                EscribirCorrectoMensaje(General.GuardadoCorrecto);
            }
            else
            {
                EscribirErrorMensaje(string.Format(General.FalloGuardado, "Contenedor"));
            }

            var parametrosGet = new
            {
                idContenedor = contenedor.IDContenedor.ToString().EncodeTo64(),
                idMobilarioRegresar = idMobilario.ToString().EncodeTo64(),
                idEntidadRegresar = idEntidad.ToString().EncodeTo64()
            };

            return RedirectToAction("Guardar", parametrosGet);
        }

        [WebMethod]
        [HttpPost]
        public JsonResult ListaContenedores(int idMobilario, string buscar, bool filtrarAgregados = false)
        {
            var lista = filtrarAgregados
                ? Json(ContenedorRepositorio.Buscar(idMobilario, buscar, ObtenerSesion().IdEntidad))
                : Json(ContenedorRepositorio.Buscar(idMobilario, buscar));
            return lista;
        }

        [HttpGet]
        public ActionResult Habilitar(string idContenedor, string idMobilarioRegresar)
        {
            var idContenedorCambiar = idContenedor.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var contenedorRepositorio = new ContenedorRepositorio(bd);
                contenedorRepositorio.CambiarHabilitado(idContenedorCambiar, true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);

                GuardarMovimientoAuditoria(idContenedorCambiar, ModulosAuditoria.Contenedores, AccionesAuditoria.Habilitar);

                bd.SaveChanges();
            }
            return RedirectToAction("Index", new {idMobilario = idMobilarioRegresar});
        }

        [HttpGet]
        public ActionResult Deshabilitar(string idContenedor, string idMobilarioRegresar)
        {
            var idContenedorCambiar = idContenedor.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var contenedorRepositorio = new ContenedorRepositorio(bd);
                contenedorRepositorio.CambiarHabilitado(idContenedorCambiar, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);

                GuardarMovimientoAuditoria(idContenedorCambiar, ModulosAuditoria.Contenedores, AccionesAuditoria.Habilitar);

                bd.SaveChanges();
            }
            return RedirectToAction("Index", new { idMobilario = idMobilarioRegresar });
        }

        [WebMethod]
        [HttpPost]
        public JsonResult AgregarContenedor(int idEntidad, int idContenedor)
        {
            using (var bd = new Contexto())
            {
                var repositorio = new ContenedorRepositorio(bd);

                repositorio.AgregarContenedorEntidad(idEntidad, idContenedor);

                bd.SaveChanges();

                return Json(true);
            }
        }

        [WebMethod]
        [HttpPost]
        public JsonResult QuitarContenedor(int idEntidad, int idContenedor)
        {
            using (var bd = new Contexto())
            {
                var repositorio = new ContenedorRepositorio(bd);

                repositorio.QuitarContenedorEntidad(idEntidad, idContenedor);

                bd.SaveChanges();

                return Json(true);
            }
        }
    }
}