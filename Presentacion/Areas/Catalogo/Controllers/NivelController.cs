﻿using System;
using System.Web.Mvc;
using System.Web.Services;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Enums;
using Datos.Repositorios.Catalogos;
using Presentacion.Controllers;
using Resources;
using Sistema.Extensiones;
using Sistema.Paginador;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class NivelController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index(NivelViewModel nivelViewModel)
        {
            nivelViewModel.Nivel = new Nivel {Habilitado = true, Edificio = new Edificio {Localidad = new Localidad()}};
            var idEdificio = nivelViewModel.IdEdificio.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                if (idEdificio > 0)
                {
                    var edificioRepositorio = new EdificioRepositorio(bd);
                    var edificio = edificioRepositorio
                        .BuscarUnoSolo(l => l.IDEdificio == idEdificio, new[] { "Localidad.Municipio" });
                    if (edificio != null) nivelViewModel.Nivel.Edificio = edificio;
                }
                else
                {
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    nivelViewModel.Nivel.Edificio.Localidad.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();
                }

                var nivelRepositorio = new NivelRepositorio(bd);
                nivelViewModel.Niveles = nivelRepositorio.Buscar(nivelViewModel);
                nivelViewModel.TotalEncontrados = nivelRepositorio.ObtenerTotalRegistros(nivelViewModel);
                nivelViewModel.Paginas = Paginar.ObtenerCantidadPaginas(nivelViewModel.TotalEncontrados,
                    nivelViewModel.TamanoPagina);
            }

            return View(nivelViewModel);
        }

        [HttpPost]
        public ActionResult Index(NivelViewModel nivelViewModel, string pagina)
        {
            nivelViewModel.PaginaActual = pagina.TryToInt();
            var idEdificio = nivelViewModel.IdEdificio.DecodeFrom64().TryToInt();

            if (nivelViewModel.PaginaActual <= 0) nivelViewModel.PaginaActual = 1;

            using (var bd = new Contexto())
            {
                if (idEdificio > 0)
                {
                    var edificioRepositorio = new EdificioRepositorio(bd);
                    var edificio = edificioRepositorio
                        .BuscarUnoSolo(l => l.IDEdificio == idEdificio, new[] { "Localidad.Municipio" });
                    if (edificio != null) nivelViewModel.Nivel.Edificio = edificio;
                }
                else
                {
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    nivelViewModel.Nivel.Edificio.Localidad.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();
                }

                var nivelRepositorio = new NivelRepositorio(bd);
                nivelViewModel.Niveles = nivelRepositorio.Buscar(nivelViewModel);
                nivelViewModel.TotalEncontrados = nivelRepositorio.ObtenerTotalRegistros(nivelViewModel);
                nivelViewModel.Paginas = Paginar.ObtenerCantidadPaginas(nivelViewModel.TotalEncontrados,
                    nivelViewModel.TamanoPagina);
            }

            LimpiarModelState();

            return View(nivelViewModel);
        }

        [HttpGet]
        public ActionResult Guardar(string idNivel, string idEdificioRegresar)
        {
            var nivel = new Nivel {IDNivel = idNivel.DecodeFrom64().TryToInt()};
            var idEdificio = idEdificioRegresar.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                if (nivel.IDNivel > 0)
                {
                    var nivelRepositorio = new NivelRepositorio(bd);
                    nivel = nivelRepositorio.BuscarUnoSolo(e => e.IDNivel == nivel.IDNivel,
                        new[] {"Edificio.Localidad"});
                }
                else if (idEdificio > 0)
                {
                    var repositorioEdificio = new EdificioRepositorio(bd);
                    nivel.Edificio = repositorioEdificio.BuscarUnoSolo(l => l.IDEdificio == idEdificio,
                        new[] {"Localidad"});
                    nivel.IDEdificio = idEdificio;
                }
                else
                {
                    nivel.Edificio = new Edificio {Localidad = new Localidad()};
                    var repositorioEstado = new EstadoRepositorio(bd);
                    nivel.Edificio.Localidad.IDEstado = repositorioEstado.ObtenerIdEstadoPredeterminado();
                }
            }

            ViewBag.IdEdificioRegresar = idEdificio;

            return View(nivel);
        }

        [HttpPost]
        public ActionResult Guardar(Nivel nivel)
        {
            nivel.IDNivel = Request.QueryString["idNivel"].DecodeFrom64().TryToInt();
            var idEdificio = Request.QueryString["idEdificioRegresar"].DecodeFrom64().TryToInt();
            nivel.Edificio = null;

            ModelState["idNivel"]?.Errors.Clear();
            AccionesAuditoria accion;
            if (ModelState.IsValid)
            {
                nivel.IDNivel = ObtenerParametroGetEnInt(Enumerados.Parametro.IdNivel);

                using (var bd = new Contexto())
                {
                    var nivelRepositorio = new NivelRepositorio(bd);

                    if (nivel.IDNivel > 0)
                    {
                        nivel.Habilitado = true;
                        nivel.FechaModificacion = DateTime.Now;
                        nivelRepositorio.Modificar(nivel);
                        accion = AccionesAuditoria.Modificar;
                    }
                    else
                    {
                        nivel.Habilitado = true;
                        nivel.FechaCreacion = DateTime.Now;
                        nivel.FechaModificacion = nivel.FechaCreacion;
                        nivelRepositorio.Guardar(nivel);
                        accion = AccionesAuditoria.Guardar;
                    }
                    GuardarMovimientoAuditoria(nivel.IDNivel, ModulosAuditoria.Niveles, accion, nivel.Nombre);
                    bd.SaveChanges();

                    nivel.Edificio = new Edificio {Localidad = new Localidad()};
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    nivel.Edificio.Localidad.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();
                }

                EscribirCorrectoMensaje(General.GuardadoCorrecto);
            }
            else
            {
                EscribirErrorMensaje(string.Format(General.FalloGuardado, "Nivel"));
            }

            dynamic parametrosGet;

            if (idEdificio > 0)
                parametrosGet = new
                {
                    idNivel = nivel.IDNivel.ToString().EncodeTo64(),
                    idEdificioRegresar = idEdificio.ToString().EncodeTo64()
                };
            else
                parametrosGet = new { idNivel = nivel.IDNivel.ToString().EncodeTo64() };

            return RedirectToAction("Guardar", parametrosGet);
        }

        [WebMethod]
        [HttpPost]
        public JsonResult ListaNiveles(int idEdificio, string buscar, bool filtrarAgregados = false)
        {
            var lista = filtrarAgregados
                ? Json(NivelRepositorio.Buscar(idEdificio, buscar, ObtenerSesion().IdEntidad))
                : Json(NivelRepositorio.Buscar(idEdificio, buscar));
            return lista;
        }

        [HttpGet]
        public ActionResult Habilitar(string idNivel, string idEdificioRegresar)
        {
            var idNivelCambiar = idNivel.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var nivelRepositorio = new NivelRepositorio(bd);
                nivelRepositorio.CambiarHabilitado(idNivelCambiar, true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);
                GuardarMovimientoAuditoria(idNivelCambiar, ModulosAuditoria.Niveles, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }
            return RedirectToAction("Index", new {idEdificio = idEdificioRegresar});
        }

        [HttpGet]
        public ActionResult Deshabilitar(string idNivel, string idEdificioRegresar)
        {
            var idNivelCambiar = idNivel.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var nivelRepositorio = new NivelRepositorio(bd);
                nivelRepositorio.CambiarHabilitado(idNivelCambiar, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);
                GuardarMovimientoAuditoria(idNivelCambiar, ModulosAuditoria.Niveles, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }
            return RedirectToAction("Index", new { idEdificio = idEdificioRegresar });
        }
    }
}