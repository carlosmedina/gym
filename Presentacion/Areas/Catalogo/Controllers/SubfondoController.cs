﻿using System;
using System.Web.Mvc;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Enums;
using Datos.Recursos;
using Datos.Repositorios.Configuracion;
using Presentacion.Controllers;
using Sistema.Extensiones;
using Sistema.Paginador;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class SubfondoController : ControladorBase
    {
        public ActionResult Index(CuadroArchivisticoViewModel cuadroArchivisticoViewModel)
        {
            cuadroArchivisticoViewModel.CuadroArchivistico = new CuadroArchivistico { Habilitado = true };
            var idPadre = cuadroArchivisticoViewModel.IdPadre.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                var archivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);

                if (idPadre > 0)
                {
                    var antecesores = archivisticoRepositorio.BuscarUnoSolo(a => a.IDCuadroArchivistico == idPadre);
                    cuadroArchivisticoViewModel.CuadroArchivistico.PadreCuadroArchivistico = antecesores;
                }

                cuadroArchivisticoViewModel.CuadroArchivistico.IDPadreCuadroArchivistico = idPadre;
                cuadroArchivisticoViewModel.CuadroArchivistico.IDTipoCuadroArchivistico = (int)TiposCuadroArchivistico.Subfondo;
                cuadroArchivisticoViewModel.CuadroArchivisticos = archivisticoRepositorio.Buscar(cuadroArchivisticoViewModel);
                cuadroArchivisticoViewModel.TotalEncontrados = archivisticoRepositorio.ObtenerTotalRegistros(cuadroArchivisticoViewModel);
                cuadroArchivisticoViewModel.Paginas = Paginar.ObtenerCantidadPaginas(cuadroArchivisticoViewModel.TotalEncontrados, cuadroArchivisticoViewModel.TamanoPagina);
            }

            return View(cuadroArchivisticoViewModel);
        }

        [HttpPost]
        public ActionResult Index(CuadroArchivisticoViewModel cuadroArchivisticoViewModel, string pagina)
        {
            if (string.IsNullOrEmpty(pagina)) pagina = "1";
            cuadroArchivisticoViewModel.PaginaActual = pagina.TryToInt();

            var idPadre = cuadroArchivisticoViewModel.IdPadre.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                var archivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);

                if (idPadre > 0)
                {
                    var antecesores = archivisticoRepositorio.BuscarUnoSolo(a => a.IDCuadroArchivistico == idPadre);
                    cuadroArchivisticoViewModel.CuadroArchivistico.PadreCuadroArchivistico = antecesores;
                }

                cuadroArchivisticoViewModel.CuadroArchivistico.IDPadreCuadroArchivistico = idPadre;
                cuadroArchivisticoViewModel.CuadroArchivistico.IDTipoCuadroArchivistico = (int)TiposCuadroArchivistico.Subfondo;
                cuadroArchivisticoViewModel.CuadroArchivisticos = archivisticoRepositorio.Buscar(cuadroArchivisticoViewModel);
                cuadroArchivisticoViewModel.TotalEncontrados = archivisticoRepositorio.ObtenerTotalRegistros(cuadroArchivisticoViewModel);
                cuadroArchivisticoViewModel.Paginas = Paginar.ObtenerCantidadPaginas(cuadroArchivisticoViewModel.TotalEncontrados,
                    cuadroArchivisticoViewModel.TamanoPagina);
            }

            LimpiarModelState();

            return View(cuadroArchivisticoViewModel);
        }

        public ActionResult Guardar(string idCuadroArchivistico, string idPadre)
        {
            var idCuadroArchivisticoBuscar = idCuadroArchivistico.DecodeFrom64().TryToInt();

            var viewModel = new GuardarCuadroArchivisticoViewModel { IdPadre = idPadre.DecodeFrom64().TryToInt() };

            using (var bd = new Contexto())
            {
                var archivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);

                CuadroArchivistico cuadroArchivistico;
                if (idCuadroArchivisticoBuscar > 0)
                {
                    cuadroArchivistico = archivisticoRepositorio
                        .BuscarPorIdCuadroArchivistico(idCuadroArchivisticoBuscar);
                    viewModel.EstablecerIdAntecesores(cuadroArchivistico);
                }
                else
                {
                    cuadroArchivistico = new CuadroArchivistico {IDPadreCuadroArchivistico = viewModel.IdPadre};
                }

                viewModel.CuadroArchivistico = cuadroArchivistico;

                return View(viewModel);
            }
        }

        [HttpPost]
        public ActionResult Guardar(GuardarCuadroArchivisticoViewModel viewModel, string idPadre)
        {
            var idCuadroArchivistico = ObtenerParametroGetEnInt(Enumerados.Parametro.IdCuadroArchivistico);
            viewModel.IdPadre = Request.QueryString["idPadre"].DecodeFrom64().TryToInt();

            var cuadroArchivistico = viewModel.CuadroArchivistico;

            using (var bd = new Contexto())
            {
                var cuadroarchivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);
                cuadroArchivistico.IDCuadroArchivistico = idCuadroArchivistico;
                cuadroArchivistico.IDTipoCuadroArchivistico = (int)TiposCuadroArchivistico.Subfondo;
                AccionesAuditoria accion;
                if (cuadroArchivistico.IDCuadroArchivistico > 0)
                {
                    cuadroArchivistico.IDPadreCuadroArchivistico = viewModel.IdFondo;
                    cuadroArchivistico.FechaModificado = DateTime.Now;
                    cuadroarchivisticoRepositorio.Modificar(cuadroArchivistico);
                    EscribirCorrectoMensaje(General.GuardadoCorrecto);
                    accion = AccionesAuditoria.Modificar;
                }
                else
                {
                    cuadroArchivistico.IDPadreCuadroArchivistico = viewModel.IdFondo;
                    cuadroArchivistico.Habilitado = true;
                    cuadroArchivistico.FechaCreacion = DateTime.Now;
                    cuadroArchivistico.FechaModificado = cuadroArchivistico.FechaCreacion;
                    cuadroarchivisticoRepositorio.Guardar(cuadroArchivistico);
                    EscribirCorrectoMensaje(General.GuardadoCorrecto);

                    accion = AccionesAuditoria.Guardar;
                }
                GuardarMovimientoAuditoria(cuadroArchivistico.IDCuadroArchivistico, ModulosAuditoria.SubFondos, accion, cuadroArchivistico.Nombre);
                bd.SaveChanges();
            }

            dynamic parametrosGet;

            if (viewModel.IdPadre > 0)
                parametrosGet = new
                {
                    idCuadroArchivistico = cuadroArchivistico.IDCuadroArchivistico.ToString().EncodeTo64(),
                    idPadre = viewModel.IdPadre.ToString().EncodeTo64()
                };
            else
                parametrosGet = new { idCuadroArchivistico = cuadroArchivistico.IDCuadroArchivistico.ToString().EncodeTo64() };

            return RedirectToAction("Guardar", parametrosGet);
        }

        public ActionResult Habilitar(string idCuadroarchivistico, string idPadre)
        {
            var idCuadroarchivisticoHabilitar = idCuadroarchivistico.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                var cuadroarchivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);
                cuadroarchivisticoRepositorio.CambiarHabilitado(idCuadroarchivisticoHabilitar, true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);
                GuardarMovimientoAuditoria(idCuadroarchivisticoHabilitar, ModulosAuditoria.SubFondos, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }

            return RedirectToAction("Index", new { idPadre });
        }

        public ActionResult Deshabilitar(string idCuadroarchivistico, string idPadre)
        {
            var idCuadroarchivisticoHabilitar = idCuadroarchivistico.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                var cuadroarchivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);
                cuadroarchivisticoRepositorio.CambiarHabilitado(idCuadroarchivisticoHabilitar, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);
                GuardarMovimientoAuditoria(idCuadroarchivisticoHabilitar, ModulosAuditoria.SubFondos, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }

            return RedirectToAction("Index", new { idPadre });
        }
    }
}