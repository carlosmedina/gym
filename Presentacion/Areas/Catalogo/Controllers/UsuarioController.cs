﻿using System;
using System.Web;
using System.Web.Mvc;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Enums;
using Datos.Recursos;
using Sistema.Paginador;
using Sistema.Extensiones;
using Presentacion.Controllers;
using Presentacion.Fabrica;
using Sistema;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using Datos.Repositorios.Catalogos;
using Datos.Repositorios.Configuracion;
using Datos.Repositorios.Digitalizacion;
using Datos.Repositorios.Reportes;
using Negocio.Servicios;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class UsuarioController : ControladorBase
    {
        public ActionResult Index(UsuarioViewModel usuarioViewModel)
        {
            var idEntidad = usuarioViewModel.IdEntidad.DecodeFrom64().TryToInt();

            usuarioViewModel.Usuario = new Usuario {Habilitado = true, IDEntidad = idEntidad};

            using (var bd = new Contexto())
            {
                if (idEntidad > 0)
                {
                    var repositorioEntidad = new EntidadRepositorio(bd);
                    var entidad = repositorioEntidad.BuscarUnoSolo(e => e.IDEntidad == idEntidad);
                    usuarioViewModel.Usuario.Entidad = entidad;
                }

                var usuarioRepositorio = new UsuarioRepositorio(bd);
                usuarioViewModel.Usuarios = usuarioRepositorio.Buscar(usuarioViewModel);
                usuarioViewModel.TotalEncontrados = usuarioRepositorio.ObtenerTotalRegistros(usuarioViewModel);
                usuarioViewModel.Paginas = Paginar.ObtenerCantidadPaginas(usuarioViewModel.TotalEncontrados,
                                                usuarioViewModel.TamanoPagina);
            }

            return View(usuarioViewModel);
        }

        [HttpPost]
        public ActionResult Index(UsuarioViewModel usuarioViewModel, string pagina)
        {
            if (string.IsNullOrEmpty(pagina)) pagina = "1";        
            usuarioViewModel.PaginaActual = pagina.TryToInt();

            var idEntidad = usuarioViewModel.IdEntidad.DecodeFrom64().TryToInt();

            usuarioViewModel.Usuario.IDEntidad = idEntidad;

            using (var bd = new Contexto())
            {
                if (idEntidad > 0)
                {
                    var repositorioEntidad = new EntidadRepositorio(bd);
                    var entidad = repositorioEntidad.BuscarUnoSolo(e => e.IDEntidad == idEntidad);
                    usuarioViewModel.Usuario.Entidad = entidad;
                }

                var usuarioRepositorio = new UsuarioRepositorio(bd);
                usuarioViewModel.Usuarios = usuarioRepositorio.Buscar(usuarioViewModel);
                usuarioViewModel.TotalEncontrados = usuarioRepositorio.ObtenerTotalRegistros(usuarioViewModel);
                usuarioViewModel.Paginas = Paginar.ObtenerCantidadPaginas(usuarioViewModel.TotalEncontrados,
                                                usuarioViewModel.TamanoPagina);
            }

            LimpiarModelState();

            return View(usuarioViewModel);
        }

        [HttpGet]
        public ActionResult Guardar(string idUsuario, string idEntidad)
        {
            Usuario usuario;

            var idUsuarioBuscar = idUsuario.DecodeFrom64().TryToInt();
            var idEntidadBuscar = idEntidad.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                if (idUsuarioBuscar > 0)
                {
                    var usuarioRepositorio = new UsuarioRepositorio(bd);
                    usuario = usuarioRepositorio.BuscarPorIdEmpleado(idUsuarioBuscar);
                }
                else {
                    usuario = new Usuario();
                }

                if (idEntidadBuscar > 0) ViewBag.IdEntidadUsuarios = idEntidadBuscar;
            }
            return View(usuario);
        }

        [HttpPost]
        public ActionResult Guardar(Usuario usuario, IEnumerable<HttpPostedFileBase> archivos)
        {
            var idUsuario = ObtenerParametroGetEnInt(Enumerados.Parametro.IdUsuario);
            var idEntidad = ObtenerParametroGetEnInt(Enumerados.Parametro.IdEntidad);

            HttpPostedFileBase imagen = null;

            var i = 0;
            var httpPostedFileBases = archivos as HttpPostedFileBase[] ?? archivos.ToArray();
            foreach (var archivo in httpPostedFileBases)
            {
                if (archivo != null)
                {
                    if (i == (httpPostedFileBases.Count() == 1 ? 0 : -1)) imagen = archivo;
                }
                i++;
            }

            var imageName = Guid.NewGuid().ToString();

            using (var bd = new Contexto())
            {
                
                var usuarioRepositorio = new UsuarioRepositorio(bd);

                usuario.IDUsuario = usuarioRepositorio.ObtenerIdUsuarioPorIdEmpleado(idUsuario);
                usuario.UrlFoto = Archivo.ObtenerRutaFotoBaseDatos(imagen, Archivo.Ruta.FotosUsuario, imageName);
                AccionesAuditoria accion;
                if (usuario.IDUsuario > 0)
                {
                    usuarioRepositorio.ActualizarUsuario(usuario);
                    accion = AccionesAuditoria.Modificar;
                }
                else
                {
                    usuarioRepositorio.GuardarUsuario(usuario);
                    accion = AccionesAuditoria.Guardar;
                }
                GuardarMovimientoAuditoria(usuario.IDUsuario, ModulosAuditoria.Usuarios, accion, usuario.Nombre);
                bd.SaveChanges();

                Archivo.GuardarImagen(imagen, Archivo.Ruta.FotosUsuario, imageName);

                EscribirCorrectoMensaje(General.GuardadoCorrecto);
            }

            if (idEntidad > 0)
                return RedirectToAction("Guardar", new {idEntidad = idEntidad.ToString().EncodeTo64()});

            return View(usuario);
        }

        [HttpPost]
        public JsonResult Obtener(string idUsuario)
        {
            Usuario usuario;
            using (var bd = new Contexto())
            {
                var usuarioRepositorio = new UsuarioRepositorio(bd);
                usuario = usuarioRepositorio.BuscarPorId(idUsuario.DecodeFrom64().TryToInt());
            }
            return Json(Factory.Obtener(usuario));
        }

        [HttpGet]
        public ActionResult Habilitar(string idUsuario)
        {
            var idUsuarioHabilitar = idUsuario.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
               
                var usuarioRepositorio = new UsuarioRepositorio(bd);
                usuarioRepositorio.CambiarHabilitado(idUsuarioHabilitar, true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);
                GuardarMovimientoAuditoria(idUsuarioHabilitar, ModulosAuditoria.Usuarios, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Deshabilitar(string idUsuario)
        {
            var idUsuarioHabilitar = idUsuario.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                
                var usuarioRepositorio = new UsuarioRepositorio(bd);
                usuarioRepositorio.CambiarHabilitado(idUsuarioHabilitar, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);
                GuardarMovimientoAuditoria(idUsuarioHabilitar, ModulosAuditoria.Usuarios, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ModificarPerfil(string idUsuario)
        {
            Usuario usuario;
            var idUsuarioBuscar = idUsuario.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var usuarioRepositorio = new UsuarioRepositorio(bd);
                usuario = usuarioRepositorio.BuscarPorIdEmpleado(idUsuarioBuscar);
            }
            return View(usuario);
        }

        [HttpPost]
        public ActionResult ModificarPerfil(Usuario usuario, IEnumerable<HttpPostedFileBase> archivos)
        {
            var idUsuario = ObtenerParametroGetEnInt(Enumerados.Parametro.IdUsuario);

            HttpPostedFileBase imagen = null;

            var i = 0;
            var httpPostedFileBases = archivos as HttpPostedFileBase[] ?? archivos.ToArray();
            foreach (var archivo in httpPostedFileBases)
            {
                if (archivo != null)
                {
                    if (i == (httpPostedFileBases.Count() == 1 ? 0 : -1)) imagen = archivo;
                }
                i++;
            }

            var imageName = Guid.NewGuid().ToString();

            using (var bd = new Contexto())
            {
             
                var usuarioRepositorio = new UsuarioRepositorio(bd);
                usuario.IDUsuario = usuarioRepositorio.ObtenerIdUsuarioPorIdEmpleado(idUsuario);
                usuario.UrlFoto = Archivo.ObtenerRutaFotoBaseDatos(imagen, Archivo.Ruta.FotosUsuario, imageName);

                if (usuario.IDUsuario > 0)
                {
                    usuarioRepositorio.ActualizarUsuario(usuario);
                }
                else
                {
                    usuarioRepositorio.GuardarUsuario(usuario);
                    bd.SaveChanges();
                }

                Archivo.GuardarImagen(imagen, Archivo.Ruta.FotosUsuario, imageName);
                EscribirCorrectoMensaje(General.GuardadoCorrecto);
            }
            return View(usuario);
        }

        [HttpGet]
        public ActionResult CambiarContrasena(string idUsuario)
        {
            Usuario usuario;
            var idUsuarioBuscar = idUsuario.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var usuarioRepositorio = new UsuarioRepositorio(bd);
                usuario = usuarioRepositorio.BuscarPorIdEmpleado(idUsuarioBuscar);
            }
            return View(usuario);
        }

        [HttpPost]
        public ActionResult CambiarContrasena(Usuario usuario)
        {   
            var idUsuario = ObtenerParametroGetEnInt(Enumerados.Parametro.IdUsuario);
            using (var bd = new Contexto())
            {
                var usuarioRepositorio = new UsuarioRepositorio(bd);
                var contrasenanterior = usuarioRepositorio.ObtenerContrasenaPorId(idUsuario);
                usuario.IDUsuario = usuarioRepositorio.ObtenerIdUsuarioPorIdEmpleado(idUsuario);   
                if (usuario.ContrasenaAnterior.GetHashSha512() == contrasenanterior)
                {
                    usuarioRepositorio.ActualizarContrasena(idUsuario,usuario.Contrasena);
                    EscribirCorrectoMensaje(General.GuardadoCorrecto);
                }
                if (usuario.ContrasenaAnterior.GetHashSha512()!=contrasenanterior)
                {
                    EscribirErrorMensaje(General.ErrorContrasenaAnterior);
                }  
            }
            return View(usuario);
        }

        [WebMethod]
        [HttpPost]
        public JsonResult ListaUsuarios(int idEntidad)
        {
            var lista = Json(UsuarioRepositorio.Buscar(idEntidad));
            return lista;
        }

        [WebMethod]
        [HttpPost]
        public JsonResult EstadisticasUsuario(string idUsuario)
        {
            var idBuscarUsuario = idUsuario.DecodeFrom64().TryToInt();

            int total;
            int mes;
            int promedio;
            string nombre;
            string[] etiquetas;
            int[] valores;

            using (var contexto = new Contexto())
            {
                var repositorioExpedientes = new ExpedienteRepositorio(contexto);
                total = repositorioExpedientes.TotalExpedientesDigitalizados(idBuscarUsuario);
                mes = repositorioExpedientes.MesExpedientesDigitalizados(idBuscarUsuario);

                var repositorioUsuario = new UsuarioRepositorio(contexto);
                nombre = repositorioUsuario.ObtenerPropiedadUno(u => u.IDUsuario == idBuscarUsuario,
                    u => u.Nombre + " " + u.ApellidoP + " " + u.ApellidoM);

                var repositorioReporte = new ReportesRepositorio(contexto);
                promedio = repositorioReporte.ObtenerUsuarioPromedioDigitalizacion(idBuscarUsuario);

                //Grafica
                var actualAnio = DateTime.Now.Year;
                var actualmes = DateTime.Now.Month;
                var fechaMesFin = new DateTime(actualAnio, actualmes, DateTime.DaysInMonth(actualAnio, actualmes), 23, 59, 59);
                var fechaMesInicio = new DateTime(actualAnio, actualmes, 1).AddMonths(-6);

                var reporteRepositorio = new ReportesRepositorio(contexto);
                var consultasMeses = reporteRepositorio.ObtenerUsuarioMesesConsultasExpediente(idBuscarUsuario, fechaMesInicio, fechaMesFin);
                consultasMeses = ServiciosEstadisticas.RellenarMeses(consultasMeses, fechaMesInicio, fechaMesFin);

                etiquetas = consultasMeses.Select(s => FechaUtilidades.ObtenerNombreMes(s.Mes.Value)).ToArray();
                valores = consultasMeses.Select(s => s.Cantidad ?? 0).ToArray();
            }

            return Json(new
            {
                Total = total,
                Mes = mes,
                Promedio = promedio,
                Etiquetas = etiquetas,
                Valores = valores,
                Nombre = nombre,
                NombreMes = FechaUtilidades.ObtenerNombreMes(DateTime.Now.Month)
            });
        }
    }
}