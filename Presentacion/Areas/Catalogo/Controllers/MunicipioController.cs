﻿using System.Web.Mvc;
using System.Web.Services;
using Presentacion.Controllers;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Enums;
using Datos.Repositorios.Catalogos;
using Sistema.Paginador;
using Resources;
using Sistema.Extensiones;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class MunicipioController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index()
        {
            var municipioViewModel = new MunicipioViewModel {Municipio = new Municipio {Estado = new Estado()}};

            using (var bd = new Contexto())
            {
                var estadoRepositorio = new EstadoRepositorio(bd);
                municipioViewModel.Municipio.Estado.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();

                var municipioRepositorio = new MunicipioRepositorio(bd);
                municipioViewModel.Municipios = municipioRepositorio.Buscar(municipioViewModel);
                municipioViewModel.TotalEncontrados = municipioRepositorio.ObtenerTotalRegistros(municipioViewModel);
                municipioViewModel.Paginas = Paginar.ObtenerCantidadPaginas(municipioViewModel.TotalEncontrados,
                    municipioViewModel.TamanoPagina);
            }

            return View(municipioViewModel);
        }

        [HttpPost]
        public ActionResult Index(MunicipioViewModel municipioViewModel, string pagina)
        {
            municipioViewModel.Municipio.Estado = new Estado();
            municipioViewModel.PaginaActual = pagina.TryToInt();
            if (municipioViewModel.PaginaActual <= 0) municipioViewModel.PaginaActual = 1;

            using (var bd = new Contexto())
            {
                var estadoRepositorio = new EstadoRepositorio(bd);
                municipioViewModel.Municipio.Estado.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();

                var municipioRepositorio = new MunicipioRepositorio(bd);
                municipioViewModel.Municipios = municipioRepositorio.Buscar(municipioViewModel);
                municipioViewModel.TotalEncontrados = municipioRepositorio.ObtenerTotalRegistros(municipioViewModel);
                municipioViewModel.Paginas = Paginar.ObtenerCantidadPaginas(municipioViewModel.TotalEncontrados,
                    municipioViewModel.TamanoPagina);
            }
            LimpiarModelState();
            return View(municipioViewModel);
        }

        [HttpGet]
        public ActionResult Guardar(string idMunicipio, string idEstado)
        {
            var municipio = new Municipio { IDMunicipio = idMunicipio.DecodeFrom64().TryToInt() };
            var estado = new Estado { IDEstado = idEstado.DecodeFrom64().TryToInt() };
            if (municipio.IDMunicipio > 0)
            {
                using (var bd = new Contexto())
                {
                    var municipioRepositorio = new MunicipioRepositorio(bd);
                    municipio = municipioRepositorio.BuscarPorId(municipio.IDMunicipio, estado.IDEstado);
                }
            }
            return View(municipio);
        }

        [HttpPost]
        public ActionResult Guardar(Municipio municipio)
        {
            municipio.IDMunicipio = Request.QueryString["idMunicipio"].DecodeFrom64().TryToInt();
            municipio.IDEstado = Request.QueryString["idEstado"].DecodeFrom64().TryToInt();

            ModelState["idMunicipio"].Errors.Clear();
            ModelState["idEstado"].Errors.Clear();
            AccionesAuditoria accion;
            if (ModelState.IsValid)
            {
                municipio.IDMunicipio = ObtenerParametroGetEnInt(Enumerados.Parametro.IdMunicipio);
                municipio.IDEstado = ObtenerParametroGetEnInt(Enumerados.Parametro.IdEstado);
                using (var bd = new Contexto())
                {
                    var municipioRepositorio = new MunicipioRepositorio(bd);
                    if (municipio.IDMunicipio > 0 && municipio.IDEstado > 0)
                    {
                        municipioRepositorio.Modificar(municipio);
                        accion = AccionesAuditoria.Modificar;
                    }
                    else
                    {
                        municipioRepositorio.Guardar(municipio);
                        accion = AccionesAuditoria.Guardar;
                    }

                    GuardarMovimientoAuditoria(municipio.IDMunicipio, ModulosAuditoria.Municipios, accion, municipio.Nombre);
                    bd.SaveChanges();

                    municipio.Estado = new EstadoRepositorio(bd).BuscarPorId(municipio.IDEstado);
                }
            }
            EscribirCorrectoMensaje(General.GuardadoCorrecto);
            return View(municipio);
        }

        [WebMethod]
        [HttpPost]
        public JsonResult ObtenerMunicipios(int idEstado)
        {
            using (var bd = new Contexto())
            {
                var municipioRepositorio = new MunicipioRepositorio(bd);
                var lista = Json(municipioRepositorio.Buscar(idEstado));
                return lista;
            }
        }

        [WebMethod]
        [HttpPost]
        public JsonResult ListaMunicipios(bool filtrarAgregados = false, int idEntidad = 0)
        {
            var idEntidadUsuario = ObtenerSesion().IdEntidad;

            if (idEntidadUsuario > 0) idEntidad = idEntidadUsuario;

            var lista = filtrarAgregados
                ? Json(MunicipioRepositorio.ObtenerPredeterminados(idEntidad, false))
                : Json(MunicipioRepositorio.ObtenerPredeterminados(false));

            return lista;
        }
    }
}
