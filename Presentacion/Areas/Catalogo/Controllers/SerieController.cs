﻿using System;
using System.Web.Mvc;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Enums;
using Datos.Recursos;
using Datos.Repositorios.Catalogos;
using Datos.Repositorios.Configuracion;
using Presentacion.Controllers;
using Sistema.Extensiones;
using Sistema.Paginador;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class SerieController : ControladorBase
    {
        public ActionResult Index(CuadroArchivisticoViewModel cuadroArchivisticoViewModel)
        {
            cuadroArchivisticoViewModel.CuadroArchivistico = new CuadroArchivistico {Habilitado = true};
            var idPadre = cuadroArchivisticoViewModel.IdPadre.DecodeFrom64().TryToInt();

            cuadroArchivisticoViewModel.IdEntidad = ObtenerParametroGetEnInt(Enumerados.Parametro.IdEntidad);

            using (var bd = new Contexto())
            {
                var archivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);

                if (idPadre > 0)
                {
                    var antecesores = archivisticoRepositorio.BuscarUnoSolo(a => a.IDCuadroArchivistico == idPadre,
                        new[]
                        {
                            "PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico"
                        });
                    cuadroArchivisticoViewModel.CuadroArchivistico.PadreCuadroArchivistico = antecesores;
                }

                if (cuadroArchivisticoViewModel.IdEntidad > 0)
                {
                    var repositorioEntidad = new EntidadRepositorio(bd);
                    cuadroArchivisticoViewModel.Entidad = repositorioEntidad.BuscarUnoSolo(e =>
                        e.IDEntidad == cuadroArchivisticoViewModel.IdEntidad);
                    cuadroArchivisticoViewModel.CuadroArchivistico.Habilitado = true;
                }

                cuadroArchivisticoViewModel.CuadroArchivistico.IDPadreCuadroArchivistico = idPadre;
                cuadroArchivisticoViewModel.CuadroArchivistico.IDTipoCuadroArchivistico = (int)TiposCuadroArchivistico.Serie;
                cuadroArchivisticoViewModel.CuadroArchivisticos = archivisticoRepositorio.Buscar(cuadroArchivisticoViewModel);
                cuadroArchivisticoViewModel.TotalEncontrados = archivisticoRepositorio.ObtenerTotalRegistros(cuadroArchivisticoViewModel);
                cuadroArchivisticoViewModel.Paginas = Paginar.ObtenerCantidadPaginas(cuadroArchivisticoViewModel.TotalEncontrados, cuadroArchivisticoViewModel.TamanoPagina);
            }

            return View(cuadroArchivisticoViewModel);
        }

        [HttpPost]
        public ActionResult Index(CuadroArchivisticoViewModel cuadroArchivisticoViewModel, string pagina)
        {
            if (string.IsNullOrEmpty(pagina)) pagina = "1";
            cuadroArchivisticoViewModel.PaginaActual = pagina.TryToInt();

            var idPadre = cuadroArchivisticoViewModel.IdPadre.DecodeFrom64().TryToInt();
            cuadroArchivisticoViewModel.IdEntidad = ObtenerParametroGetEnInt(Enumerados.Parametro.IdEntidad);

            using (var bd = new Contexto())
            {
                var archivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);

                if (idPadre > 0)
                {
                    var antecesores = archivisticoRepositorio.BuscarUnoSolo(a => a.IDCuadroArchivistico == idPadre,
                        new[]
                        {
                            "PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico"
                        });
                    cuadroArchivisticoViewModel.CuadroArchivistico.PadreCuadroArchivistico = antecesores;
                }

                if (cuadroArchivisticoViewModel.IdEntidad > 0)
                {
                    var repositorioEntidad = new EntidadRepositorio(bd);
                    cuadroArchivisticoViewModel.Entidad = repositorioEntidad.BuscarUnoSolo(e =>
                        e.IDEntidad == cuadroArchivisticoViewModel.IdEntidad);
                    cuadroArchivisticoViewModel.CuadroArchivistico.Habilitado = true;
                }

                cuadroArchivisticoViewModel.CuadroArchivistico.IDPadreCuadroArchivistico = idPadre;
                cuadroArchivisticoViewModel.CuadroArchivistico.IDTipoCuadroArchivistico = (int)TiposCuadroArchivistico.Serie;
                cuadroArchivisticoViewModel.CuadroArchivisticos = archivisticoRepositorio.Buscar(cuadroArchivisticoViewModel);
                cuadroArchivisticoViewModel.TotalEncontrados = archivisticoRepositorio.ObtenerTotalRegistros(cuadroArchivisticoViewModel);
                cuadroArchivisticoViewModel.Paginas = Paginar.ObtenerCantidadPaginas(cuadroArchivisticoViewModel.TotalEncontrados,
                    cuadroArchivisticoViewModel.TamanoPagina);
            }

            LimpiarModelState();

            return View(cuadroArchivisticoViewModel);
        }

        public ActionResult Guardar(string idCuadroArchivistico, string idPadre, string idEntidadRegresar)
        {
            var idCuadroArchivisticoBuscar = idCuadroArchivistico.DecodeFrom64().TryToInt();
            var idEntidad = idEntidadRegresar.DecodeFrom64().TryToInt();

            var viewModel = new GuardarCuadroArchivisticoViewModel {IdPadre = idPadre.DecodeFrom64().TryToInt()};

            using (var bd = new Contexto())
            {
                var archivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);

                CuadroArchivistico cuadroArchivistico;
                if (idCuadroArchivisticoBuscar > 0)
                {                   
                    cuadroArchivistico = archivisticoRepositorio
                        .BuscarPorIdCuadroArchivistico(idCuadroArchivisticoBuscar);
                    viewModel.EstablecerIdAntecesores(cuadroArchivistico);
                }
                else
                {
                    cuadroArchivistico = new CuadroArchivistico();

                    if (viewModel.IdPadre > 0)
                    {
                        var antecesores = archivisticoRepositorio.BuscarUnoSolo(a => a.IDCuadroArchivistico == viewModel.IdPadre,
                            new[]
                            {
                                "PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico.PadreCuadroArchivistico"
                            });

                        viewModel.EstablecerIdAntecesores(antecesores);
                    }
                }
                
                viewModel.CuadroArchivistico = cuadroArchivistico;
                ViewBag.IdEntidadRegresar = idEntidad;

                return View(viewModel);
            }
        }

        [HttpPost]
        public ActionResult Guardar(GuardarCuadroArchivisticoViewModel viewModel, string idPadre)
        {
            var idCuadroArchivistico = ObtenerParametroGetEnInt(Enumerados.Parametro.IdCuadroArchivistico);
            viewModel.IdPadre = Request.QueryString["idPadre"].DecodeFrom64().TryToInt();

            var idEntidad = Request.QueryString["idEntidadRegresar"].DecodeFrom64().TryToInt();

            var cuadroArchivistico = viewModel.CuadroArchivistico;

            if (viewModel.IdSubseccion > 0)
            {
                cuadroArchivistico.IDPadreCuadroArchivistico = viewModel.IdSubseccion;
            }
            else if (viewModel.IdSeccion > 0)
            {
                cuadroArchivistico.IDPadreCuadroArchivistico = viewModel.IdSeccion;
            }

            using (var bd = new Contexto())
            {
                var cuadroarchivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);
                cuadroArchivistico.IDCuadroArchivistico = idCuadroArchivistico;
                cuadroArchivistico.IDTipoCuadroArchivistico = (int)TiposCuadroArchivistico.Serie;
                AccionesAuditoria accion;
                if (cuadroArchivistico.IDCuadroArchivistico > 0)
                {
                    cuadroArchivistico.FechaModificado = DateTime.Now;
                    cuadroarchivisticoRepositorio.Modificar(cuadroArchivistico);
                    EscribirCorrectoMensaje(General.GuardadoCorrecto);
                    accion = AccionesAuditoria.Modificar;
                }
                else
                {
                    cuadroArchivistico.Habilitado = true;
                    cuadroArchivistico.FechaCreacion = DateTime.Now;
                    cuadroArchivistico.FechaModificado = cuadroArchivistico.FechaCreacion;
                    cuadroarchivisticoRepositorio.Guardar(cuadroArchivistico);
                    EscribirCorrectoMensaje(General.GuardadoCorrecto);

                    accion = AccionesAuditoria.Guardar;
                }
                GuardarMovimientoAuditoria(cuadroArchivistico.IDCuadroArchivistico, ModulosAuditoria.Series, accion, cuadroArchivistico.Nombre);
                bd.SaveChanges();
            }

            var parametrosGet = new
            {
                idCuadroArchivistico = cuadroArchivistico.IDCuadroArchivistico.ToString().EncodeTo64(),
                idPadre = viewModel.IdPadre.ToString().EncodeTo64(),
                idEntidadRegresar = idEntidad.ToString().EncodeTo64()
            };

            return RedirectToAction("Guardar", parametrosGet);
        }

        public ActionResult Habilitar(string idCuadroarchivistico, string idPadre)
        {
            var idCuadroarchivisticoHabilitar = idCuadroarchivistico.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                var cuadroarchivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);
                cuadroarchivisticoRepositorio.CambiarHabilitado(idCuadroarchivisticoHabilitar, true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);
                GuardarMovimientoAuditoria(idCuadroarchivisticoHabilitar, ModulosAuditoria.Series, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }

            return RedirectToAction("Index", new {idPadre});
        }

        public ActionResult Deshabilitar(string idCuadroarchivistico, string idPadre)
        {
            var idCuadroarchivisticoHabilitar = idCuadroarchivistico.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                var cuadroarchivisticoRepositorio = new CuadroArchivisticoRepositorio(bd);
                cuadroarchivisticoRepositorio.CambiarHabilitado(idCuadroarchivisticoHabilitar, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);
                GuardarMovimientoAuditoria(idCuadroarchivisticoHabilitar, ModulosAuditoria.Series, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }

            return RedirectToAction("Index", new {idPadre});
        }       
    }
}