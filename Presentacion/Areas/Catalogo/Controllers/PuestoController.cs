﻿using System.Linq;
using System.Web.Mvc;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Enums;
using Datos.Recursos;
using Datos.Repositorios.Catalogos;
using Presentacion.Controllers;
using Sistema.Extensiones;
using Sistema.Paginador;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class PuestoController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index(PuestoViewModel puestoViewModel)
        {
            puestoViewModel.Puesto = new Puesto { Habilitado = true };
            using (var bd = new Contexto())
            {
                var puestoRepostorio = new PuestoRepostorio(bd);
                puestoViewModel.Puestos = puestoRepostorio.Buscar(puestoViewModel);
                puestoViewModel.TotalEncontrados = puestoRepostorio.ObtenerTotalRegistros(puestoViewModel);
                puestoViewModel.Paginas = Paginar.ObtenerCantidadPaginas(puestoViewModel.TotalEncontrados,
                                                puestoViewModel.TamanoPagina);
            }
            return View(puestoViewModel);
        }

        [HttpPost]
        public ActionResult Index(PuestoViewModel puestoViewModel, string pagina)
        {
            if (string.IsNullOrEmpty(pagina)) pagina = "1";
            puestoViewModel.PaginaActual = pagina.TryToInt();
            using (var bd = new Contexto())
            {
                var puestoRepostorio = new PuestoRepostorio(bd);
                puestoViewModel.Puestos = puestoRepostorio.Buscar(puestoViewModel);
                puestoViewModel.TotalEncontrados = puestoRepostorio.ObtenerTotalRegistros(puestoViewModel);
                puestoViewModel.Paginas = Paginar.ObtenerCantidadPaginas(puestoViewModel.TotalEncontrados,
                                                puestoViewModel.TamanoPagina);
            }
            LimpiarModelState();
            return View(puestoViewModel);
        }

        [HttpGet]
        public ActionResult Guardar(string idPuesto)
        {
            var idPuestoBuscar = idPuesto.DecodeFrom64().TryToInt();
            var puesto = new Puesto { IDPuesto = idPuestoBuscar };
            if (idPuestoBuscar > 0)
            {
                using (var bd = new Contexto())
                {
                    var puestoRepostorio = new PuestoRepostorio(bd);
                    puesto = puestoRepostorio.BuscarUnoSolo(p => p.IDPuesto == idPuestoBuscar);
                }
            }
            GuardarDatoTemporal(Enumerados.TempData.IdModificar, idPuestoBuscar, true);
            return View(puesto);
        }

        [HttpPost]
        public ActionResult Guardar(Puesto puesto)
        {
            puesto.IDPuesto = Request.QueryString["idPuesto"].DecodeFrom64().TryToInt();
            if (puesto.IDPuesto > 0)
            {
                ModelState["idPuesto"].Errors.Clear();
            }
            AccionesAuditoria accion;
            if (ModelState.IsValid)
            {
                puesto.IDPuesto = ObtenerParametroGetEnInt(Enumerados.Parametro.IdPuesto);
                puesto.Habilitado = true;
                using (var bd = new Contexto())
                {
                   var puestoRepostorio = new PuestoRepostorio(bd);

                    if (puesto.IDPuesto > 0)
                    {
                        puestoRepostorio.Modificar(puesto);
                        accion = AccionesAuditoria.Modificar;
                    }
                    else
                    {
                        puestoRepostorio.Guardar(puesto);
                        accion = AccionesAuditoria.Guardar;
                    }
                    GuardarMovimientoAuditoria(puesto.IDPuesto, ModulosAuditoria.Puestos, accion, puesto.Nombre);
                    bd.SaveChanges();

                    EscribirCorrectoMensaje(General.GuardarCorrectoPuesto);
                }
            }

            return View(puesto);
        }

        [HttpGet]
        public ActionResult Habilitar(string idPuesto)
        {
           var idPuestoHabilitar = idPuesto.DecodeFrom64().TryToInt();
           using (var bd = new Contexto())
           {
                
                var puestoRepostorio = new PuestoRepostorio(bd);
                puestoRepostorio.CambiarHabilitado(idPuestoHabilitar, true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);
                GuardarMovimientoAuditoria(idPuestoHabilitar, ModulosAuditoria.Puestos, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
           }
           return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Deshabilitar(string idPuesto)
        {
            var idPuestoHabilitar = idPuesto.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                
                var puestoRepostorio = new PuestoRepostorio(bd);
                puestoRepostorio.CambiarHabilitado(idPuestoHabilitar, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);
                GuardarMovimientoAuditoria(idPuestoHabilitar, ModulosAuditoria.Puestos, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Eliminar(string idPuesto)
        {
            var idPuestoEliminar = idPuesto.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var puestoRepostorio = new PuestoRepostorio(bd);
                puestoRepostorio.Eliminar(new Puesto { IDPuesto = idPuestoEliminar });
                bd.SaveChanges();
                EscribirCorrectoMensaje(General.EliminadoCorrecto);
            }      
            return RedirectToAction("Index");
        }

        public ActionResult VerificarExiste(Puesto puesto)
        {
            var idModificar = ObtenerDatoTemporal<int>(Enumerados.TempData.IdModificar, true);
            using (var bd = new Contexto())
            {
                var puestoRepostorio = new PuestoRepostorio(bd);
                var existe = puestoRepostorio.Buscar(s => s.Nombre == puesto.Nombre && s.IDPuesto != idModificar).Any();
                return Json(!existe, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
