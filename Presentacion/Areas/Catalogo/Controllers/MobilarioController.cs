﻿using System;
using System.Web.Mvc;
using System.Web.Services;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Enums;
using Datos.Repositorios.Catalogos;
using Presentacion.Controllers;
using Resources;
using Sistema.Extensiones;
using Sistema.Paginador;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class MobilarioController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index(MobilarioViewModel mobilarioViewModel)
        {
            mobilarioViewModel.Mobilario = new Mobilario
            {
                Habilitado = true,
                Area = new Area {Nivel = new Nivel {Edificio = new Edificio {Localidad = new Localidad()}}}
            };
            var idCliente = mobilarioViewModel.IdArea.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                if (idCliente > 0)
                {
                    var areaRepositorio = new ClienteRepositorio(bd);
                    var cliente = areaRepositorio
                        .BuscarUnoSolo(l => l.IdCliente == idCliente, new[] { "Nivel.Edificio.Localidad.Municipio" });
                }
                else
                {
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    mobilarioViewModel.Mobilario.Area.Nivel.Edificio.Localidad.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();
                }

                var mobilarioRepositorio = new MobilarioRepositorio(bd);
                mobilarioViewModel.Mobilarios = mobilarioRepositorio.Buscar(mobilarioViewModel);
                mobilarioViewModel.TotalEncontrados = mobilarioRepositorio.ObtenerTotalRegistros(mobilarioViewModel);
                mobilarioViewModel.Paginas = Paginar.ObtenerCantidadPaginas(mobilarioViewModel.TotalEncontrados,
                    mobilarioViewModel.TamanoPagina);
            }

            return View(mobilarioViewModel);
        }

        [HttpPost]
        public ActionResult Index(MobilarioViewModel mobilarioViewModel, string pagina)
        {
            mobilarioViewModel.PaginaActual = pagina.TryToInt();
            var idCliente = mobilarioViewModel.IdArea.DecodeFrom64().TryToInt();

            if (mobilarioViewModel.PaginaActual <= 0) mobilarioViewModel.PaginaActual = 1;

            using (var bd = new Contexto())
            {
                if (idCliente > 0)
                {
                    var areaRepositorio = new ClienteRepositorio(bd);
                    var cliente = areaRepositorio.BuscarUnoSolo(l => l.IdCliente == idCliente,
                        new[] {"Nivel.Edificio.Localidad.Municipio"});
                }
                else
                {
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    mobilarioViewModel.Mobilario.Area.Nivel.Edificio.Localidad.IDEstado = estadoRepositorio
                        .ObtenerIdEstadoPredeterminado();
                }

                var mobilarioRepositorio = new MobilarioRepositorio(bd);
                mobilarioViewModel.Mobilarios = mobilarioRepositorio.Buscar(mobilarioViewModel);
                mobilarioViewModel.TotalEncontrados = mobilarioRepositorio.ObtenerTotalRegistros(mobilarioViewModel);
                mobilarioViewModel.Paginas = Paginar.ObtenerCantidadPaginas(mobilarioViewModel.TotalEncontrados,
                    mobilarioViewModel.TamanoPagina);
            }

            LimpiarModelState();

            return View(mobilarioViewModel);
        }

        [HttpGet]
        public ActionResult Guardar(string idMobilario, string idAreaRegresar)
        {
            var mobilario = new Mobilario {IDMobilario = idMobilario.DecodeFrom64().TryToInt()};
            var idCliente = idAreaRegresar.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                if (mobilario.IDMobilario > 0)
                {
                    var mobilarioRepositorio = new MobilarioRepositorio(bd);
                    mobilario = mobilarioRepositorio.BuscarUnoSolo(e => e.IDMobilario == mobilario.IDMobilario,
                        new[] { "Area.Nivel.Edificio.Localidad" });
                }
                else if (idCliente > 0)
                {

                }
                else
                {
                    mobilario.Area = new Area
                    {
                        Nivel = new Nivel {Edificio = new Edificio {Localidad = new Localidad()}}
                    };
                    var repositorioEstado = new EstadoRepositorio(bd);
                    mobilario.Area.Nivel.Edificio.Localidad.IDEstado = repositorioEstado.ObtenerIdEstadoPredeterminado();
                }
            }

            return View(mobilario);
        }

        [HttpPost]
        public ActionResult Guardar(Mobilario mobilario)
        {
            mobilario.IDMobilario = Request.QueryString["idMobilario"].DecodeFrom64().TryToInt();
            var idArea = Request.QueryString["idAreaRegresar"].DecodeFrom64().TryToInt();
            mobilario.Area = null;

            ModelState["idMobilario"]?.Errors.Clear();
            AccionesAuditoria accion;
            if (ModelState.IsValid)
            {
                mobilario.IDMobilario = ObtenerParametroGetEnInt(Enumerados.Parametro.IdMobilario);

                using (var bd = new Contexto())
                {
                    var mobilarioRepositorio = new MobilarioRepositorio(bd);

                    if (mobilario.IDMobilario > 0)
                    {
                        mobilario.Habilitado = true;
                        mobilario.FechaModificacion = DateTime.Now;
                        mobilarioRepositorio.Modificar(mobilario);
                        accion = AccionesAuditoria.Modificar;
                    }
                    else
                    {
                        mobilario.Habilitado = true;
                        mobilario.FechaCreacion = DateTime.Now;
                        mobilario.FechaModificacion = mobilario.FechaCreacion;
                        mobilarioRepositorio.Guardar(mobilario);
                        accion = AccionesAuditoria.Guardar;
                    }

                    GuardarMovimientoAuditoria(mobilario.IDMobilario, ModulosAuditoria.Mobiliarios, accion, mobilario.Nombre);
                    bd.SaveChanges();

                    mobilario.Area = new Area
                    {
                        Nivel = new Nivel {Edificio = new Edificio {Localidad = new Localidad()}}
                    };
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    mobilario.Area.Nivel.Edificio.Localidad.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();
                }

                EscribirCorrectoMensaje(General.GuardadoCorrecto);
            }
            else
            {
                EscribirErrorMensaje(string.Format(General.FalloGuardado, "Mobilario"));
            }

            dynamic parametrosGet;

            if (idArea > 0)
                parametrosGet = new
                {
                    idMobilario = mobilario.IDMobilario.ToString().EncodeTo64(),
                    idAreaRegresar = idArea.ToString().EncodeTo64()
                };
            else
                parametrosGet = new { idMobilario = mobilario.IDMobilario.ToString().EncodeTo64() };

            return RedirectToAction("Guardar", parametrosGet);
        }

        [WebMethod]
        [HttpPost]
        public JsonResult ListaMobilarios(int idArea, string buscar, bool filtrarAgregados = false)
        {
            var lista = filtrarAgregados
                ? Json(MobilarioRepositorio.Buscar(idArea, buscar, ObtenerSesion().IdEntidad))
                : Json(MobilarioRepositorio.Buscar(idArea, buscar));
            return lista;
        }

        [HttpGet]
        public ActionResult Habilitar(string idMobilario, string idAreaRegresar)
        {
            var idMobilarioCambiar = idMobilario.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var mobilarioRepositorio = new MobilarioRepositorio(bd);
                mobilarioRepositorio.CambiarHabilitado(idMobilarioCambiar, true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);
                GuardarMovimientoAuditoria(idMobilarioCambiar, ModulosAuditoria.Mobiliarios, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }
            return RedirectToAction("Index", new { idArea = idAreaRegresar });
        }

        [HttpGet]
        public ActionResult Deshabilitar(string idMobilario, string idAreaRegresar)
        {
            var idMobilarioCambiar = idMobilario.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var mobilarioRepositorio = new MobilarioRepositorio(bd);
                mobilarioRepositorio.CambiarHabilitado(idMobilarioCambiar, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);
                GuardarMovimientoAuditoria(idMobilarioCambiar, ModulosAuditoria.Mobiliarios, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }
            return RedirectToAction("Index", new { idArea = idAreaRegresar });
        }
    }
}