﻿using System;
using System.Web.Mvc;
using System.Web.Services;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Enums;
using Datos.Repositorios.Catalogos;
using Presentacion.Controllers;
using Resources;
using Sistema.Extensiones;
using Sistema.Paginador;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class EdificioController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index(EdificioViewModel edificioViewModel)
        {
            edificioViewModel.Edificio = new Edificio {Habilitado = true, Localidad = new Localidad()};
            var idLocalidad = edificioViewModel.IdLocalidad.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                if (idLocalidad > 0)
                {
                    var localidadRepositorio = new LocalidadRepositorio(bd);
                    var localidad = localidadRepositorio
                        .BuscarUnoSolo(l => l.IDLocalidad == idLocalidad, new[] {"Municipio"});
                    if (localidad != null) edificioViewModel.Edificio.Localidad = localidad;
                }
                    else
                {
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    edificioViewModel.Edificio.Localidad.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();
                }

                var edificioRepositorio = new EdificioRepositorio(bd);
                edificioViewModel.Edificios = edificioRepositorio.Buscar(edificioViewModel);
                edificioViewModel.TotalEncontrados = edificioRepositorio.ObtenerTotalRegistros(edificioViewModel);
                edificioViewModel.Paginas = Paginar.ObtenerCantidadPaginas(edificioViewModel.TotalEncontrados,
                    edificioViewModel.TamanoPagina);
            }

            return View(edificioViewModel);
        }

        [HttpPost]
        public ActionResult Index(EdificioViewModel edificioViewModel, string pagina)
        {
            edificioViewModel.PaginaActual = pagina.TryToInt();
            var idLocalidad = edificioViewModel.IdLocalidad.DecodeFrom64().TryToInt();

            if (edificioViewModel.PaginaActual <= 0) edificioViewModel.PaginaActual = 1;

            using (var bd = new Contexto())
            {
                if (idLocalidad > 0)
                {
                    var localidadRepositorio = new LocalidadRepositorio(bd);
                    var localidad = localidadRepositorio
                        .BuscarUnoSolo(l => l.IDLocalidad == idLocalidad, new[] { "Municipio" });
                    if (localidad != null) edificioViewModel.Edificio.Localidad = localidad;
                }
                else
                {
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    edificioViewModel.Edificio.Localidad.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();
                }

                var edificioRepositorio = new EdificioRepositorio(bd);
                edificioViewModel.Edificios = edificioRepositorio.Buscar(edificioViewModel);
                edificioViewModel.TotalEncontrados = edificioRepositorio.ObtenerTotalRegistros(edificioViewModel);
                edificioViewModel.Paginas = Paginar.ObtenerCantidadPaginas(edificioViewModel.TotalEncontrados,
                    edificioViewModel.TamanoPagina);
            }

            LimpiarModelState();

            return View(edificioViewModel);
        }

        [HttpGet]
        public ActionResult Guardar(string idEdificio, string idLocalidadRegresar)
        {
            var edificio = new Edificio {IDEdificio = idEdificio.DecodeFrom64().TryToInt()};
            var idLocalidad = idLocalidadRegresar.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                if (edificio.IDEdificio > 0)
                {
                    var edificioRepositorio = new EdificioRepositorio(bd);
                    edificio = edificioRepositorio.BuscarUnoSolo(e => e.IDEdificio == edificio.IDEdificio,
                        new[] {"Localidad"});
                }
                else if (idLocalidad > 0)
                {
                    var repositorioLocalidad = new LocalidadRepositorio(bd);
                    edificio.Localidad = repositorioLocalidad.BuscarUnoSolo(l => l.IDLocalidad == idLocalidad);
                    edificio.IDLocalidad = idLocalidad;
                }
                else
                {
                    edificio.Localidad = new Localidad();
                    var repositorioEstado = new EstadoRepositorio(bd);
                    edificio.Localidad.IDEstado = repositorioEstado.ObtenerIdEstadoPredeterminado();
                }
            }

            ViewBag.IdLocalidadRegresar = idLocalidad;

            return View(edificio);
        }

        [HttpPost]
        public ActionResult Guardar(Edificio edificio)
        {
            edificio.IDEdificio = Request.QueryString["idEdificio"].DecodeFrom64().TryToInt();
            var idLocalidad = Request.QueryString["idLocalidadRegresar"].DecodeFrom64().TryToInt();
            edificio.Localidad = null;

            ModelState["idEdificio"]?.Errors.Clear();

            if (ModelState.IsValid)
            {
                edificio.IDEdificio = ObtenerParametroGetEnInt(Enumerados.Parametro.IdEdificio);

                using (var bd = new Contexto())
                {
                    var edificioRepositorio = new EdificioRepositorio(bd);
                    AccionesAuditoria accion;
                    if (edificio.IDEdificio > 0)
                    {
                        edificio.Habilitado = true;
                        edificio.FechaModificacion = DateTime.Now;
                        edificioRepositorio.Modificar(edificio);
                        accion = AccionesAuditoria.Modificar;
                    }
                    else
                    {
                        edificio.Habilitado = true;
                        edificio.FechaCreacion = DateTime.Now;
                        edificio.FechaModificacion = edificio.FechaCreacion;
                        edificioRepositorio.Guardar(edificio);
                        accion = AccionesAuditoria.Guardar;
                    }

                    GuardarMovimientoAuditoria(edificio.IDEdificio, ModulosAuditoria.Edificios, accion, edificio.Nombre);
                    bd.SaveChanges();

                    edificio.Localidad = new Localidad();
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    edificio.Localidad.IDEstado = estadoRepositorio.ObtenerIdEstadoPredeterminado();
                }

                EscribirCorrectoMensaje(General.GuardadoCorrecto);
            }
            else
            {
                EscribirErrorMensaje(string.Format(General.FalloGuardado, "Edificio"));
            }

            dynamic parametrosGet;

            if (idLocalidad > 0)
                parametrosGet = new
                {
                    idEdificio = edificio.IDEdificio.ToString().EncodeTo64(),
                    idLocalidadRegresar = idLocalidad.ToString().EncodeTo64()
                };
            else
                parametrosGet = new {idEdificio = edificio.IDEdificio.ToString().EncodeTo64()};

            return RedirectToAction("Guardar", parametrosGet);
        }

        [WebMethod]
        [HttpPost]
        public JsonResult ListaEdificios(int idLocalidad, string buscar, bool filtrarAgregados = false)
        {
            var lista = filtrarAgregados
                ? Json(EdificioRepositorio.Buscar(idLocalidad, buscar, ObtenerSesion().IdEntidad))
                : Json(EdificioRepositorio.Buscar(idLocalidad, buscar));
            return lista;
        }

        [HttpGet]
        public ActionResult Habilitar(string idEdificio, string idLocalidadRegresar)
        {
            var idEdificioCambiar = idEdificio.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var edificioRepositorio = new EdificioRepositorio(bd);
                edificioRepositorio.CambiarHabilitado(idEdificioCambiar, true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);

                GuardarMovimientoAuditoria(idEdificioCambiar, ModulosAuditoria.Edificios, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }
            return RedirectToAction("Index", new {idLocalidad = idLocalidadRegresar});
        }

        [HttpGet]
        public ActionResult Deshabilitar(string idEdificio, string idLocalidadRegresar)
        {
            var idEdificioCambiar = idEdificio.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var edificioRepositorio = new EdificioRepositorio(bd);
                edificioRepositorio.CambiarHabilitado(idEdificioCambiar, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);

                GuardarMovimientoAuditoria(idEdificioCambiar, ModulosAuditoria.Edificios, AccionesAuditoria.Habilitar);
                bd.SaveChanges();
            }
            return RedirectToAction("Index", new { idLocalidad = idLocalidadRegresar });
        }
    }
}