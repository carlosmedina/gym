﻿using System.Web.Mvc;
using Presentacion.Controllers;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Enums;
using Datos.Repositorios.Catalogos;
using Sistema.Paginador;
using Resources;
using Sistema.Extensiones;

namespace Presentacion.Areas.Catalogo.Controllers
{
    public class EstadoController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index(EstadoViewModel estadoViewModel)
        {
            estadoViewModel.Estado = new Estado();

            using (var bd = new Contexto())
            {
                var estadoRepositorio = new EstadoRepositorio(bd);
                estadoViewModel.Estados = estadoRepositorio.Buscar(estadoViewModel);
                estadoViewModel.TotalEncontrados = estadoRepositorio.ObtenerTotalRegistros(estadoViewModel);
                estadoViewModel.Paginas = Paginar.ObtenerCantidadPaginas(estadoViewModel.TotalEncontrados,
                    estadoViewModel.TamanoPagina);
            }

            return View(estadoViewModel);
        }

        [HttpPost]
        public ActionResult Index(EstadoViewModel estadoViewModel, string pagina)
        {
            estadoViewModel.PaginaActual = pagina.TryToInt();

            if (estadoViewModel.PaginaActual <= 0) estadoViewModel.PaginaActual = 1;

            using (var bd = new Contexto())
            {
                var estadoRepositorio = new EstadoRepositorio(bd);
                estadoViewModel.Estados = estadoRepositorio.Buscar(estadoViewModel);
                estadoViewModel.TotalEncontrados = estadoRepositorio.ObtenerTotalRegistros(estadoViewModel);
                estadoViewModel.Paginas = Paginar.ObtenerCantidadPaginas(estadoViewModel.TotalEncontrados,
                    estadoViewModel.TamanoPagina);
            }

            LimpiarModelState();

            return View(estadoViewModel);
        }

        [HttpGet]
        public ActionResult Guardar(string idEstado)
        {
            var estado = new Estado { IDEstado = idEstado.DecodeFrom64().TryToInt() };
            if (estado.IDEstado > 0)
            {
                using (var bd = new Contexto())
                {
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    estado = estadoRepositorio.BuscarPorId(estado.IDEstado);
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
            return View(estado);
        }

        [HttpPost]
        public ActionResult Guardar(Estado estado)
        {
            estado.IDEstado = Request.QueryString["idEstado"].DecodeFrom64().TryToInt();
            
            estado.IDEstado = ObtenerParametroGetEnInt(Enumerados.Parametro.IdEstado);
            using (var bd = new Contexto())
            {
                AccionesAuditoria accion;
                var estadoRepositorio = new EstadoRepositorio(bd);
             
                if (estado.IDEstado > 0)
                {
                    if(estado.Predeterminado)
                        estadoRepositorio.QuitarPredeterminado();

                    estadoRepositorio.Modificar(estado);
                    accion = AccionesAuditoria.Modificar;
                }
                else
                {
                    accion = AccionesAuditoria.Guardar;
                }
                GuardarMovimientoAuditoria(estado.IDEstado, ModulosAuditoria.Estados, accion, estado.Nombre);
                bd.SaveChanges();
            }

            EscribirCorrectoMensaje(General.GuardadoCorrecto);

            return View(estado);
        }
    }
}
