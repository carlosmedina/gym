﻿using System;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Services;
using Sistema.Extensiones;
using Datos;
using Datos.Recursos;
using Datos.Enums;
using Datos.Repositorios.Configuracion;
using Datos.DTO.Infraestructura.ViewModels;
using Presentacion.Controllers;

namespace Presentacion.Areas.Configuracion.Controllers
{
    public class MenuController : ControladorBase
    {
        private MenuViewModel _menuViewModel;

        [HttpGet]
        public ActionResult Index(string idRol, string idUsuario, string idMenuPadre, string habilitado = "on")
        {
            var idGestionMenu = 0;
            var tipoGestion = (Enumerados.GestionMenu)0;
            var nombreTipoGestion = string.Empty;
            var buscar = string.Empty;
            var habilitadoBuscar = habilitado.TryToBool();
            var idRolBuscar = idRol.DecodeFrom64().TryToInt();
            var idUsuarioBuscar = idUsuario.DecodeFrom64().TryToInt();
            var idMenuPadreBuscar = idMenuPadre.DecodeFrom64().TryToInt();

            try
            {
                tipoGestion = ObtenerDatoTemporal<Enumerados.GestionMenu>(Enumerados.TempData.TipoGestion);
                idGestionMenu = ObtenerDatoTemporal<int>(Enumerados.TempData.IdGestionarMenu);
                nombreTipoGestion = ObtenerDatoTemporal<string>(Enumerados.TempData.NombreTipoGestion);
            }
            catch (Exception)
            {
                idGestionMenu = 0;
            }
            finally
            {
                _menuViewModel = new MenuViewModel(idRolBuscar, idUsuarioBuscar, idMenuPadreBuscar, buscar,
                habilitadoBuscar, idGestionMenu, tipoGestion, nombreTipoGestion)
                {
                    IdPadre = idMenuPadreBuscar
                };

                if (idRolBuscar > 0 || idUsuarioBuscar > 0 || _menuViewModel.IdGestionarMenu >= 0)
                {
                    GuardarDatoTemporal(Enumerados.TempData.TipoGestion, _menuViewModel.TipoGestion, true);
                    GuardarDatoTemporal(Enumerados.TempData.IdGestionarMenu, _menuViewModel.IdGestionarMenu, true);
                    GuardarDatoTemporal(Enumerados.TempData.NombreTipoGestion, _menuViewModel.NombreTipoGestion, true);
                }
            }

            return View(_menuViewModel);
        }

        [HttpPost]
        public ActionResult Index(Menu menu)
        {
            var idGestionMenu = 0;
            var tipoGestion = (Enumerados.GestionMenu)0;
            var nombreTipoGestion = string.Empty;
            var habilitadoBuscar = menu.Habilitado;
            var idRol = ObtenerParametroGetEnInt(Enumerados.Parametro.IdRol);
            var idUsuario = ObtenerParametroGetEnInt(Enumerados.Parametro.IdUsuario);
            var idMenuPadre = ObtenerParametroGetEnInt(Enumerados.Parametro.IdMenuPadre);

            try
            {
                tipoGestion = ObtenerDatoTemporal<Enumerados.GestionMenu>(Enumerados.TempData.TipoGestion, true);
                idGestionMenu = ObtenerDatoTemporal<int>(Enumerados.TempData.IdGestionarMenu, true);
                nombreTipoGestion = ObtenerDatoTemporal<string>(Enumerados.TempData.NombreTipoGestion);
            }
            catch (Exception)
            {
                idGestionMenu = 0;
            }
            finally
            {
                _menuViewModel = new MenuViewModel(idRol, idUsuario, idMenuPadre, menu.Opcion, habilitadoBuscar,
                    idGestionMenu, tipoGestion, nombreTipoGestion);
                ViewBag.TipoGestion = _menuViewModel.TipoGestion;
            }

            return View(_menuViewModel);
        }

        [HttpGet]
        public ActionResult Guardar(string idMenu, string idMenuPadre)
        {
            var menu = new Menu();
            var idMenuGuardar = idMenu.DecodeFrom64().TryToInt();
            idMenuPadre.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                var menuRepositorio = new MenuRepositorio(bd);
                if (idMenuGuardar != 0)
                {
                    menu = menuRepositorio.BuscarPorId(idMenuGuardar);
                }
                else
                {
                    menu.Padre = int.Parse(idMenuPadre.DecodeFrom64());
                }
            }

            return View(menu);
        }

        [HttpPost]
        public ActionResult Guardar(Menu menu)
        {        
            var idMenu = ObtenerParametroGetEnInt(Enumerados.Parametro.IdMenu);
            var idMenuPadre = ObtenerParametroGetEnInt(Enumerados.Parametro.IdMenuPadre);

            ModelState["IDMenu"]?.Errors.Clear();

            if (ModelState.IsValid)
            {
                using (var bd = new Contexto())
                {
                    
                    var menuRepositorio = new MenuRepositorio(bd);
                    menu.Habilitado = true;
                    menu.IDMenu = idMenu;
                    if (idMenuPadre > 0 || idMenu == 0)
                    {
                        menu.Padre = idMenuPadre;
                        menu.Camino = string.Empty;
                        menu.Indice = menuRepositorio.ObtenerMaximoIndice(idMenuPadre);
                        menuRepositorio.Guardar(menu);
                    }
                    else
                    {
                        menuRepositorio.Modificar(menu);
                        bd.SaveChanges();
                    }
                }
            }
            EscribirCorrectoMensaje(General.GuardadoCorrecto);
            return View(menu);
        }

        [HttpGet]
        public ActionResult Habilitar(string idMenu)
        {
            var idPadre = ObtenerParametroGetEnInt(Enumerados.Parametro.IdMenuPadre);
            var id = idMenu.DecodeFrom64();

            using (var bd = new Contexto())
            {
                
                var menuRepositorio = new MenuRepositorio(bd);
                menuRepositorio.CambiarHabilitado(id.TryToInt(), true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);
            }

            return RedirectToAction("Index", new { idMenuPadre = idPadre.ToString().EncodeTo64() });
        }

        [HttpGet]
        public ActionResult Deshabilitar(string idMenu)
        {
            var idPadre = ObtenerParametroGetEnInt(Enumerados.Parametro.IdMenuPadre);
            var intIdMenu = idMenu.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
               
                var menuRepositorio = new MenuRepositorio(bd);
                menuRepositorio.CambiarHabilitado(intIdMenu, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);
            }
            return RedirectToAction("Index", new { idMenuPadre = idPadre.ToString().EncodeTo64() });
        }

        [HttpGet]
        public ActionResult Agregar(string idMenu)
        {
            var idPadre = ObtenerParametroGetEnInt(Enumerados.Parametro.IdMenuPadre);
            var idMenuAgregar = idMenu.DecodeFrom64().TryToInt();
            var idGestionarMenu = ObtenerDatoTemporal<int>(Enumerados.TempData.IdGestionarMenu);
            var tipoGestion = ObtenerDatoTemporal<Enumerados.GestionMenu>(Enumerados.TempData.TipoGestion);
            var idGestion = idGestionarMenu.ToString(CultureInfo.InvariantCulture).EncodeTo64();

            _menuViewModel = new MenuViewModel(tipoGestion);
            _menuViewModel.AgregarOpcionMenu(idGestionarMenu, idMenuAgregar);

            return tipoGestion == Enumerados.GestionMenu.Usuario
                ? RedirectToAction("Index", new { idUsuario = idGestion, idMenuPadre = idPadre.ToString().EncodeTo64() })
                : RedirectToAction("Index", new { idRol = idGestion, idMenuPadre = idPadre.ToString().EncodeTo64() });
        }

        [HttpGet]
        public ActionResult Quitar(string idMenu)
        {
            var idPadre = ObtenerParametroGetEnInt(Enumerados.Parametro.IdMenuPadre);
            var idMenuAgregar = idMenu.DecodeFrom64().TryToInt();
            var idGestionarMenu = ObtenerDatoTemporal<int>(Enumerados.TempData.IdGestionarMenu);
            var tipoGestion = ObtenerDatoTemporal<Enumerados.GestionMenu>(Enumerados.TempData.TipoGestion);
            var idGestion = idGestionarMenu.ToString(CultureInfo.InvariantCulture).EncodeTo64();

            _menuViewModel = new MenuViewModel(tipoGestion);
            _menuViewModel.QuitarOpcionMenu(idGestionarMenu, idMenuAgregar);

            EscribirCorrectoMensaje(General.AgregadoCorrectamente);
            
            return tipoGestion == Enumerados.GestionMenu.Usuario
                ? RedirectToAction("Index", new { idUsuario = idGestion, idMenuPadre = idPadre.ToString().EncodeTo64() })
                : RedirectToAction("Index", new { idRol = idGestion, idMenuPadre = idPadre.ToString().EncodeTo64() });
        }

        [WebMethod]
        [HttpPost]
        public ActionResult ActivarMenuPersonalizado(string idUsuario, bool activar)
        {
            using (var bd = new Contexto())
            {
               
            }
            return Json(new { guardado = true });
        }

        [WebMethod]
        [HttpPost]
        public ActionResult ComprobarMenuPadre(string idPadre)
        {
            return Json(idPadre.DecodeFrom64().TryToInt() == 0);
        }

        [HttpGet]
        public ActionResult BajarNivel(string idMenu)
        {
            var idPadre = ObtenerParametroGetEnInt(Enumerados.Parametro.IdMenuPadre);
            var id = idMenu.DecodeFrom64();
            using (var bd = new Contexto())
            {
                //bd.Configuration.ValidateOnSaveEnabled = false;
                var menuRepositorio = new MenuRepositorio(bd);
                menuRepositorio.BajarNivel(id.TryToInt(), idPadre);
            }
            return RedirectToAction("Index", new { idMenuPadre = idPadre.ToString().EncodeTo64() });
        }

        [HttpGet]
        public ActionResult SubirNivel(string idMenu)
        {
            var idPadre = ObtenerParametroGetEnInt(Enumerados.Parametro.IdMenuPadre);
            var id = idMenu.DecodeFrom64();
            using (var bd = new Contexto())
            {
                
                var menuRepositorio = new MenuRepositorio(bd);
                menuRepositorio.SubirNivel(id.TryToInt(), idPadre);
            }
            return RedirectToAction("Index", new { idMenuPadre = idPadre.ToString().EncodeTo64() });
        }
    }
}