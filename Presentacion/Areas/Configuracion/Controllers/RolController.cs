﻿using System;
using System.Linq;
using System.Web.Mvc;
using Sistema.Extensiones;
using Datos;
using Datos.Recursos;
using Datos.Enums;
using Datos.Repositorios.Configuracion;
using Datos.DTO.Infraestructura.ViewModels;
using Presentacion.Controllers;

namespace Presentacion.Areas.Configuracion.Controllers
{
    public class RolController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index()
        {
            var rolViewModel = new RolViewModel();

            using (var bd = new Contexto())
            {
                var rolRepositorio = new RolRepositorio(bd);
                rolViewModel.Roles = rolRepositorio.Buscar(r => r.Habilitado,
                    r => r.OrderBy(x => x.Nombre),
                    r => r.MenuInicio);
            }

            return View(rolViewModel);
        }

        [HttpPost]
        public ActionResult Index(Rol rol)
        {
            var rolViewModel = new RolViewModel();

            if (rol.Nombre == null) rol.Nombre = string.Empty;
            ModelState["rol.Nombre"].Errors.Clear();

            using (var bd = new Contexto())
            {
                var rolRepositorio = new RolRepositorio(bd);
                rolViewModel.Roles = rolRepositorio.Buscar(r =>
                    r.Nombre.Contains(rol.Nombre) &&
                    r.Descripcion.Contains(rol.Descripcion ?? r.Descripcion) &&
                    r.Habilitado == rol.Habilitado,
                    r => r.OrderBy(x => x.Nombre),
                    r => r.MenuInicio);
            }

            return View(rolViewModel);
        }

        [HttpGet]
        public ActionResult Guardar(string idRol)
        {
            Rol rol;
            var idRolGuardar = idRol.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                var rolRepositorio = new RolRepositorio(bd);
                rol = rolRepositorio.BuscarUnoSolo(r => r.IDRol == idRolGuardar);
            }

            GuardarDatoTemporal(Enumerados.TempData.IdModificar, idRolGuardar, true);
            return View(rol);
        }

        [HttpPost]
        public ActionResult Guardar(Rol rol)
        {
            rol.IDRol = ObtenerParametroGetEnInt(Enumerados.Parametro.IdRol);
            using (var bd = new Contexto())
            {
                var rolRepositorio = new RolRepositorio(bd);

                AccionesAuditoria accion;

                if (rol.IDRol > 0)
                {
                    rolRepositorio.ModificarRol(rol);
                    EscribirCorrectoMensaje(General.GuardadoCorrecto);
                    accion = AccionesAuditoria.Modificar;
                }
                else
                {
                    rol.FechaCreacion = DateTime.Now;
                    rol.Habilitado = true;
                    rolRepositorio.Guardar(rol);
                    EscribirCorrectoMensaje(General.GuardadoCorrecto);
                    accion = AccionesAuditoria.Guardar;
                }

                GuardarMovimientoAuditoria(rol.IDRol, ModulosAuditoria.Roles, accion, rol.Nombre);

                bd.SaveChanges();
            }
            return View(rol);
        }

        [HttpGet]
        public ActionResult Habilitar(string idRol)
        {
            var idRolHabilitar = idRol.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                var rolRepositorio = new RolRepositorio(bd);
                rolRepositorio.CambiarHabilitado(idRolHabilitar, true);
                EscribirCorrectoMensaje(General.HabilitadoCorrecto);
                
                GuardarMovimientoAuditoria(idRolHabilitar, ModulosAuditoria.Roles, AccionesAuditoria.Habilitar);

                bd.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Deshabilitar(string idRol)
        {
            var idRolHabilitar = idRol.DecodeFrom64().TryToInt();
            using (var bd = new Contexto())
            {
                
                var rolRepositorio = new RolRepositorio(bd);
                rolRepositorio.CambiarHabilitado(idRolHabilitar, false);
                EscribirCorrectoMensaje(General.DeshabilitadoCorrecto);
                
                GuardarMovimientoAuditoria(idRolHabilitar, ModulosAuditoria.Roles, AccionesAuditoria.Habilitar);

                bd.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult VerificarExiste(Rol rol)
        {
            var idModificar = ObtenerDatoTemporal<int>(Enumerados.TempData.IdModificar, true);
            using (var bd = new Contexto())
            {
                var rolRepositorio = new RolRepositorio(bd);
                var existe = rolRepositorio.Buscar(s => s.Nombre == rol.Nombre && s.IDRol != idModificar).Any();
                return Json(!existe, JsonRequestBehavior.AllowGet);
            }
        }
    }
}