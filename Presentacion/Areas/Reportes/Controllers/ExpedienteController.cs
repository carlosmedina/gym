﻿using System.Web.Mvc;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Repositorios.Catalogos;
using Datos.Repositorios.Digitalizacion;
using Negocio.Servicios;
using Presentacion.Controllers;
using Sistema;
using Sistema.Extensiones;
using Sistema.Paginador;

namespace Presentacion.Areas.Reportes.Controllers
{
    public class ExpedienteController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index()
        {
            var viewModel = new ExpedienteBuscarViewModel
            {
                Expediente = new Expediente
                {
                    Cadido = new Cadido {EsAdministrativo = true, EsLegal = true, EsContableFiscal = true},
                    Contenedor = new Contenedor
                    {
                        Mobilario = new Mobilario
                        {
                            Area = new Area {Nivel = new Nivel {Edificio = new Edificio {Localidad = new Localidad()}}}
                        }
                    }
                },
                MostrarProceso = true,
                MostrarConcluido = true
            };
            viewModel.Expediente.IDEntidad = ObtenerSesion().IdEntidad;

            var idMobilario = viewModel.IdMobiliario.DecodeFrom64().TryToInt();

            using (var bd = new Contexto())
            {
                if (idMobilario > 0)
                {
                    var mobilarioRepositorio = new MobilarioRepositorio(bd);
                    var mobilario = mobilarioRepositorio
                        .BuscarUnoSolo(l => l.IDMobilario == idMobilario,
                            new[] {"Area.Nivel.Edificio.Localidad.Municipio"});
                    if (mobilario != null) viewModel.Expediente.Contenedor.Mobilario = mobilario;
                }
                else
                {
                    var estadoRepositorio = new EstadoRepositorio(bd);
                    viewModel.Expediente.Contenedor.Mobilario.Area.Nivel.Edificio.Localidad.IDEstado =
                        estadoRepositorio.ObtenerIdEstadoPredeterminado();
                }

                var expedienteRepositorio = new ExpedienteRepositorio(bd);
                viewModel.Expedientes = expedienteRepositorio.Buscar(viewModel);
                viewModel.TotalEncontrados = expedienteRepositorio.ObtenerTotalRegistros(viewModel);
                viewModel.Paginas = Paginar.ObtenerCantidadPaginas(viewModel.TotalEncontrados,
                    viewModel.TamanoPagina);

            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(ExpedienteBuscarViewModel viewModel)
        {
            var idEntidad = ObtenerSesion().IdEntidad;
            if (idEntidad > 0) viewModel.Expediente.IDEntidad = idEntidad;

            var servicio = new ServiciosReporte();
            var reporte = servicio.GenerarExpedientes(viewModel);

            var nombreReporte = ObtenerSesion().IdUsuario + "_reporte_expediente.xlsx";
            
            TempCarpeta.Guardar(reporte, nombreReporte);
            ViewBag.UbicacionReporte = ObtenerUrlAbsoluta(@"Archivos/Temp/" + nombreReporte);

            return View(viewModel);
        }
    }
}
