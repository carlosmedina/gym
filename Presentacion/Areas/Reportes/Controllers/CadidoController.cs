﻿using System.IO;
using System.Web.Mvc;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Negocio.Servicios;
using Presentacion.Controllers;
using Sistema;

namespace Presentacion.Areas.Reportes.Controllers
{
    public class CadidoController : ControladorBase
    {
        [HttpGet]
        public ActionResult Index()
        {
            var viewModel = new CadidoViewModel
            {
                Cadido = new Cadido
                {
                    Habilitado = true,
                    EsAdministrativo = true,
                    EsLegal = true,
                    EsContableFiscal = true
                },
                IdEntidad = ObtenerSesion().IdEntidad
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(CadidoViewModel viewModel)
        {
            viewModel.Cadido.Habilitado = true;

            var idEntidad = ObtenerSesion().IdEntidad;
            if (idEntidad > 0) viewModel.IdEntidad = idEntidad;

            var servicios = new ServiciosReporte();
            var reporte = servicios.GenerarCadido(viewModel);

            var nombreReporte = ObtenerSesion().IdUsuario + "_reporte_cadido.xlsx";
            
            TempCarpeta.Guardar(reporte, nombreReporte);

            ViewBag.UbicacionReporte = ObtenerUrlAbsoluta(@"Archivos/Temp/" + nombreReporte);

            return View(viewModel);
        }
    }
}