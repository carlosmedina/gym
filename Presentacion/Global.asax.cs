﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using NLog;
using Presentacion.Controllers;
using Sistema;

namespace Presentacion
{
    public class MvcApplication : HttpApplication
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            TempCarpeta.RutaTemp = Server.MapPath(@"Archivos\Temp");
            TempCarpeta.EliminarArchivosAsync();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var esProduccion = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["AmbienteProduccion"]);

            if (esProduccion == false) return;

            var exception = Server.GetLastError();
            Server.ClearError();

            Log.Log(LogLevel.Error, exception, exception.Message);

            var routeData = new RouteData();
            routeData.Values.Add("controller", "Error");

            var httpEx = exception as HttpException;

            if (httpEx != null && httpEx.GetHttpCode() == 403)
            {
                routeData.Values.Add("action", "Error403");
            }
            else if (httpEx != null && httpEx.GetHttpCode() == 404)
            {
                routeData.Values.Add("action", "Error404");
            }
            else
            {
                routeData.Values.Add("action", "Errordefault");
            }

            Response.ContentType = "text/html";

            IController controller = new ErrorController();

            controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));           
        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
