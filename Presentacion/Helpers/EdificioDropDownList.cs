﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Datos.Recursos;
using Datos.Repositorios.Catalogos;

namespace Presentacion.Helpers
{
    public static class EdificioDropDownList
    {
        public static MvcHtmlString EdificioDdl<TModel, TProperty>(
            this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> expression,
            int idLocalidad, string clase = "form-control input-sm", string dataValidation = "", int idEntidad = 0)
        {
            helper.RenderPartial("_EdificioDdl", idEntidad > 0);
            helper.ValidationMessageFor(expression, null, new {@class = "label label warning"});

            if (idLocalidad == 0)
            {
                return helper.DropDownListFor(expression, new[]
                    {
                        new SelectListItem {Text = string.Empty, Value = string.Empty}
                    },
                    new {@class = clase, @disabled = true, data_validation = dataValidation });
            }
            else
            {
                var edificios = idEntidad > 0
                    ? EdificioRepositorio.Buscar(idLocalidad, string.Empty, idEntidad)
                    : EdificioRepositorio.Buscar(idLocalidad, string.Empty);

                if (edificios.Any())
                    edificios.Insert(0, new SelectListItem {Text = General.Seleccione, Value = string.Empty});
                
                return helper.DropDownListFor(expression, edificios, new {@class = clase});
            }
        }
    }
}