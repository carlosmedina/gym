﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Datos.Recursos;
using Datos.Repositorios.Catalogos;

namespace Presentacion.Helpers
{
    public static class NivelDropDownList
    {
        public static MvcHtmlString NivelDdl<TModel, TProperty>(
            this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> expression,
            int idEdificio, string clase = "form-control input-sm", string dataValidation = "", int idEntidad = 0)
        {
            helper.RenderPartial("_NivelDdl", idEntidad > 0);
            helper.ValidationMessageFor(expression, null, new {@class = "label label warning"});

            if (idEdificio == 0)
            {
                return helper.DropDownListFor(expression, new[]
                    {
                        new SelectListItem {Text = string.Empty, Value = string.Empty}
                    },
                    new {@class = clase, @disabled = true, data_validation = dataValidation });
            }
            else
            {
                var niveles = idEntidad > 0
                    ? NivelRepositorio.Buscar(idEdificio, string.Empty, idEntidad)
                    : NivelRepositorio.Buscar(idEdificio, string.Empty);

                if (niveles.Any())
                    niveles.Insert(0, new SelectListItem {Text = General.Seleccione, Value = string.Empty});
                
                return helper.DropDownListFor(expression, niveles, new {@class = clase});
            }
        }
    }
}