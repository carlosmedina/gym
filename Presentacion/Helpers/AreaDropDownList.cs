﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Datos.Recursos;
using Datos.Repositorios.Catalogos;

namespace Presentacion.Helpers
{
    public static class AreaDropDownList
    {
        public static MvcHtmlString AreaDdl<TModel, TProperty>(
            this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> expression,
            int idNivel, string clase = "form-control input-sm", string dataValidation = "", int idEntidad = 0)
        {
            helper.RenderPartial("_AreaDdl", idEntidad > 0);
            helper.ValidationMessageFor(expression, null, new {@class = "label label warning"});

            if (idNivel == 0)
            {
                return helper.DropDownListFor(expression, new[]
                    {
                        new SelectListItem {Text = string.Empty, Value = string.Empty}
                    },
                    new {@class = clase, @disabled = true, data_validation = dataValidation });
            }
            else
            {
                var areas = idEntidad > 0
                    ? ClienteRepositorio.Buscar(idNivel, string.Empty, idEntidad)
                    : ClienteRepositorio.Buscar(idNivel, string.Empty);
                if (areas.Any())
                    areas.Insert(0, new SelectListItem {Text = General.Seleccione, Value = string.Empty});
                
                return helper.DropDownListFor(expression, areas, new {@class = clase});
            }
        }
    }
}