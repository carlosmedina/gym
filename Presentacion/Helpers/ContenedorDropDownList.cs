﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Datos.Recursos;
using Datos.Repositorios.Catalogos;

namespace Presentacion.Helpers
{
    public static class ContenedorDropDownList
    {
        public static MvcHtmlString ContenedorDdl<TModel, TProperty>(
            this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> expression,
            int idMobilario, string clase = "form-control input-sm", string dataValidation = "", int idEntidad = 0)
        {
            helper.RenderPartial("_ContenedorDdl", idEntidad > 0);
            helper.ValidationMessageFor(expression, null, new {@class = "label label warning"});

            if (idMobilario == 0)
            {
                return helper.DropDownListFor(expression, new[]
                    {
                        new SelectListItem {Text = string.Empty, Value = string.Empty}
                    },
                    new {@class = clase, @disabled = true, data_validation = dataValidation });
            }
            else
            {
                var contenedores = idEntidad > 0
                    ? ContenedorRepositorio.Buscar(idMobilario, string.Empty, idEntidad)
                    : ContenedorRepositorio.Buscar(idMobilario, string.Empty);

                if (contenedores.Any())
                    contenedores.Insert(0, new SelectListItem {Text = General.Seleccione, Value = string.Empty});
                
                return helper.DropDownListFor(expression, contenedores, new {@class = clase});
            }
        }
    }
}