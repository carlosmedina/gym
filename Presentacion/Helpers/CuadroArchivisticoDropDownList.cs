﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Repositorios.Configuracion;
using Datos.Recursos;

namespace Presentacion.Helpers
{
    public static class CuadroArchivisticoDropDownList
    {
        public static MvcHtmlString CuadroArchivisticoDdl<TModel, TProperty>(
            this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> expression,
            int idCuadroArchivistico, TiposCuadroArchivistico tipo, int idEntidad = 0)
        {
            helper.RenderPartial("_CuadroArchivisticoDdl",
                new DdlCuadroArchivisticoViewModel {Tipo = tipo, SoloAgregados = idEntidad > 0});
            helper.ValidationMessageFor(expression, null, new {@class = "label label warning"});

            var datosValidaciones = string.Empty;

            if (tipo != TiposCuadroArchivistico.Subfondo && 
                tipo != TiposCuadroArchivistico.Subseccion &&
                tipo != TiposCuadroArchivistico.Subserie)
            {
                datosValidaciones = "required";
            }

            if (tipo != TiposCuadroArchivistico.Subserie)
            {
                datosValidaciones = "";
            }

            if (idCuadroArchivistico == 0)
            {
                return helper.DropDownListFor(expression, new[]
                    {
                        new SelectListItem {Text = string.Empty, Value = string.Empty}
                    },
                    new {@class = "form-control input-sm", disabled = true, data_validation = datosValidaciones });
            }
            else
            {
                var cuadroArchivistico = idEntidad > 0
                    ? CuadroArchivisticoRepositorio.Buscar(idCuadroArchivistico, tipo, idEntidad)
                    : CuadroArchivisticoRepositorio.Buscar(idCuadroArchivistico, tipo);

                if (cuadroArchivistico.Any())
                    cuadroArchivistico.Insert(0, new SelectListItem {Text = General.Seleccione, Value = string.Empty});
                else
                    cuadroArchivistico.Insert(0, new SelectListItem { Text = General.SinElementos, Value = string.Empty });
                
                return helper.DropDownListFor(expression, cuadroArchivistico,
                    new {@class = "form-control input-sm", data_validation = datosValidaciones});
            }
        }
    }
}