﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Datos;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Recursos;
using Datos.Repositorios.Catalogos;

namespace Presentacion.Helpers
{
    public static class LocalidadDropDownList
    {
        public static MvcHtmlString LocalidadDdl<TModel, TProperty>(
            this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> expression,
            int idEstado, int idMunicipio, string dataValidation = "", int idEntidad = 0)
        {
            var viewModel = new LocalidadViewModel("Localidad_IDMunicipio", "IDLocalidad")
            {
                Localidad = new Localidad {IDEstado = idEstado},
                SoloAgregados = idEntidad > 0
            };

            helper.RenderPartial("_LocalidadDdl", viewModel);
            helper.ValidationMessageFor(expression, null, new {@class = "label label warning"});

            if (idMunicipio == 0)
            {
                return helper.DropDownListFor(expression, new[]
                    {
                        new SelectListItem {Text = string.Empty, Value = string.Empty}
                    },
                    new {@class = "form-control input-sm", @disabled = true, data_validation = dataValidation });
            }
            else
            {
                var localidades = idEntidad > 0
                    ? LocalidadRepositorio.Buscar(idEstado, idMunicipio, string.Empty, idEntidad)
                    : LocalidadRepositorio.Buscar(idEstado, idMunicipio, string.Empty);

                if (localidades.Any())
                    localidades.Insert(0, new SelectListItem {Text = General.Seleccione, Value = string.Empty});
                
                return helper.DropDownListFor(expression, localidades, new {@class = "form-control input-sm"});
            }
        }
    }
}