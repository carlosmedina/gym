﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Datos.Recursos;
using Datos.Repositorios.Catalogos;

namespace Presentacion.Helpers
{
    public static class MobilarioDropDownList
    {
        public static MvcHtmlString MobilarioDdl<TModel, TProperty>(
            this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> expression,
            int idArea, string clase = "form-control input-sm", string dataValidation = "", int idEntidad = 0)
        {
            helper.RenderPartial("_MobilarioDdl", idEntidad > 0);
            helper.ValidationMessageFor(expression, null, new {@class = "label label warning"});

            if (idArea == 0)
            {
                return helper.DropDownListFor(expression, new[]
                    {
                        new SelectListItem {Text = string.Empty, Value = string.Empty}
                    },
                    new {@class = clase, @disabled = true, data_validation = dataValidation });
            }
            else
            {
                var mobilarios = idEntidad > 0
                    ? MobilarioRepositorio.Buscar(idArea, string.Empty, idEntidad)
                    : MobilarioRepositorio.Buscar(idArea, string.Empty);
                if (mobilarios.Any())
                    mobilarios.Insert(0, new SelectListItem {Text = General.Seleccione, Value = string.Empty});
                
                return helper.DropDownListFor(expression, mobilarios, new {@class = clase});
            }
        }
    }
}