﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sistema.Extensiones;
using System.Linq.Expressions;

namespace Presentacion.Helpers
{
    public static class LabelsPersonalizados
    {

        public static MvcHtmlString LabelAddonFor<TModel, TProperty>(this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> expression, string clase = null, string icono = null)
        {

            var lbl = new TagBuilder("label");

            var member = (MemberExpression)expression.Body;

            lbl.Attributes["for"] = member.Member.Name;

            if (!string.IsNullOrEmpty(clase))
                lbl.Attributes["class"] = clase;
            if (!string.IsNullOrEmpty(icono))
                lbl.InnerHtml = string.Format("<i class='{0}'></i>", icono);

            return MvcHtmlString.Create(lbl.ToString());
        }

    }

    
}