﻿using System.Collections.Generic;
using Datos;
using Datos.Enums;

namespace Presentacion.Fabrica
{
    public static class Factory
    {
        public static UsuarioDetalles Obtener(Usuario usuario)
        {
            if (usuario == null) return null;
            return new UsuarioDetalles
            {
                NombreUsuario =
                    string.Format("{0} {1} {2}", usuario.Nombre, usuario.ApellidoP, usuario.ApellidoM),
                Sexo = ((Enumerados.Genero)usuario.Sexo - 1).ToString(),
                UrlFoto = usuario.UrlFoto.Trim('~'),
                Email = usuario.Email,
                Rol = usuario.Rol.Nombre
            };
        }

        public static AuditoriaDetalles Obtener(Auditoria auditoria)
        {
            if (auditoria == null) return null;
            return new AuditoriaDetalles
            {
                IdAuditoria = auditoria.IDAuditoria.ToString(),
                IdUsuario = auditoria.Usuario.NombreUsuario,
                IdTipoAuditoria = auditoria.AccionAuditoria.Nombre
            };
        }

        public class UsuarioDetalles
        {
            public string NombreUsuario { get; set; }
            public string TipoUsuario { get; set; }
            public string Sexo { get; set; }
            public string Puesto { get; set; }
            public string UrlFoto { get; set; }
            public string Rol { get; set; }
            public string Email { get; set; }
        }

        public class AuditoriaDetalles
        {
            public string IdAuditoria { get; set; }
            public string IdExpediente { get; set; }
            public string Fecha { get; set; }
            public string IdTipoAuditoria { get; set; }
            public string IdUsuario { get; set; }
        }
    }
}