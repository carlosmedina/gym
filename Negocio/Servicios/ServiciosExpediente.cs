﻿
using System.Collections.Generic;
using System.IO;
using Datos;
using Datos.Repositorios.Digitalizacion;
using Sistema;

namespace Negocio.Servicios
{
    public class ServiciosExpediente
    {
        public Dictionary<string, byte[]> ObtenerExpediente(int idExpediente)
        {
            string ubicacionRelativa;

            using (var bd = new Contexto())
            {
                var expedienteRepositorio = new ExpedienteRepositorio(bd);
                ubicacionRelativa = expedienteRepositorio.ObtenerPropiedadUno(e => e.IDExpediente == idExpediente,
                    e => e.Ubicacion);
            }

            var ubicacionAbsoluta = Archivo.ObtenerRutaExpediente(ubicacionRelativa);

            var expediente = new Dictionary<string, byte[]>();

            if (File.Exists(ubicacionAbsoluta))
            {
                var nombreDocumento = Path.GetFileName(ubicacionAbsoluta);
                var nombre = Path.GetFileName(Path.GetDirectoryName(ubicacionAbsoluta)) + "_" + nombreDocumento;

                expediente.Add(nombre, Archivo.ObtenerExpediente(ubicacionAbsoluta));
            }
            else
            {
                expediente.Add("no_encontrado.pdf", RecursosServicios.PDF_Default);
            }

            return expediente;
        }
    }
}
