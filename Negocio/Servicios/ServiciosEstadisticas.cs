﻿using System;
using System.Collections.Generic;
using Datos;
using System.Linq;

namespace Negocio.Servicios
{
    public class ServiciosEstadisticas
    {
        public static List<SpResultadoConsultasExpedientesMes> RellenarMeses(List<SpResultadoConsultasExpedientesMes> meses, DateTime fechaInicio, DateTime fechaFin)
        {
            for (var i = 0; fechaInicio < fechaFin; fechaInicio = fechaInicio.AddMonths(1))
            {
                var existe = (from c in meses
                              where c.Anio == fechaInicio.Year &&
                          c.Mes == fechaInicio.Month
                    select c).Any();

                if (existe) continue;

                meses.Insert(i, new SpResultadoConsultasExpedientesMes
                {
                    Anio = fechaInicio.Year,
                    Mes = fechaInicio.Month,
                    Cantidad = 0
                });

                i++;
            }

            return meses;
        }
    }
}
