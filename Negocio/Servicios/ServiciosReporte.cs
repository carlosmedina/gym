﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using Datos;
using Datos.DTO.Infraestructura.Reportes;
using Datos.DTO.Infraestructura.ViewModels;
using Datos.Repositorios.Catalogos;
using Datos.Repositorios.Digitalizacion;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using OfficeOpenXml.Style;

namespace Negocio.Servicios
{
    public class ServiciosReporte
    {
        public byte[] GenerarCadido(CadidoViewModel criterios)
        {
            var archivoPlantilla = RecursosServicios.Plantilla_CADIDO;

            List<ReporteCadido> reporteCadidos;

            using (var contexto = new Contexto())
            {
                var repositorio = new CadidoRepositorio(contexto);
                reporteCadidos = repositorio.ObtenerReporte(criterios);
            }

            using (var stream = new MemoryStream(archivoPlantilla))
            using (var excel = new ExcelPackage(stream))
            {
                var hoja = excel.Workbook.Worksheets.First();

                var fila = 6;
                var idPadreActual = 0;
                foreach (var cadido in reporteCadidos)
                {
                    if (idPadreActual != cadido.IDPadreCuadroArchivistico)
                    {
                        hoja.Cells["A" + fila].Value = cadido.PadreClave + " - " + cadido.PadreNombre;
                        hoja.Cells["A" + fila + ":O" + fila].Merge = true;
                        hoja.Cells["A" + fila + ":O" + fila].Style.Font.Bold = true;
                        var fondo = Color.FromArgb(200, 200, 200);
                        hoja.Cells["A" + fila + ":O" + fila].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        hoja.Cells["A" + fila + ":O" + fila].Style.Fill.BackgroundColor.SetColor(fondo);
                        hoja.Cells["A" + fila + ":O" + fila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        fila++;
                        idPadreActual = cadido.IDPadreCuadroArchivistico;
                    }

                    hoja.Cells["A" + fila].Value = cadido.Clave;
                    hoja.Cells["B" + fila].Value = cadido.Nombre;
                    hoja.Cells["C" + fila].Value = cadido.EsAdministrativo ? "X" : string.Empty;
                    hoja.Cells["D" + fila].Value = cadido.EsLegal ? "X" : string.Empty;
                    hoja.Cells["E" + fila].Value = cadido.EsContableFiscal ? "X" : string.Empty;

                    hoja.Cells["F" + fila].Value = cadido.TiempoTramite;
                    hoja.Cells["F" + fila + ":G" + fila].Merge = true;

                    hoja.Cells["H" + fila].Value = cadido.TiempoConcentracion;
                    hoja.Cells["H" + fila + ":I" + fila].Merge = true;

                    hoja.Cells["J" + fila].Value = string.Empty;
                    hoja.Cells["L" + fila].Value =
                        cadido.IDDisposicionDocumental == (int) TiposDisposicionDocumental.BajaDocumental
                            ? "X"
                            : string.Empty;
                    hoja.Cells["M" + fila].Value =
                        cadido.IDDisposicionDocumental == (int) TiposDisposicionDocumental.ConservaciónPermanente
                            ? "X"
                            : string.Empty;
                    hoja.Cells["N" + fila].Value =
                        cadido.IDDisposicionDocumental == (int) TiposDisposicionDocumental.Muestreo
                            ? "X"
                            : string.Empty;
                    hoja.Cells["O" + fila].Value = string.Empty;

                    hoja.Cells["C" + fila + ":O" + fila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    fila++;
                }

                hoja.Cells["A6:O" + (fila - 1)].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                hoja.Cells["A6:O" + (fila - 1)].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                hoja.Cells["A6:O" + (fila - 1)].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                hoja.Cells["A6:O" + (fila - 1)].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                hoja.PrinterSettings.PrintArea = hoja.Cells["A1:P" + (fila - 1)];

                return excel.GetAsByteArray();
            }
        }

        public byte[] GenerarExpedientes(ExpedienteBuscarViewModel criterios)
        {
            var archivoPlantilla = RecursosServicios.Plantilla_Expedientes;

            List<ReporteExpediente> reporteExpedientes;

            using (var contexto = new Contexto())
            {
                var repositorio = new ExpedienteRepositorio(contexto);
                reporteExpedientes = repositorio.ObtenerReporte(criterios);
            }

            using (var stream = new MemoryStream(archivoPlantilla))
            using (var contexto2 = new Contexto())
            using (var excel = new ExcelPackage(stream))
            {
                var hoja = excel.Workbook.Worksheets.First();

                var fila = 3;
                var consecutivo = 1;
                foreach (var expediente in reporteExpedientes)
                {                   
                    hoja.Cells["A" + fila].Value = consecutivo.ToString();
                    hoja.Cells["A" + fila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja.Cells["B" + fila].Value = expediente.Asunto;
                    hoja.Cells["B" + fila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    hoja.Cells["B" + fila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                    hoja.Cells["C" + fila].Value = expediente.ClaveCuadroArchivistico;
                    hoja.Cells["C" + fila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja.Cells["D" + fila].Value = expediente.NombreCuadroArchivistico;
                    hoja.Cells["D" + fila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    hoja.Cells["D" + fila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                    hoja.Cells["E" + fila].Value = expediente.NumeroExpediente;
                    hoja.Cells["E" + fila + ":U" + fila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja.Cells["F" + fila].Value = expediente.NumeroLegajo;


                    var repositorio = new ExpedienteRepositorio(contexto2);
                    var fila2 = fila;
                    var consecutivo2 = 1;
                    foreach (var legajos in repositorio.Consultarlegajos(expediente.IdExpediente)) 
                    {
                        hoja.Cells["A" + fila + ":" + "A" + fila2].Merge = true;
                        hoja.Cells["B" + fila + ":" + "B" + fila2].Merge = true;
                        hoja.Cells["C" + fila + ":" + "C" + fila2].Merge = true;
                        hoja.Cells["D" + fila + ":" + "D" + fila2].Merge = true;
                        hoja.Cells["E" + fila + ":" + "E" + fila2].Merge = true;
                        hoja.Cells["F" + fila + ":" + "F" + fila2].Merge = true;
                        hoja.Cells["H" + fila + ":" + "H" + fila2].Merge = true;
                        hoja.Cells["I" + fila + ":" + "I" + fila2].Merge = true;
                        hoja.Cells["K" + fila + ":" + "K" + fila2].Merge = true;
                        hoja.Cells["L" + fila + ":" + "L" + fila2].Merge = true;
                        hoja.Cells["M" + fila + ":" + "M" + fila2].Merge = true;
                        hoja.Cells["N" + fila + ":" + "N" + fila2].Merge = true;
                        hoja.Cells["O" + fila + ":" + "O" + fila2].Merge = true;
                        hoja.Cells["P" + fila + ":" + "P" + fila2].Merge = true;
                        hoja.Cells["Q" + fila + ":" + "Q" + fila2].Merge = true;
                        hoja.Cells["R" + fila + ":" + "R" + fila2].Merge = true;
                        hoja.Cells["S" + fila + ":" + "S" + fila2].Merge = true;
                        hoja.Cells["T" + fila + ":" + "T" + fila2].Merge = true;
                        hoja.Cells["U" + fila + ":" + "U" + fila2].Merge = true;

                        hoja.Cells["A" + fila + ":" + "A" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["B" + fila + ":" + "B" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["C" + fila + ":" + "C" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["D" + fila + ":" + "D" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["E" + fila + ":" + "E" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["F" + fila + ":" + "F" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["G" + fila + ":" + "G" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["H" + fila + ":" + "H" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["I" + fila + ":" + "I" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["K" + fila + ":" + "K" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["L" + fila + ":" + "L" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["M" + fila + ":" + "M" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["N" + fila + ":" + "N" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["O" + fila + ":" + "O" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["P" + fila + ":" + "P" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["Q" + fila + ":" + "Q" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["R" + fila + ":" + "R" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["S" + fila + ":" + "S" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["T" + fila + ":" + "T" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hoja.Cells["U" + fila + ":" + "U" + fila2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        hoja.Cells["G" + fila2].Value = consecutivo2 + "/" + expediente.NumeroLegajo;
                        hoja.Cells["G" + fila2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["J" + fila2].Value = legajos.FolioInicial + "-" + legajos.FolioFinal;
                        hoja.Cells["J" + fila2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        fila2++;
                        consecutivo2++;
                    }                    

                    hoja.Cells["H" + fila].Value = expediente.FechaInicio;
                    hoja.Cells["H" + fila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja.Cells["I" + fila].Value = expediente.FechaTermino;
                    hoja.Cells["H" + fila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja.Cells["K" + fila].Value = expediente.Clasificacion;
                    hoja.Cells["L" + fila].Value = expediente.Estado;
                    hoja.Cells["M" + fila].Value = expediente.TiempoTramite + " Años";
                    hoja.Cells["N" + fila].Value = expediente.TiempoConcentracion + " Años";
                    hoja.Cells["O" + fila].Value = expediente.EsAdministrativo ? "A" : string.Empty;
                    hoja.Cells["P" + fila].Value = expediente.EsContableFiscal ? "F/C" : string.Empty;
                    hoja.Cells["Q" + fila].Value = expediente.EsLegal ? "L" : string.Empty;
                    hoja.Cells["R" + fila].Value = "";
                    hoja.Cells["S" + fila].Value = "";
                    hoja.Cells["T" + fila].Value = "";
                    hoja.Cells["U" + fila].Value = expediente.Contenedor;
                    hoja.Cells["V" + fila].Value = "";
                    hoja.Cells["W" + fila].Value = "";

                    if (repositorio.TotalLegajos(expediente.IdExpediente) > 0)
                    {
                        fila = fila2;
                    }

                    else
                    {
                        fila++;                
                    }
                    
                    consecutivo++;
                }

                hoja.PrinterSettings.PrintArea = hoja.Cells["A1:P" + (fila - 1)];

                return excel.GetAsByteArray();
            }
        }

        public byte[] GenerarEtiquetas(ExpedienteGuardarViewModel criterios)
        {
            var archivoPlantilla = RecursosServicios.Etiquetas_Legajo;

            using (var contexto = new Contexto())
            {
                var repositorio = new ExpedienteRepositorio(contexto);
                criterios.Expediente = repositorio.ObtenerPorId(criterios.Expediente.IDExpediente);
            }
            using (var stream = new MemoryStream(archivoPlantilla))
            using (var excel = new ExcelPackage(stream))
            {

                var consecutivo = 2;
                var legajoNumero = 1;               

                if (criterios.Expediente.IDEstadoExpediente == 2)
                {
                    var nombrePrimerLegajo = excel.Workbook.Worksheets["Etiqueta legajo 1"];
                    nombrePrimerLegajo.Name = "Etiqueta legajo 1";

                    for (var j = 1; j < criterios.Expediente.Legajo.Count; j++)
                    {
                        excel.Workbook.Worksheets.Copy("Etiqueta legajo 1", "Etiqueta legajo " + consecutivo);
                        consecutivo++;
                    }
                    consecutivo = 2;

                    foreach (var etiquetaFrontalLegajo in criterios.Expediente.Legajo)
                    {
                        var hoja = excel.Workbook.Worksheets[consecutivo];
                        hoja.Cells["C11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["C11"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["C11"].Value = criterios.Expediente.Cadido.CuadroArchivistico.PadreCuadroArchivistico
                            .PadreCuadroArchivistico.Nombre;
                        hoja.Cells["C15"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["C15"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["C15"].Value = criterios.Expediente.Cadido.CuadroArchivistico.PadreCuadroArchivistico.Nombre;
                        hoja.Cells["A18"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["A18"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["A18"].Value = "UnidadAdmin";
                        hoja.Cells["C21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["C21"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["C21"].Value = "Areaproductora";
                        hoja.Cells["C24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["C24"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["C24"].Value = criterios.Expediente.Cadido.CuadroArchivistico.Clave;
                        hoja.Cells["A27"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["A27"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["A27"].Value = criterios.Expediente.Cadido.CuadroArchivistico.Nombre;
                        hoja.Cells["T28"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["T28"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["T28"].Value = criterios.Expediente.NumeroExpediente;
                        hoja.Cells["T29"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["T29"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["T29"].Value = legajoNumero + "/" + criterios.Expediente.Legajo.Count;
                        hoja.Cells["B31"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["B31"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["B31"].Value = criterios.Expediente.DescripcionAsunto;
                        hoja.Cells["C34"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["C34"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["C34"].Value = criterios.Expediente.Cadido.TipoClasificacionInformacion.Nombre;
                        hoja.Cells["J37"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["J37"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["J37"].Value = criterios.Expediente.Cadido.TiempoTramite;
                        hoja.Cells["J38"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["J38"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["J38"].Value = criterios.Expediente.Cadido.TiempoConcentracion;
                        hoja.Cells["R42"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["R42"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["R42"].Value = etiquetaFrontalLegajo.FolioInicial + "-" + etiquetaFrontalLegajo.FolioFinal;
                        hoja.Cells["W37"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["W37"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["W37"].Value = criterios.Expediente.Cadido.EsAdministrativo ? "A" : string.Empty;
                        hoja.Cells["Y37"].Value = criterios.Expediente.Cadido.EsContableFiscal ? "F/C" : string.Empty;
                        hoja.Cells["Z37"].Value = criterios.Expediente.Cadido.EsLegal ? "L" : string.Empty;
                        hoja.Cells["W38"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["W38"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["W38"].Value = 1;
                        hoja.Cells["C41"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["C41"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["C41"].Value = etiquetaFrontalLegajo.FolioInicial;
                        hoja.Cells["Q41"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["Q41"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["Q41"].Value = etiquetaFrontalLegajo.FolioFinal;
                        hoja.Cells["C44"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["C44"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["C44"].Value = criterios.Expediente.Contenedor.Nombre;
                        hoja.Cells["Q44"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        hoja.Cells["Q44"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["Q44"].Value = "signatura top.";

                        consecutivo++;
                        legajoNumero++;
                    }
                }

                if (criterios.Expediente.IDEstadoExpediente == 1)
                {
                    excel.Workbook.Worksheets.Delete(2);
                }

                var hoja2 = excel.Workbook.Worksheets[1];                
                hoja2.Cells["A3"].Value = "1";
                hoja2.Cells["A3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                hoja2.Cells["B3"].Value = criterios.Expediente.DescripcionAsunto;
                hoja2.Cells["B3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                hoja2.Cells["B3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                hoja2.Cells["C3"].Value = criterios.Expediente.Cadido.CuadroArchivistico.Clave;
                hoja2.Cells["C3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                hoja2.Cells["D3"].Value = criterios.Expediente.Cadido.CuadroArchivistico.Nombre;
                hoja2.Cells["D3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                hoja2.Cells["D3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                hoja2.Cells["E3"].Value = criterios.Expediente.NumeroExpediente;
                hoja2.Cells["E3" + ":U3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                hoja2.Cells["F3"].Value = criterios.Expediente.Legajo.Count;
                
                var legajoFila = 3;
                legajoNumero = 1;
                foreach (var legajos in criterios.Expediente.Legajo)
                {
                    hoja2.Cells["A3:" +"A"+legajoFila].Merge = true;
                    hoja2.Cells["B3:" + "B" + legajoFila].Merge = true;
                    hoja2.Cells["C3:" + "C" + legajoFila].Merge = true;
                    hoja2.Cells["D3:" + "D" + legajoFila].Merge = true;
                    hoja2.Cells["E3:" + "E" + legajoFila].Merge = true;
                    hoja2.Cells["F3:" + "F" + legajoFila].Merge = true;

                    hoja2.Cells["A3:" + "A" + legajoFila].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    hoja2.Cells["B3:" + "B" + legajoFila].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    hoja2.Cells["C3:" + "C" + legajoFila].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    hoja2.Cells["D3:" + "D" + legajoFila].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    hoja2.Cells["E3:" + "E" + legajoFila].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    hoja2.Cells["F3:" + "F" + legajoFila].Style.VerticalAlignment = ExcelVerticalAlignment.Center;



                    hoja2.Cells["G" + legajoFila].Value =  legajoNumero + "/" + criterios.Expediente.Legajo.Count;
                    hoja2.Cells["G" + legajoFila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja2.Cells["H" + legajoFila].Value = legajos.FechaCreacion;
                    hoja2.Cells["H" + legajoFila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja2.Cells["I" + legajoFila].Value = legajos.FechaModificacion;
                    hoja2.Cells["I" + legajoFila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja2.Cells["J" + legajoFila].Value = legajos.FolioInicial + "-" + legajos.FolioFinal;
                    hoja2.Cells["J" + legajoFila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    hoja2.Cells["K" + legajoFila].Value = criterios.Expediente.Cadido.TipoClasificacionInformacion.Nombre;
                    hoja2.Cells["K" + legajoFila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja2.Cells["L" + legajoFila].Value = criterios.Expediente.EstadoExpediente.Nombre;
                    hoja2.Cells["L" + legajoFila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja2.Cells["M" + legajoFila].Value = criterios.Expediente.Cadido.TiempoTramite + " Años";
                    hoja2.Cells["M" + legajoFila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja2.Cells["N" + legajoFila].Value = criterios.Expediente.Cadido.TiempoConcentracion + " Años";
                    hoja2.Cells["N" + legajoFila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja2.Cells["O" + legajoFila].Value = criterios.Expediente.Cadido.EsAdministrativo ? "A" : string.Empty;
                    hoja2.Cells["O" + legajoFila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja2.Cells["P" + legajoFila].Value = criterios.Expediente.Cadido.EsContableFiscal ? "F/C" : string.Empty;
                    hoja2.Cells["P" + legajoFila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    hoja2.Cells["Q" + legajoFila].Value = criterios.Expediente.Cadido.EsLegal ? "L" : string.Empty;
                    hoja2.Cells["Q" + legajoFila].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    legajoFila++;
                    legajoNumero++;
                }                                                                
                hoja2.Cells["R3"].Value = "";
                hoja2.Cells["S3"].Value = "";
                hoja2.Cells["T3"].Value = "";
                hoja2.Cells["U3"].Value = criterios.Expediente.Contenedor.Nombre;
                hoja2.Cells["V3"].Value = "";
                hoja2.Cells["W3"].Value = "";

                hoja2.PrinterSettings.PrintArea = hoja2.Cells["A3:P"];

                return excel.GetAsByteArray();
            }
        }
      
    }
}
